#include "stdafx.h"

#include <fstream>
#include <iostream>
#include <string>
#include <random>
#include <time.h>

#include "mapGenerator.h"
#include "globalVariables.h"

void generateMap(unsigned int _mapSizeX, unsigned int _mapSizeY, float difficulty, std::string tilesetLocation)
{
	std::string defaultRandomMapLocation;
	defaultRandomMapLocation = "Resources/randomMap.txt";


	std::fstream openfile(defaultRandomMapLocation);

	if (openfile.is_open())
	{
		openfile << tilesetLocation << std::endl;
		openfile << _mapSizeX << std::endl;
		openfile << _mapSizeY << std::endl;
		openfile << 32 << std::endl;

		mapGeneratorSeed = (unsigned int)(time(NULL));
		//mapGeneratorSeed = 1429812109;
		//mapGeneratorSeed = 1430240112;

		srand(mapGeneratorSeed);

		for (unsigned int i = 0; i < _mapSizeY; i++)
		{
			for (unsigned int j = 0; j < _mapSizeX; j++)
			{
				if (j == 0)
				{
					openfile << "2,3 ";
				}
				else if (j == _mapSizeX - 1)
				{
					openfile << "3,3 ";
				}
				else if (i == 0 || i == _mapSizeY - 1)
				{
					openfile << "4,0 ";
				}
				else
				{
					int randomCheck = rand() % 100 + 1;
					if (randomCheck > 95)
					{
						
						openfile << "4,3 ";
						
					}
					/*else if (randomCheck == 9)
					{
						openfile << "6,0 ";
					}
					else if (randomCheck == 9 || randomCheck == 7 || randomCheck == 6)
					{
						openfile << "6,0 ";
					}*/
					else
					{
						openfile << "0,0 ";
					}
				}
			}
			if (i < _mapSizeY - 1)
			{
				openfile << "\n";
			}
		}
	}
	openfile.close();
}