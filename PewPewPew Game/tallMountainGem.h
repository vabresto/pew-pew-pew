#ifndef TALLMOUNTAINGEM_CPP_
#define	TALLMOUNTAINGEM_CPP_
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#pragma once

namespace sf
{
	class Texture;
}

namespace enemyMonster
{
	class tallMountainGem: public sf::Drawable
	{
	public:
		sf::Sprite gemSprite;

		tallMountainGem(const sf::Texture& mountainGemTexture, float _posX, float _posY);
		~tallMountainGem();
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		void update();
	};

};

extern std::vector < enemyMonster::tallMountainGem > gemList;
#endif