#ifndef UPDATETOCLIENT_H
#define UPDATETOCLIENT_H
#pragma once
/*
extern void updateToClient(int _packetHeader, std::string mapResourceToOpen, int playerNumber, std::string otherData, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateToClient(int _packetHeader, std::string textMessage, int _team, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateToClient(int _packetHeader, int playerID, std::string playerName, int playerTeam, sf::Vector2f newPlayerPosition, sf::Vector2f newPlayerVelocity, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateToClient(int _packetHeader, int _redTeamKills, int _blueTeamKills, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateToClient(int _packetHeader, int _bulletType, sf::Vector2f _bulletPosition, sf::Vector2f _bulletVelocity, int _team, bool _isDestroyed, sf::IpAddress clientIP, unsigned short clientPort);
*/

extern void updateBackendToClient(unsigned int _packetSubHeader, std::string _code, int _team, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateBackendToClient(unsigned int _packetSubHeader, std::string _mapLocation, int _iAmPlayerNumber, std::string _code, 
	float _redTeamSpawnX, float _redTeamSpawnY, float _blueTeamSpawnX, float _blueTeamSpawnY, int _scoreToWin, sf::IpAddress clientIP, unsigned short clientPort);

extern void updateBackendToClient(unsigned int _packetSubHeader, sf::Time _timeSpentInCurrentLevel, sf::IpAddress clientIP, unsigned short clientPort);

extern void updateChatToClient(unsigned int _packetSubHeader, std::string _chatMessage, int _team, sf::IpAddress clientIP, unsigned short clientPort);

extern void updateCharacterToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, std::string _username, int _team, int _characterType, sf::IpAddress clientIP, 
	unsigned short clientPort);
extern void updateCharacterToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, 
	float _posX, float _posY, int _directionFacing, int _currentFrameX, int _currentFrameY, float _health, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateCharacterToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _characterType, unsigned int _textureNumber, unsigned int _textureSubNumber, sf::IpAddress clientIP, unsigned short clientPort);

extern void updateScoreToClient(unsigned int _packetSubHeader, int _changeInScore, int _team, sf::IpAddress clientIP, unsigned short clientPort);

extern void updateParticlesToClient(unsigned int _packetSubHeader, sf::Vector2f _bulletPos, sf::Vector2f _velocity, int _team, float _damageDealt, 
	sf::IpAddress clientIP, unsigned short clientPort);
extern void updateParticlesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _particleType, unsigned int _posY, 
	sf::IpAddress clientIP, unsigned short clientPort);

extern void updateAbilitiesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownTime, 
	float _damageDealt, sf::Time _timeCharged, sf::IpAddress clientIP, unsigned short clientPort);
extern void updateAbilitiesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownLeft, 
	sf::IpAddress clientIP, unsigned short clientPort);
extern void updateAbilitiesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, bool _isStarting, bool _isDoing, bool _isEnding, bool _isActive, sf::IpAddress clientIP, unsigned short clientPort);

extern void updateStatusEffectsToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, unsigned int _effectID, int _value, float _damageDealt, 
	sf::Time _durationLeft, sf::IpAddress clientIP, unsigned short clientPort);

#endif