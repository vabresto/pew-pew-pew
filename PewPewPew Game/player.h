#ifndef PLAYER_H_
#define PLAYER_H_
#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System.hpp>
#include <vector>

struct playerAbilitiesStruct
{
	unsigned int ability;
	sf::Time cooldownLeft, cooldownTime, channelTime, durationLeft, durationTotal, channelLeft, timeCharged;
	bool isActive, hasChannel, canCharge, hasPersistentEffect;
	bool isStartingAbility, isDoingAbility, isEndingAbility;

	float damageDealt;
};

struct statusEffectStruct
{
	unsigned int effectID;

	float damageDealt;
	int value;

	sf::Time durationLeft;
};

// Forward Declaration
namespace sf {
	class Texture;
}

// provide your namespace to avoid collision/ambiguities
namespace myPlayerNamespace 
{
	/*******************/
	//Player Class
	/******************/
	class player: public sf::Drawable 
	{
	public:

		enum Direction 
		{
			Left, Right
		};

		bool isJumping, doubleJump;
		int directionFacing;
		int teamAffiliation;
		int playerID;

		float health, healthMax, armour, armourMax;

		sf::Time timeSinceAnimChange;
		sf::Time deathTimer;
		sf::Time deathDuration;

		bool doubleJumpAllowed;
		bool hitGround;
		bool isShooting;
		bool canWalk;
		bool isUsingAbilityAnimation;
		bool isNPC;
		bool isDead;
		int NPCType;

		int characterTexture;
		int textureSize;
		// 1 = 32 by 48, 2 = 48 by 48

		unsigned int NPCState;

		unsigned int currentFrameX, currentFrameY;
		std::vector < playerAbilitiesStruct > abilitiesVector;
		std::vector < statusEffectStruct > statusEffectsVector;

		std::string playerName;

		float xSpeed, ySpeed;

		bool goingLeft, goingRight, goingUp, goingDown;
		
		float playerMaxSpeed;
		float playerMaxJumpSpeed;
		float playerMaxFallSpeed;

		float playerSpeed;
		float playerJumpSpeed;
		float playerFallSpeed;

		float maxNormalJumpSpeed;

		float slowDebuffTimeLeft;
		float slowDebuffPlayerSpeed;
		float slowDebuffPlayerJumpSpeed;

		player(const sf::Texture& playerTexture, const float _startPosX, const float _startPosY, const int _team, bool _isNPC);
		virtual ~player();

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		/*
		void moveLeft(float speed);
		void moveRight(float speed);
		void moveUp(float speed);
		void moveDown(float speed);
		void movePlayer(float xSpeed, float ySpeed);*/

		void updatePlayer();

		sf::Sprite mSprite;
		sf::Vector2i mSource;
		sf::Vector2f playerVelocity;
		sf::Vector2f playerMaxVelocity;
	};

	/*******************/
	//Projectile Class
	/******************/
	class projectile : public sf::Drawable
		{
			
	public:

		enum Direction 
		{
			Left, Right
		};
		float projectileSpeed;

		bool goingLeft, goingRight, destroyBullet;
		int bulletType;

		projectile(const sf::Texture& imagePath, const sf::Vector2f currentPos, const sf::Vector2f velocity, const int _bulletNumber, const int _team, float _damageDealt);
		virtual ~projectile();

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		void moveLeft(float speed);
		void moveRight(float speed);
		void update();

		sf::Sprite mSprite;
		sf::Vector2i mSource;
		sf::Vector2f startPos;

		sf::Vector2f currentPosition;
		sf::Vector2f velocity;

		int direction;
		int myBulletNumber;
		int teamAffiliationBullet;

		float damageDealt;
	};
} /*end namespace*/

extern std::vector < myPlayerNamespace::projectile > bulletsList;
#endif