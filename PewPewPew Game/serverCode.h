#ifndef SERVERCODE_H_
#define SERVERCODE_H_
#pragma once

#include "player.h"

extern void runServer();
extern void generateMap();

struct computerID
{
	sf::IpAddress computerIP;
	unsigned short port;
	std::string computerUsername;
	bool isReadyToStartGame;
	int team;
	int computerIDValue;
	int characterType;
	unsigned int playerNumber;
};

struct playerStatsServer
{
	std::string playerName;
	int computerIDValue;

	int team;

	int directionFacing;

	int playerLevel;
	int characterStats[10];

	int playerNumber;
	sf::Vector2f playerPosition;
	sf::Vector2f playerVelocity;

	int currentFrameX, currentFrameY;

	int textureNumber, textureSubNumber;

	float health;

	std::vector < playerAbilitiesStruct > abilitiesVector;
	std::vector < statusEffectStruct > statusEffectsVector;
};

struct playerStatsClient
{
	std::string playerName;
	int computerIDValue;
	int team;
	int playerLevel;
	int characterStats[5];
	int playerNumber;
};


#endif