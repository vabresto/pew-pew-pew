#include "stdafx.h"

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include <vector>
#include <iostream>
#include <fstream>

#include "player.h"
#include "enemies.h"
#include "globalVariables.h"
#include "serverCode.h"
#include "clientCode.h"
#include "updateToClient.h"

void runServer()
{
	gameWindow.close();
	//Set up
	char temp;
	std::cout << localLANIPAddress << std::endl;
	std::cout << localIPAddress << std::endl;

	std::cout << "Enter port to listen to: ";
	std::cin >> UDPSendPort;
	std::cout << UDPSendPort << std::endl;

	std::cout << "Should the server resend packets to the packet's sender? [Y]es or [N]o: ";
	std::cin >> temp;

	if (temp == 'y' || temp == 'Y')
	{
		serverResendUpdateToSender = true;
	}
	else
	{
		serverResendUpdateToSender = false;
	}

	if (UDPSocket.bind(UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error binding to port " << UDPSocket.getLocalPort() << "! \n";
	}

	UDPSocket.setBlocking(false);
	socketSelector.add(UDPSocket);

	bool done = false;
	sf::Vector2f playerUpdatePosition, playerUpdateVelocity;

	/*unsigned int i1 = 0;
	unsigned int j1 = 0;
	unsigned int k1 = 0;
	unsigned int l1 = 0;*/

	while (done == false)
	{
		elapsedTime = mainGameClock.restart();
		timeSinceLastUpdate += elapsedTime;
		timeSpentInCurrentLevel += elapsedTime;
		while (UDPSocket.receive(inPacket, senderIPAddress, UDPRecievePort) == sf::Socket::Done)
		{
			if (timeSinceLastUpdate > sf::seconds(5.f))
			{
				timeSinceLastUpdate -= sf::seconds(5.f);
				//TODO: Update scores to client
				if (isGameOngoing == true)
				{
					if (connections.size() > 0)
					{
						for (unsigned int i = 0; i < connections.size(); i ++)
						{

							updateBackendToClient(4, timeSpentInCurrentLevel, connections[i].computerIP, connections[i].port);

							/*
							for (unsigned int i = 0; i < connections.size(); i++)
							{
							for (unsigned int j = 0; j < connections.size(); j++)
							{
							//updateCharacterToClient(0, connections[i].computerIDValue, connections[i].computerUsername, connections[i].team, connections[i].characterType, connections[j].computerIP, connections[j].port);
							//updateCharacterToClient(1, playerStatsServerList[i].computerIDValue, playerStatsServerList[i].playerPosition.x, playerStatsServerList[i].playerPosition.y, playerStatsServerList[i].directionFacing, playerStatsServerList[i].currentFrameX, playerStatsServerList[i].currentFrameY, playerStatsServerList[i].health, connections[j].computerIP, connections[j].port);
							//updateCharacterToClient(3, connections[i].computerIDValue, connections[i].characterType, playerStatsServerList[i].textureNumber, playerStatsServerList[i].textureSubNumber, connections[j].computerIP, connections[j].port);
							}
							}*/
						}
					}
				}
			}
			if (inPacket >> packetHeader)
			{
				if (packetHeader == 0)
				{	
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 0)
						{
							if (isGameOngoing == false)
							{
								if (inPacket >> testData >> username >> whichTeam)
								{
									if (testData == "applesbananascoconutsdontexist")
									{
										std::cout << "Recieved authentication code from \n" << username << " located at " << senderIPAddress << " on port " << UDPRecievePort << std::endl;
										UDPSendPort = UDPRecievePort;

										wasConnectedTo = true;
										isStillConnectedTo = true;

										computerID newConnection;
										newConnection.computerIP = senderIPAddress;
										newConnection.port = UDPRecievePort;
										newConnection.computerUsername = username;
										newConnection.isReadyToStartGame = false;
										newConnection.team = whichTeam;

										playerStatsServer newPlayerStats;


										bool isRegistered = false;
										for (unsigned int i = 0; i < connections.size(); i++)
										{
											if (connections[i].computerIP == newConnection.computerIP && connections[i].port == newConnection.port)
											{
												isRegistered = true;
											}
										}
										if (isRegistered == false)
										{
											connections.push_back(newConnection);

											connections[connections.size()- 1].computerIDValue = connections.size() - 1;

											newPlayerStats.team = whichTeam;
											newPlayerStats.computerIDValue = connections.size()-1;
											newPlayerStats.playerLevel = 0;
											newPlayerStats.playerName = connections[connections.size()-1].computerUsername;

											std::cout << connections.size();

											if (whichTeam == 1)
											{
												numOfRedPlayers++;
											}
											else if (whichTeam == 2)
											{
												numOfBluePlayers++;
											}

											playerStatsServerList.push_back(newPlayerStats);

											//Send handshake to client
											updateBackendToClient(0, "simonsaysshello", 1, connections[connections.size()- 1].computerIP, connections[connections.size()- 1].port);
											//updateToClient(1, "simonsaysshello", 1, connections[connections.size()- 1].computerIP, connections[connections.size()- 1].port);

											for (unsigned int i = 0; i < connections.size(); i++)
											{
												if (connections[i].computerUsername != username)
												{
													updateChatToClient(1, (username + " has joined the game!"), 0, connections[i].computerIP, connections[i].port);
												}
											}
										}
										else
										{
											std::cout << "Computer is already registered!\n";
										}
									}
									else if (testData == "vaguewhitexrayyellowcakeszoneout")
									{
										std::cout << "Recieved disconnect code from \n" << username << " located at " << senderIPAddress << " on port " << UDPRecievePort << std::endl;
										for (unsigned int i = 0; i < connections.size(); i++)
										{
											if (connections[i].computerIP == senderIPAddress)
											{
												connections.erase(connections.begin() + i);
												if (connections.size() < 1)
												{
													isStillConnectedTo = false;
												}
											}
										}
									}
								}
							}
							else //If there IS an active game, deny the request
							{
								updateChatToClient(1, "Server rejected connection: Game is already in progress", 0, senderIPAddress, UDPRecievePort);
							}
						}
						else if (packetSubHeader == 1) //TODO: Update all clients (?)
						{
							unsigned int player, character;
							int team;

							if (inPacket >> player >> character >> team)
							{
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].computerIP == senderIPAddress && connections[i].port == UDPRecievePort)
									{
										connections[i].characterType = character;
										connections[i].team = team;
										connections[i].playerNumber = connections[i].computerIDValue;
										break;
									}
								}
							}
						}
						else if (packetSubHeader == 2) //TODO: Reply to client with server time (timestamps, etc)
						{
							sf::Int32 clientGameTime;
							if (inPacket >> clientGameTime)
							{
								//TODO: Reply to client with proper time, if it is incorrect
							}
						}
						inPacket.clear();
					}
				}
				else if (packetHeader == 1)
				{
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 1)
						{
							std::string temp;
							if (inPacket >> textMessage >> teamAffiliation)
							{
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (senderIPAddress == connections[i].computerIP)
									{
										if(textMessage.find_first_not_of(' ') != std::string::npos)
										{
											// There's a non-space
											temp = connections[i].computerUsername;
											temp += ": ";
											temp += textMessage;
											if (textMessage == "ready")
											{
												for (unsigned int i = 0; i < connections.size(); i++)
												{
													if (senderIPAddress == connections[i].computerIP)
													{
														if (connections[i].isReadyToStartGame == false)
														{
															connections[i].isReadyToStartGame = true;
															std::cout << connections[i].computerUsername << " is ready to start!\n";
															numOfPlayersReady++;
															temp = connections[i].computerUsername + " is ready to start! (" + std::to_string((_ULonglong)numOfPlayersReady) + "/" + std::to_string((_ULonglong)connections.size()) + ")";
														}
														else
														{
															connections[i].isReadyToStartGame = false;
															std::cout << connections[i].computerUsername << " is no longer ready to start!\n";
															numOfPlayersReady--;
															temp = connections[i].computerUsername + " is no longer ready to start! (" + std::to_string((_ULonglong)numOfPlayersReady) + "/" + std::to_string((_ULonglong)connections.size()) + ")";
														}
													}
												}

												//Check if all players are ready
												if (numOfPlayersReady == connections.size())
												{
													std::cout << "Start game!\n";
													isGameOngoing = true;
													for (unsigned int i = 0; i < connections.size(); i++)
													{
														for (unsigned int j = 0; j < playerStatsServerList.size(); j++)
														{
															updateCharacterToClient(1, playerStatsServerList[j].computerIDValue, 100.f, 100.f, 1, 1, 1, 25.f, connections[i].computerIP, connections[i].port);
															//updateToClient(2, playerStatsServerList[j].computerIDValue, playerStatsServerList[j].playerName, playerStatsServerList[j].team, playerStatsServerList[j].playerPosition, playerStatsServerList[j].playerVelocity, connections[i].computerIP, connections[i].port);
														}
														
														if (connections[i].computerIDValue == i)
														{
															updateBackendToClient(1, "Resources/arenaMap.txt", i, "totallysecrectpassword", 1000.f, 1440.f, 2000.f, 1440.f, 15, connections[i].computerIP, connections[i].port);
														}

														//updateBackendToClient(1, "Resources/arenaMap.txt", playerStatsServerList[i].computerIDValue, "totallysecrectpassword", 520.f, 1440.f, 3200.f, 1440.f, connections[i].computerIP, connections[i].port);
														//updateToClient(0, "Resources/arenaMap.txt", playerStatsServerList[i].computerIDValue, "totallysecrectpassword", connections[i].computerIP, connections[i].port);
													}
												}
											}
										}
									}
								}
								messagesList.push_back(messagingStruct());
								messagesList[numOfMessages].message = temp;
								messagesList[numOfMessages].team = teamAffiliation;
								numOfMessages++;
								std::cout << temp << std::endl;

								//TODO: Update all clients with message
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									updateChatToClient(1, temp, teamAffiliation, connections[i].computerIP, connections[i].port);
									//updateToClient(1, temp, teamAffiliation, connections[i].computerIP, connections[i].port);
								}

								temp.clear();
								textMessage.clear();
							}
						}
						inPacket.clear();
					}
				}
				else if (packetHeader == 2)
				{
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 0)
						{
							int playerNumber, team, characterType;
							std::string _username;
							if (inPacket >> playerNumber >> _username >> team >> characterType)
							{
								connections[playerNumber].team = team;
								connections[playerNumber].characterType = characterType;

								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].playerNumber != playerNumber)
									{
										updateCharacterToClient(0, playerNumber, _username, team, characterType, connections[i].computerIP, connections[i].port);
									}
								}
							}
						}
						else if (packetSubHeader == 1)
						{
							unsigned int _player;
							float posX, posY, health;
							int _currentFrameX, _currentFrameY, _directionFacing;

							if (inPacket >> _player >> posX >> posY >> _directionFacing >> _currentFrameX >> _currentFrameY >> health)
							{
								//for (unsigned int i = 0; i < playerStatsServerList.size(); i++)
								//{
									//if (connections[i].computerIDValue == _player)
									//{
										//TODO: Authenticate movement
										playerStatsServerList[_player].playerPosition.x = posX;
										playerStatsServerList[_player].playerPosition.y = posY;

										playerStatsServerList[_player].directionFacing = _directionFacing;

										playerStatsServerList[_player].currentFrameX = _currentFrameX;
										playerStatsServerList[_player].currentFrameY = _currentFrameY;

										playerStatsServerList[_player].health = health;

										for (unsigned int i = 0; i< connections.size(); i++)
										{
											//if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
											if (connections[i].computerIDValue != _player)
											{
												updateCharacterToClient(1, playerStatsServerList[_player].computerIDValue, playerStatsServerList[_player].playerPosition.x, playerStatsServerList[_player].playerPosition.y, playerStatsServerList[_player].directionFacing, playerStatsServerList[_player].currentFrameX, playerStatsServerList[_player].currentFrameY, playerStatsServerList[_player].health, connections[i].computerIP, connections[i].port);
												//updateToClient(2, playerStatsServerList[i].computerIDValue, playerStatsServerList[i].playerName, playerStatsServerList[i].team, playerStatsServerList[i].playerPosition, playerStatsServerList[i].playerVelocity, connections[j].computerIP, connections[j].port);
											}
										}
										break;
									//}
								//}
							}
						}
						else if (packetSubHeader == 2)
						{

						}
						else if (packetSubHeader == 3)
						{
							unsigned int _playerNumber;
							int _characterType;
							unsigned int _textureNumber, _textureSubNumber;
							if (inPacket >> _playerNumber >> _characterType >> _textureNumber >> _textureSubNumber)
							{
								connections[_playerNumber].characterType = _characterType;

								playerStatsServerList[_playerNumber].textureNumber = _textureNumber;
								playerStatsServerList[_playerNumber].textureSubNumber = _textureSubNumber;

								for (unsigned int i = 0; i< connections.size(); i++)
								{
									//if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
									if (connections[i].computerIDValue != _playerNumber)
									{
										updateCharacterToClient(3, _playerNumber, _characterType, _textureNumber, _textureSubNumber, connections[i].computerIP, connections[i].port);
									}
								}
							}
						}
						inPacket.clear();
					}
				}
				else if (packetHeader == 3) //TODO: Update all clients
				{
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 0)
						{
							int scoreChange, team;
							if (inPacket >> scoreChange >> team)
							{
								if (team == 1)
								{
									redTeamKills += scoreChange;
								}
								else if (team == 2)
								{
									blueTeamKills += scoreChange;
								}
								else
								{

								}
							}
							/*for (unsigned int i = 0; i < connections.size(); i++)
							{
								//updateScoreToClient(0, scoreChange, team, connections[i].computerIP, connections[i].port);
							}*/
							//TODO: Update all clients
						}
					}
				}
				else if (packetHeader == 4)
				{
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 1) //TODO: Update all clients - DONE (?)
						{
							sf::Vector2f velocity;
							int team;
							float damageDealt;

							if (inPacket >> bulletPositionReceived.x >> bulletPositionReceived.y >> velocity.x >> velocity.y >> team >> damageDealt)
							{
								std::cout << "Bullet created at position " << bulletPositionReceived.x << ", " << bulletPositionReceived.y << " by team " << team << "\n";
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
									//if (connections[i].computerIDValue != _player)
									{
										//TODO: Update other clients
										updateParticlesToClient(1, bulletPositionReceived, velocity, team, damageDealt, connections[i].computerIP, connections[i].port);
									}
								}
							}
						}
						else if (packetSubHeader == 2) //TODO: Update all clients - DONE (?)
						{
							int playerNumber; 
							int particleType;
							int posY;

							if (inPacket >> playerNumber >> particleType >> posY)
							{
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (particleType != 15)
									{
										//if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
										if (connections[i].computerIDValue != playerNumber)
										{
											//TODO: Update other clients
											updateParticlesToClient(2, playerNumber, particleType, posY, connections[i].computerIP, connections[i].port);
										}
									}
									else
									{
										updateParticlesToClient(2, playerNumber, particleType, posY, connections[i].computerIP, connections[i].port);	
									}
								}

								if (particleType == 15)
								{
									if (playerStatsServerList[playerNumber].team == 1)
									{
										for (unsigned int i = 0; i < connections.size(); i++)
										{
											updateScoreToClient(0, 1, 2, connections[i].computerIP, connections[i].port);
										}

									}
									else
									{
										for (unsigned int i = 0; i < connections.size(); i++)
										{
											updateScoreToClient(0, 1, 1, connections[i].computerIP, connections[i].port);
										}
									}
								}
								
							}
						}
						else if (packetSubHeader == 3)
						{

						}
						else if (packetSubHeader == 4)
						{

						}

						inPacket.clear();
					}
				}	
				else if (packetHeader == 5)
				{
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 0) //TODO: Update all clients
						{
							unsigned int playerNumber, ability, abilityNumber;
							float damageDealt;
							sf::Int32 cooldownTime, timeCharged;

							if (inPacket >> playerNumber >> ability >> abilityNumber >> cooldownTime >> damageDealt >> timeCharged)
							{

								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].playerNumber == playerNumber)
									{
										playerStatsServerList[i].abilitiesVector[ability].ability = abilityNumber;
										playerStatsServerList[i].abilitiesVector[ability].cooldownTime = sf::milliseconds(cooldownTime);
										playerStatsServerList[i].abilitiesVector[ability].damageDealt = damageDealt;
									}
								}

								for (unsigned int i = 0; i < connections.size(); i++)
								{
									//if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
									if (connections[i].computerIDValue != playerNumber)
									{
										updateAbilitiesToClient(0, playerNumber, ability, abilityNumber, sf::milliseconds(cooldownTime), damageDealt, sf::milliseconds(timeCharged), connections[i].computerIP, connections[i].port);
									}
								}
								//TODO: Update other clients
							}
						}
						else if (packetSubHeader == 1) //TODO: Update all clients
						{
							unsigned int playerNumber, ability, abilityNumber;
							sf::Int32 cooldownLeft;

							if (inPacket >> playerNumber >> ability >> abilityNumber >> cooldownLeft)
							{

								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].playerNumber == playerNumber)
									{
										playerStatsServerList[i].abilitiesVector[ability].ability = abilityNumber;
										playerStatsServerList[i].abilitiesVector[ability].cooldownLeft = sf::milliseconds(cooldownLeft);
									}
								}

								for (unsigned int i = 0; i < connections.size(); i++)
								{
									//if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
									if (connections[i].computerIDValue != playerNumber)
									{
										updateAbilitiesToClient(1, playerNumber, ability, abilityNumber, sf::milliseconds(cooldownLeft), connections[i].computerIP, connections[i].port);
									}
								}

								//TODO: Update other clients
							}
						}
						else if (packetSubHeader == 2)
						{
							unsigned int _playerNumber;
							int _ability, _abilityNumber;
							bool _isStarting, _isDoing, _isEnding, _isActive;

							if (inPacket >> _playerNumber >> _ability >> _abilityNumber >> _isStarting >> _isDoing >> _isEnding >> _isActive)
							{
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].playerNumber == _playerNumber)
									{
										playerStatsServerList[i].abilitiesVector[_ability].ability = _abilityNumber;
										playerStatsServerList[i].abilitiesVector[_ability].isStartingAbility = _isStarting;
										playerStatsServerList[i].abilitiesVector[_ability].isDoingAbility = _isDoing;
										playerStatsServerList[i].abilitiesVector[_ability].isEndingAbility = _isEnding;
										playerStatsServerList[i].abilitiesVector[_ability].isActive = _isActive;
									}
								}

								for (unsigned int i = 0; i < connections.size(); i++)
								{
									//if (connections[i].computerIP != senderIPAddress || connections[i].port != UDPRecievePort)
									if (connections[i].computerIDValue != _playerNumber)
									{
										updateAbilitiesToClient(2, _playerNumber, _ability, _abilityNumber, _isStarting, _isDoing, _isEnding, _isActive, connections[i].computerIP, connections[i].port);
									}
								}
							}
							inPacket.clear();
						}
					}
					inPacket.clear();
				}
				else if (packetHeader == 6)
				{
					if (inPacket >> packetSubHeader)
					{
						if (packetSubHeader == 0) //TODO: Update all clients
						{
							unsigned int playerNumber, effectID;
							int value;
							float damageDealt;
							sf::Int32 durationLeft;

							if (inPacket >> playerNumber >> effectID >> value >> damageDealt >> durationLeft)
							{
								for (unsigned int i = 0; i < connections.size(); i++)
								{
									if (connections[i].playerNumber == playerNumber)
									{
										for (unsigned int j = 0; j < playerStatsServerList[i].statusEffectsVector.size(); j++)
										{
											if (playerStatsServerList[i].statusEffectsVector[j].effectID == effectID)
											{
												playerStatsServerList[i].statusEffectsVector[j].value = value;
												playerStatsServerList[i].statusEffectsVector[j].damageDealt = damageDealt;
												playerStatsServerList[i].statusEffectsVector[j].durationLeft = sf::milliseconds(durationLeft);
												break;
											}
										}
									}
								}
							}
						}
						inPacket.clear();
					}
				}

				inPacket.clear();
			}
		}
		//Proper loop exit
		if (wasConnectedTo == true && isStillConnectedTo == false)
		{
			char repeatGame;
			std::cout << "Run again? [Y]es or [N]o: ";
			std::cin >> repeatGame;
			if (repeatGame == 'y' || repeatGame == 'Y')
			{
				wasConnectedTo = false;
				isGameOngoing = false;
				connections.clear();
			}
			else
			{
				done = true;
			}
		}
	}
	system("pause");
}
/*void generateMap(unsigned int mapSizeX, unsigned int mapSizeY, unsigned int seed, std::string newFileLocation, std::string newTilesetLocation)
{
	std::ofstream file(newFileLocation);

	if(file.is_open())
	{
		file << newTilesetLocation << std::endl;
		bool blockMapEdges;
		blockMapEdges = true;

		file << mapSizeX << std::endl;
		file << mapSizeY << std::endl;
		file << mapTileSize << std::endl;

		srand(seed);

		//Setting every tile to something
		for (unsigned int i = 0; i < mapSizeY; i++)
		{
			for (unsigned int j = 0; j < mapSizeX; j++)
			{
				//Set as "0,0 " for platformer
				if (blockMapEdges == true)
				{
					if (i == 0 || i == mapSizeY - 1)
					{
						file << "4,0 ";
					}
					else if (j == 0 || j == mapSizeX - 1)
					{
						file << "4,0 ";
					}
					else
					{
						int randomNumber = rand() % 4 + 1;

						if (randomNumber == 1)
						{

						}
						else if (randomNumber == 2)
						{

						}
						else if (randomNumber == 3)
						{

						}
						else if (randomNumber == 4)
						{

						}
						else
						{
							file << "0,0 ";
						}	
					}
				}
			}
			if (i < mapSizeY - 1)
				file << "\n";
		}
	}
	else
	{
		std::cout << "Could not open filepath " << newFileLocation << ".\n";
	}

	std::cout << "Map generated.\n";
}

*/