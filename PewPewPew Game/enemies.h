#ifndef ENEMIES_H_
#define ENEMIES_H_
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#pragma once

namespace sf
{
	class Texture;
}

namespace enemyMonster
{
	class enemyMonsterBase: public sf::Drawable
	{
	public:
		int health, armour, directionFacing;
		float movementSpeed, enemyJumpSpeed, enemyFallSpeed;
		bool isJumping;

		bool goingLeft, goingRight, goingDown, goingUp, destroyMonster;

		sf::Sprite enemySprite;

	
		enum Direction 
		{
			Left, Right
		};

		enemyMonsterBase(const sf::Texture& monsterBaseTexture, float _posX, float _posY);
		virtual ~enemyMonsterBase();

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		virtual void update();

		virtual void moveLeft();
		virtual void moveRight();
		virtual void moveUp();
		virtual void moveDown();
	};


};
extern std::vector < enemyMonster::enemyMonsterBase > targetsList;
#endif