#include "stdafx.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include "HUDManager.h"
#include "globalVariables.h"

//Forward declare functions
void initializeGameHUD();
void drawGameHUD();
void updateGameHUD();

//Declare sprites and textures
sf::Texture HUDAbilitiesGraphicTexture;
sf::Sprite HUDAbilitiesGraphicSprite;

sf::Texture HUDAbilitiesAbilityOneTexture;
sf::Sprite HUDAbilitiesAbilityOneSprite;

sf::Texture HUDAbilitiesAbilityTwoTexture;
sf::Sprite HUDAbilitiesAbilityTwoSprite;

sf::Texture HUDAbilitiesAbilityThreeTexture;
sf::Sprite HUDAbilitiesAbilityThreeSprite;

sf::Text abilityTwoCooldownTimerText, abilityOneCooldownTimerText;


void initializeGameHUD()
{
	abilityOneCooldownTimerText.setFont(arial);
	abilityOneCooldownTimerText.setCharacterSize(30);

	abilityTwoCooldownTimerText.setFont(arial);
	abilityTwoCooldownTimerText.setCharacterSize(30);

	if (!HUDAbilitiesGraphicTexture.loadFromFile("Resources/HUD/HUD-Abilities.png"))
	{
		std::cout << "Error loading HUD abilities sprite!\n";
	}
	HUDAbilitiesGraphicSprite.setTexture(HUDAbilitiesGraphicTexture);

	if (playersList[localPlayerNumber].abilitiesVector[7].ability == 1)
	{
		if (!HUDAbilitiesAbilityOneTexture.loadFromFile("Resources/HUD/shootArrowIcon.png"))
		{
			std::cout << "Error loading shoot arrow icon!\n";
		}
		HUDAbilitiesAbilityOneSprite.setTexture(HUDAbilitiesAbilityOneTexture);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[7].ability == 8)
	{
		if (!HUDAbilitiesAbilityOneTexture.loadFromFile("Resources/HUD/sliceDownIcon.png"))
		{
			std::cout << "Error loading slice down icon!\n";
		}
		HUDAbilitiesAbilityOneSprite.setTexture(HUDAbilitiesAbilityOneTexture);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[7].ability == 15)
	{
		if (!HUDAbilitiesAbilityOneTexture.loadFromFile("Resources/HUD/shootFireballIcon.png"))
		{
			std::cout << "Error loading shoot fireball icon!\n";
		}
		HUDAbilitiesAbilityOneSprite.setTexture(HUDAbilitiesAbilityOneTexture);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[7].ability == 22)
	{
		if (!HUDAbilitiesAbilityOneTexture.loadFromFile("Resources/HUD/rogueSlashIcon.png"))
		{
			std::cout << "Error loading rogue slash icon!\n";
		}
		HUDAbilitiesAbilityOneSprite.setTexture(HUDAbilitiesAbilityOneTexture);
	}

	if (playersList[localPlayerNumber].abilitiesVector[8].ability == 1)
	{
		if (!HUDAbilitiesAbilityTwoTexture.loadFromFile("Resources/HUD/transformHawkIcon.png"))
		{
			std::cout << "Error loading transform hawk icon!\n";
		}
		HUDAbilitiesAbilityTwoSprite.setTexture(HUDAbilitiesAbilityTwoTexture);
		HUDAbilitiesAbilityTwoSprite.setScale(1.f, 1.f);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[8].ability == 8)
	{
		if (!HUDAbilitiesAbilityTwoTexture.loadFromFile("Resources/HUD/shieldBlockIcon.png"))
		{
			std::cout << "Error loading shield block icon!\n";
		}
		HUDAbilitiesAbilityTwoSprite.setTexture(HUDAbilitiesAbilityTwoTexture);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[8].ability == 15)
	{
		if (!HUDAbilitiesAbilityTwoTexture.loadFromFile("Resources/HUD/thunderclapIcon.png"))
		{
			std::cout << "Error loading thunderclap icon!\n";
		}
		HUDAbilitiesAbilityTwoSprite.setTexture(HUDAbilitiesAbilityTwoTexture);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[8].ability == 22)
	{
		if (!HUDAbilitiesAbilityTwoTexture.loadFromFile("Resources/HUD/rogueDashIcon.png"))
		{
			std::cout << "Error loading rogue dash icon!\n";
		}
		HUDAbilitiesAbilityTwoSprite.setTexture(HUDAbilitiesAbilityTwoTexture);
	}

	if (playersList[localPlayerNumber].abilitiesVector[9].ability == 0)
	{
		if (!HUDAbilitiesAbilityThreeTexture.loadFromFile("Resources/HUD/lockedAbilityIcon.png"))
		{
			std::cout << "Error loading unknown ability icon!\n";
		}
		HUDAbilitiesAbilityThreeSprite.setTexture(HUDAbilitiesAbilityThreeTexture);
	}
}

void updateGameHUD()
{
	HUDAbilitiesGraphicSprite.setPosition(view1.getCenter().x - 300, view1.getCenter().y + 206);

	HUDAbilitiesAbilityOneSprite.setPosition(view1.getCenter().x - 124, view1.getCenter().y + 220);
	HUDAbilitiesAbilityTwoSprite.setPosition(view1.getCenter().x, view1.getCenter().y + 220);
	HUDAbilitiesAbilityThreeSprite.setPosition(view1.getCenter().x + 125, view1.getCenter().y + 220);

	HUDAbilitiesAbilityOneSprite.setOrigin(0.f, 0.f);
	HUDAbilitiesAbilityTwoSprite.setOrigin(0.f, 0.f);
	
	abilityOneCooldownTimerText.setPosition(view1.getCenter().x-75 , view1.getCenter().y + 245);
	abilityTwoCooldownTimerText.setPosition(view1.getCenter().x+25 , view1.getCenter().y + 245);

	//Ability Cooldown timer
	if (playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft > sf::Time::Zero)
	{
		abilityOneCooldownTimerText.setString(std::to_string((_ULonglong)playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft.asSeconds()));
	}
	if (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft > sf::Time::Zero)
	{
		abilityTwoCooldownTimerText.setString(std::to_string((_ULonglong)playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft.asSeconds()));
	}

	for (unsigned int i = 0; i < playersList[localPlayerNumber].statusEffectsVector.size(); i++)
	{
		if (playersList[localPlayerNumber].statusEffectsVector[i].effectID == 1)
		{
			if (playersList[localPlayerNumber].statusEffectsVector[i].value == 1)
			{
				HUDAbilitiesAbilityOneSprite.setTextureRect(sf::IntRect(0,0,100,70));
			}
			else
			{
				HUDAbilitiesAbilityOneSprite.setTextureRect(sf::IntRect(0,70,100,-70));
			}
		}
	}
	

	//Ability Icons
	if (playersList[localPlayerNumber].abilitiesVector[7].isActive == true)
	{
		HUDAbilitiesAbilityOneSprite.setColor(sf::Color::Magenta);

		if (playersList[localPlayerNumber].abilitiesVector[7].isActive == true && playersList[localPlayerNumber].abilitiesVector[7].ability == 22)
		{
			HUDAbilitiesAbilityOneSprite.setColor(sf::Color::Red);
		}
	}
	else if (playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft > sf::Time::Zero)
	{
		HUDAbilitiesAbilityOneSprite.setColor(sf::Color::Red);
	}
	else
	{
		HUDAbilitiesAbilityOneSprite.setColor(sf::Color::White);
	}

	if (playersList[localPlayerNumber].abilitiesVector[8].isActive == true)
	{
		HUDAbilitiesAbilityTwoSprite.setColor(sf::Color::Magenta);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft > sf::Time::Zero)
	{
		HUDAbilitiesAbilityTwoSprite.setColor(sf::Color::Red);
	}
	else
	{
		HUDAbilitiesAbilityTwoSprite.setColor(sf::Color::White);
	}
}

void drawGameHUD()
{
	//HUDAbilitiesGraphicSprite.setColor(sf::Color::Color(255,255,255,150));
	gameWindow.draw(HUDAbilitiesGraphicSprite);

	//HUDAbilitiesAbilityOneSprite.setColor(sf::Color::Color(255,255,255,150));
	HUDAbilitiesAbilityOneSprite.setScale(1.f, 1.f);
	gameWindow.draw(HUDAbilitiesAbilityOneSprite);

	//HUDAbilitiesAbilityTwoSprite.setColor(sf::Color::Color(255,255,255,150));
	HUDAbilitiesAbilityTwoSprite.setScale(1.f, 1.f);
	gameWindow.draw(HUDAbilitiesAbilityTwoSprite);

	//HUDAbilitiesAbilityThreeSprite.setColor(sf::Color::Color(255,255,255,150));
	gameWindow.draw(HUDAbilitiesAbilityThreeSprite);

	if (playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft > sf::Time::Zero)
	{
		HUDAbilitiesAbilityOneSprite.setPosition(view1.getCenter().x - 124 + 50, view1.getCenter().y + 220 + 35);
		HUDAbilitiesAbilityOneSprite.setOrigin(50.f, 35.f);

		HUDAbilitiesAbilityOneSprite.setScale((float)(((playersList[localPlayerNumber].abilitiesVector[7].cooldownTime.asSeconds()) - (playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft.asSeconds()))/(playersList[localPlayerNumber].abilitiesVector[7].cooldownTime.asSeconds())), (float)(((playersList[localPlayerNumber].abilitiesVector[7].cooldownTime.asSeconds()) - (playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft.asSeconds()))/(playersList[localPlayerNumber].abilitiesVector[7].cooldownTime.asSeconds())));
		HUDAbilitiesAbilityOneSprite.setColor(sf::Color::Color(225, 225, 225, 200));

		gameWindow.draw(HUDAbilitiesAbilityOneSprite);

		abilityOneCooldownTimerText.setColor(sf::Color::Color(255,255,255,255));

		gameWindow.draw(abilityOneCooldownTimerText);
	}


	if (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft > sf::Time::Zero)
	{
		HUDAbilitiesAbilityTwoSprite.setPosition(view1.getCenter().x + 50, view1.getCenter().y + 220 + 35);
		HUDAbilitiesAbilityTwoSprite.setOrigin(50.f, 35.f);

		HUDAbilitiesAbilityTwoSprite.setScale((float)(((playersList[localPlayerNumber].abilitiesVector[8].cooldownTime.asSeconds()) - (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft.asSeconds()))/(playersList[localPlayerNumber].abilitiesVector[8].cooldownTime.asSeconds())), (float)(((playersList[localPlayerNumber].abilitiesVector[8].cooldownTime.asSeconds()) - (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft.asSeconds()))/(playersList[localPlayerNumber].abilitiesVector[8].cooldownTime.asSeconds())));
		HUDAbilitiesAbilityTwoSprite.setColor(sf::Color::Color(225, 225, 225, 200));

		gameWindow.draw(HUDAbilitiesAbilityTwoSprite);

		abilityTwoCooldownTimerText.setColor(sf::Color::Color(255,255,255,255));

		gameWindow.draw(abilityTwoCooldownTimerText);
	}
}