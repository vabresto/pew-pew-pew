#ifndef MAINMENUELEMENTS_CPP_
#define MAINMENUELEMENTS_CPP_

#include <SFML/Network.hpp>

//Functions
extern void mainMenu();
extern void singleplayerCampaign();
extern void initializeMenuText();
extern void startSingleplayerMap(sf::Vector2f _position, sf::Vector2f _velocity, std::string _mapLocation);
extern void serverSelectMenu();
extern void serverLobbyMenu(unsigned int _serverNumber);
extern void serverTeamAndNameSelect(unsigned int _serverNumber);
extern void endOfMultiplayerGameScreen();
extern void gameEndScreen(unsigned int _winningTeam);
extern void initializeSettings();
extern void characterSelectScreen();
extern void initializeCharacterIcons();

//Map location
extern std::string mapLocation;

//Textures and Sprites
extern sf::Texture mainMenuBackgroundTexture;
extern sf::Texture mainMenuTitleTextTexture;
extern sf::Texture mainMenuBackTexture;

extern sf::Sprite mainMenuBackgroundSprite;
extern sf::Sprite mainMenuTitleSprite;
extern sf::Sprite mainMenuBackSprite;


extern sf::Texture mainMenuSingleplayerTexture;
extern sf::Texture mainMenuMultiplayerTexture;
extern sf::Texture mainMenuExitTexture;

extern sf::Sprite mainMenuSingleplayerSprite;
extern sf::Sprite mainMenuMultiplayerSprite;
extern sf::Sprite mainMenuExitSprite;


extern sf::Texture mainMenuPlayTexture;
extern sf::Sprite mainMenuPlaySprite;

extern sf::Texture mainMenuPlayMountainLevelTexture;
extern sf::Sprite mainMenuPlayMountainLevelSprite;

extern sf::Texture serverSelectBoxTexture;
extern sf::Sprite serverSelectBoxSprite;

extern sf::Clock menuClock;
extern sf::Time timeUntilCanAct;

struct rememberedServersList
{
	sf::IpAddress IPAddress;
	unsigned short port;

	sf::Texture iconTexture;
	sf::Sprite iconSprite;

	sf::Sprite selectionBox;
	
	std::string name;
	std::string messageOfTheDay;
	std::string description;
};

struct selectedCharacter
{
	sf::Sprite character;
	sf::Sprite ability1;
	sf::Sprite ability2;
	sf::Sprite ability3;

	std::string tooltip1;
	std::string tooltip2;
	std::string tooltip3;
};

extern std::vector < rememberedServersList > knownServers;

#endif