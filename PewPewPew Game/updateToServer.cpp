#include "stdafx.h"
#include "player.h"
#include "globalVariables.h"
#include "enemies.h"
#include "updateToServer.h"

#include <iostream>
#include <stdio.h>
#include <SFML/Network.hpp>
/*
void updateToServer(int _packetHeader)
{
	outPacket.clear();
	if (_packetHeader == 0)
	{
		packetHeader = 0;

		if (wasConnectedTo == false)
		{
			std::string connectionPassword = "applesbananascoconutsdontexist";
			outPacket << packetHeader << connectionPassword << username << whichTeam;

			if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
			{
				//std::cout << "Error sending authentication to server!\n";
				messagingStruct newMessage;

				newMessage.message = "Error sending authentication to server!";
				newMessage.team = 0;

				messagesList.push_back(newMessage);
			}
			else
			{
				//std::cout << "Sent authentication code to server!\n";

				messagingStruct newMessage;

				newMessage.message = "Sent authentication code to server!";
				newMessage.team = 0;

				messagesList.push_back(newMessage);

				wasConnectedTo = true;
			}
		}
		else
		{
			std::string connectionPassword = "vaguewhitexrayyellowcakeszoneout";
			outPacket << packetHeader << connectionPassword << username << whichTeam;

			if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
			{
				std::cout << "Error properly disconnecting from server!\n";
			}
			else
			{
				std::cout << "Disconnected from server!\n";
			}
		}

		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match function arguements!\n";
	}
}
void updateToServer(int _packetHeader, std::string textMessage, int _teamAffiliation)
{
	outPacket.clear();
	if (_packetHeader == 1)
	{
		packetHeader = 1;

		outPacket << packetHeader << textMessage << _teamAffiliation;
		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			std::cout << "Error sending message to server!\n";
		}

		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match function arguements!\n";
	}
}
void updateToServer(int _packetHeader, sf::Vector2f newPlayerPosition, sf::Vector2f newPlayerVelocity)
{
	outPacket.clear();
	if (_packetHeader == 2)
	{
		packetHeader = 2;
		outPacket << packetHeader << newPlayerPosition.x << newPlayerPosition.y << newPlayerVelocity.x << newPlayerVelocity.y;

		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			std::cout << "Error updating player's position & velocity to server!\n";
		}
		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match function arguements!\n";
	}
}
void updateToServer(int _packetHeader, int _redTeamKills, int _blueTeamKills)
{
	outPacket.clear();
	if (_packetHeader == 3)
	{
		packetHeader = 3;
		outPacket << packetHeader << redTeamKills << blueTeamKills;

		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			std::cout << "Error updating score to server!\n";
		}
		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match function arguements!\n";
	}
}
void updateToServer(int _packetHeader, myPlayerNamespace::projectile _projectile)
{
	outPacket.clear();
	if (_packetHeader == 4)
	{
		packetHeader = 4;
		outPacket << packetHeader << _projectile.bulletType << bulletStartPos.x << bulletStartPos.y << _projectile.velocity.x << _projectile.velocity.y << _projectile.teamAffiliationBullet << _projectile.destroyBullet;

		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			std::cout << "Error updating bullets to server!\n";
		}
		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match function arguements!\n";
	}
}*/

void updateBackendToServer(unsigned int _packetSubHeader, std::string _code, std::string _username, int _team)
{
	outPacket.clear();
	packetHeader = 0;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for backend confirmation packets!\n";
	}

	if (wasConnectedTo == false)
	{
		std::string connectionPassword = "applesbananascoconutsdontexist";
		outPacket << packetHeader << packetSubHeader << connectionPassword << _username << _team;

		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			messagingStruct newMessage;
			newMessage.message = "Error sending authentication to server!";
			newMessage.team = 0;
			messagesList.push_back(newMessage);
		}
		else
		{
			messagingStruct newMessage;
			newMessage.message = "Sent authentication code to server!";
			newMessage.team = 0;
			messagesList.push_back(newMessage);
			wasConnectedTo = true;
		}
	}
	else
	{
		std::string connectionPassword = "vaguewhitexrayyellowcakeszoneout";
		outPacket << packetHeader << packetSubHeader << connectionPassword << _username << _team;
		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			std::cout << "Error properly disconnecting from server!\n";
		}
		else
		{
			std::cout << "Disconnected from server!\n";
		}
	}
	outPacket.clear();
}
void updateBackendToServer(unsigned int _packetSubHeader, unsigned int _characterSelected, unsigned int _localPlayerNumber, int _team)
{
	outPacket.clear();
	packetHeader = 0;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for character selection packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _localPlayerNumber << _characterSelected << _team;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending selected character to server!\n";
	}
	outPacket.clear();
}
void updateBackendToServer(unsigned int _packetSubHeader, sf::Time _timeSpentInCurrentLevel)
{
	outPacket.clear();
	packetHeader = 0;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for timestamp packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _timeSpentInCurrentLevel.asMicroseconds();
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending current time to server!\n";
	}
	outPacket.clear();
}
//Complete control over abilities + sprite - Add properly
void updateBackendToServer(unsigned int _packetSubHeader, unsigned int _characterSelected, unsigned int ability1,
	unsigned int ability2, unsigned int ability3, unsigned int ability4, unsigned int ability5, unsigned int ability6, 
	unsigned int ability7, unsigned int ability8, unsigned int ability9, unsigned int ability10, int _team)
{

}




void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage)
{
	outPacket.clear();
	packetHeader = 1;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for chat message packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _chatMessage;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending message!\n";
	}
	outPacket.clear();
}
void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage, int _team)
{
	outPacket.clear();
	packetHeader = 1;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for team chat message packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _chatMessage << _team;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending team chat message!\n";
	}
	outPacket.clear();
}
void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage, unsigned int _playerNumber)
{
	outPacket.clear();
	packetHeader = 1;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for whisper message packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _chatMessage << _playerNumber;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending whisper message!\n";
	}
	outPacket.clear();
}
void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage, bool _spectatorsOnly)
{
	outPacket.clear();
	packetHeader = 1;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 3)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for spectator chat message packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _chatMessage << _spectatorsOnly;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending spectator message!\n";
	}
	outPacket.clear();
}





void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, std::string _username, int _team, int _characterType)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for player spawn packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _username << _team << _characterType;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending player spawn packet!\n";
	}
	outPacket.clear();
}
void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, 
	float _posX, float _posY, int _directionFacing, int _currentFrameX, int _currentFrameY, float _health)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for player update packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _posX << _posY << _directionFacing << _currentFrameX << _currentFrameY << _health;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending player update packet!\n";
	}
	outPacket.clear();
}
void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, sf::Time _deathTime)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for player death packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _deathTime.asMilliseconds();
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending player death packet!\n";
	}
	outPacket.clear();
}
void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _characterType, unsigned int _textureNumber, unsigned int _textureSubNumber)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 3)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for texture change packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _characterType << _textureNumber << _textureSubNumber;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending texture change packet!\n";
	}
	outPacket.clear();
}




void updateScoreToServer(unsigned int _packetSubHeader, int _changeInScore, int _team)
{
	outPacket.clear();
	packetHeader = 3;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for score packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _changeInScore << _team;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending score message!\n";
	}
	outPacket.clear();
}
void updateScoreToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _changeInResource, int _resource, int _team)
{
	outPacket.clear();
	packetHeader = 3;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for resource packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _changeInResource << _resource << _team;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending resource message!\n";
	}
	outPacket.clear();
}




void updateParticlesToServer(unsigned int _packetSubHeader, sf::Vector2f _bulletPos, sf::Vector2f _velocity, int _team, float _damageDealt)
{
	outPacket.clear();
	packetHeader = 4;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for arrow packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _bulletPos.x << _bulletPos.y << _velocity.x << _velocity.y << _team << _damageDealt;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending arrow message!\n";
	}
	outPacket.clear();
}
//TODO - Consider: Should the particles store their damage values? or Retreive dynamically from client?
void updateParticlesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _particleType, unsigned int _posY)
{
	outPacket.clear();
	packetHeader = 4;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for particle spawn packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _particleType << _posY;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending particle spawn message!\n";
	}
	outPacket.clear();
}
//TODO - Add projectile & particle destroy server update functions




void updateAbilitiesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownTime, float _damageDealt, sf::Time _timeCharged)
{
	outPacket.clear();
	packetHeader = 5;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for particle spawn packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _ability << _abilityNumber << _cooldownTime.asMilliseconds() << _damageDealt << _timeCharged.asMilliseconds();
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending particle spawn message!\n";
	}
	outPacket.clear();
}
void updateAbilitiesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownLeft)
{
	outPacket.clear();
	packetHeader = 5;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for ability cooldown packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _ability << _abilityNumber << _cooldownLeft.asMilliseconds();
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending ability cooldown message!\n";
	}
	outPacket.clear();
}


void updateAbilitiesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, bool _isStarting, bool _isDoing, bool _isEnding, bool _isActive)
{
	outPacket.clear();
	packetHeader = 5;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for ability management packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _ability << _abilityNumber << _isStarting << _isDoing << _isEnding << _isActive;
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending ability management message!\n";
	}
	outPacket.clear();
}


void updateStatusEffectsToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, unsigned int _effectID, int _value, float _damageDealt, sf::Time _durationLeft)
{
	outPacket.clear();
	packetHeader = 6;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for status effect packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _effectID << _value << _damageDealt << _durationLeft.asMilliseconds();
	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending ability cooldown message!\n";
	}
	outPacket.clear();
}