#include "stdafx.h"
#include "enemies.h"
#include "globalVariables.h"
#include "player.h"
#include "updateToServer.h"
#include <vector>
#include <windows.h>
#include <iostream>
#include <random>
#include <time.h>

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics.hpp>

namespace enemyMonster
{
	enemyMonsterBase::enemyMonsterBase(const sf::Texture& imagePath, float _posX, float _posY) : enemySprite(imagePath)
	{
		
		enemySprite.setPosition(_posX, _posY);
		if (targetsCount % 2 == 0)
		{
			enemySprite.setTextureRect(sf::IntRect(0,0,32,48));
			directionFacing = 0;
		}
		else 
		{
			enemySprite.setTextureRect(sf::IntRect(32,0,32,48));
			directionFacing = 1;
		}
		targetsCount++;

		destroyMonster = false;
		isJumping = true;
		goingDown = true;

		health = 2;

		movementSpeed = 4.f;
		enemyJumpSpeed = 10.f;
		enemyFallSpeed = 5.f;
	}

	enemyMonsterBase::~enemyMonsterBase()
	{
		//TODO Auto-generated destructor stub
	}

	void enemyMonsterBase::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		target.draw(enemySprite, states);
	}
	void enemyMonsterBase::update()
	{
		//Set the monster to have gravity when it spawns, so it can fall
		if (enemyMonsterBase::isJumping == true)
		{
					enemyMonsterBase::goingDown = true;
		}

		//Collision detection for terrain
		//Gravity
		if (((map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].x) != 0 || 
			(map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].y) != 0 ))
		{
			if (((map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].x) == 0 && 
				(map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].y) == 5)
				|| ((map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].x) == 5 && 
				(map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].y) == 4)
				|| ((map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].x) == 7 && 
				(map[(int)(enemyMonsterBase::enemySprite.getPosition().y+48)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x)/32].y) == 4))
			{
				//Exception - I want them to fall through the cloud tiles
			}
			else
			{
				enemyMonsterBase::isJumping = false;
				enemyMonsterBase::goingDown = false;
			}
		}
		else
		{
			enemyMonsterBase::isJumping = true;
			enemyMonsterBase::goingDown = true;
		}

		//Kill enemies stuck inside walls (not working ... ? )
		if (((map[(int)(enemyMonsterBase::enemySprite.getPosition().y+20)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x+16)/32].x) != 0 
			&& (map[(int)(enemyMonsterBase::enemySprite.getPosition().y+20)/32][(int)(enemyMonsterBase::enemySprite.getPosition().x+16)/32].y) != 0 ))
		{
			enemyMonsterBase::destroyMonster = true;
		}

		if (goingLeft == true)
		{
			enemyMonsterBase::enemySprite.move(-movementSpeed, 0.f);
			if ((map[(int)(enemySprite.getPosition().y+7)/32][(int)(enemySprite.getPosition().x+7)/32].x) != 0 || 
				(map[(int)(enemySprite.getPosition().y+7)/32][(int)(enemySprite.getPosition().x+7)/32].y) != 0)
			{
				enemyMonsterBase::destroyMonster = true;
			}
		}

		if (goingRight == true)
		{
			enemyMonsterBase::enemySprite.move(movementSpeed, 0.f);
			if ((map[(int)(enemySprite.getPosition().y+7)/32][(int)(enemySprite.getPosition().x+7)/32].x) != 0 || 
				(map[(int)(enemySprite.getPosition().y+7)/32][(int)(enemySprite.getPosition().x+7)/32].y) != 0)
			{
				enemyMonsterBase::destroyMonster = true;
			}
		}

		//Collision detection for bullets

		for (unsigned int i = 0; i < bulletsList.size(); i++)
		{
			if (enemySprite.getGlobalBounds().intersects(bulletsList[i].mSprite.getGlobalBounds()))
			{
				bulletsList[i].destroyBullet = true;
				enemyMonsterBase::health -= 1;
				if (enemyMonsterBase::health <= 0)
				{
					enemyMonsterBase::destroyMonster = true;
					if (bulletsList[i].teamAffiliationBullet == 1)
					{
						redTeamKills++;
					}
					else
					{
						blueTeamKills++;
					}
					std::cout << "Red team kills: " << redTeamKills << " Blue team kills: " << blueTeamKills << std::endl;
					//updateToServer(3, redTeamKills, blueTeamKills);
				}
				if (enemyMonsterBase::directionFacing == 0)
				{
					enemySprite.setTextureRect(sf::IntRect(0,48,32,48));
				}
				else
				{
					enemySprite.setTextureRect(sf::IntRect(32,48,32,48));
				}

			}
		}

		for (unsigned int i = 0; i < targetsList.size(); i++)
		{
			if (targetsList[i].destroyMonster == true)
			{
				targetsList.erase(targetsList.begin() +i);
				i--;
			}
		}




		//Move the sprite

		if (enemyMonsterBase::goingDown == true)
		{
			enemyMonsterBase::enemySprite.move(0.f, enemyFallSpeed);
		}
	}

	void enemyMonsterBase::moveLeft()
	{
		enemySprite.move(-movementSpeed, 0.f);
	}
	void enemyMonsterBase::moveRight()
	{
		enemySprite.move(movementSpeed, 0.f);
	}
	void enemyMonsterBase::moveUp()
	{
		enemySprite.move(0.f, -enemyJumpSpeed);
	}
	void enemyMonsterBase::moveDown()
	{
		enemyMonsterBase::enemySprite.move(0.f, enemyFallSpeed);
	}
};

std::vector < enemyMonster::enemyMonsterBase > targetsList;