#ifndef AUDIOMANAGER_CPP_
#define AUDIOMANAGER_CPP_

#include <SFML/Audio.hpp>

extern void initializeSoundEffects();

//Sound Effects
extern sf::SoundBuffer bowShootSoundBuffer;
extern sf::Sound bowShootSound;

extern sf::SoundBuffer arrowImpactSoundBuffer;
extern sf::Sound arrowImpactSound;

extern sf::SoundBuffer rubbleExplosionSoundBuffer;
extern sf::Sound rubbleExplosionSound;

extern sf::SoundBuffer playerJumpSoundBuffer;
extern sf::Sound playerJumpSound;

extern sf::SoundBuffer playerHitGroundSoundBuffer;
extern sf::Sound playerHitGroundSound;

extern sf::SoundBuffer hawkWingsFlapSoundBuffer;
extern sf::Sound HawkWingsFlapSound;

//Music
extern sf::Music ambientAbyssMusic;
extern sf::Music sunflowerDanceMusic;

struct soundEffect
{
	sf::SoundBuffer soundEffectBuffer;
	sf::Time timeSinceAnimChange;

	void update();
};

extern std::vector < soundEffect > soundEffectsList;
#endif