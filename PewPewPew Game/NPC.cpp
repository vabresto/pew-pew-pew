#include "stdafx.h"
#include "player.h"
#include "globalVariables.h"
#include <random>

void spawnWarrior(sf::Vector2f _position)
{
	playerStatsClient newPlayerStats;

	newPlayerStats.playerName = "Warrior Guy";
	newPlayerStats.team = 2;
	velocity.x = 0.f;
	velocity.y = 0.f;

	myPlayerNamespace::player newPlayer(warriorGuyTexture, blueTeamSpawnX, blueTeamSpawnY, newPlayerStats.team, true);

	newPlayer.mSprite.setPosition(blueTeamSpawnX, blueTeamSpawnY);
	newPlayer.playerVelocity.x = velocity.x;
	newPlayer.playerVelocity.y = velocity.y;
	newPlayer.playerName = newPlayerStats.playerName;

	newPlayer.currentFrameX = 1;
	newPlayer.currentFrameY = 1;

	newPlayer.healthMax = 50.f;
	newPlayer.health = newPlayer.healthMax;

	newPlayer.characterTexture = 2;

	newPlayer.NPCType = 1;

	newPlayer.abilitiesVector[7].ability = 1;
	newPlayer.abilitiesVector[7].cooldownTime = sf::seconds((float)0.35);
	newPlayer.abilitiesVector[7].hasChannel = true;
	newPlayer.abilitiesVector[7].channelTime = sf::seconds((float)0.2);
	newPlayer.abilitiesVector[7].channelLeft = sf::seconds((float)0.2);
	newPlayer.abilitiesVector[7].canCharge = true;
	newPlayer.abilitiesVector[7].hasPersistentEffect = false;
	newPlayer.abilitiesVector[7].isStartingAbility = false;
	newPlayer.abilitiesVector[7].isDoingAbility = false;
	newPlayer.abilitiesVector[7].isEndingAbility = false;
	newPlayer.abilitiesVector[7].damageDealt = 15.f;

	newPlayer.abilitiesVector[8].ability = 1;
	newPlayer.abilitiesVector[8].cooldownTime = sf::seconds(14.f);
	newPlayer.abilitiesVector[8].durationTotal = sf::seconds(4.f);
	newPlayer.abilitiesVector[8].hasChannel = false;
	newPlayer.abilitiesVector[8].canCharge = false;
	newPlayer.abilitiesVector[8].hasPersistentEffect = true;
	newPlayer.abilitiesVector[8].isStartingAbility = false;
	newPlayer.abilitiesVector[8].isDoingAbility = false;
	newPlayer.abilitiesVector[8].isEndingAbility = false;
	newPlayer.abilitiesVector[8].damageDealt = 0.f;

	/*statusEffectStruct newEffect;
	newEffect.effectID = 0;
	newEffect.value = 0;*/

	playersList.push_back(newPlayer);

	

	//playersList[playersList.size()].statusEffectsVector.push_back(newEffect);

	blueTeamPlayerStatsClient.push_back(newPlayerStats);

}
void controlNPC(unsigned int _positionInPlayersList)
{
	if (playersList[_positionInPlayersList].NPCType == 1)
	{
		if (playersList[_positionInPlayersList].NPCState == 1)
		{
			for (unsigned int i = 0; i < bulletsList.size(); i++)
			{
				if (bulletsList[i].destroyBullet == false && bulletsList[i].teamAffiliationBullet != playersList[_positionInPlayersList].teamAffiliation)
				{
					if ((playersList[_positionInPlayersList].mSprite.getPosition().x + 50) > bulletsList[i].mSprite.getPosition().x &&
						((playersList[_positionInPlayersList].mSprite.getPosition().x) < bulletsList[i].mSprite.getPosition().x))
					{
						/*if (playersList[_positionInPlayersList].playerVelocity.x < playersList[_positionInPlayersList].playerMaxVelocity.x)
						{*/
							playersList[_positionInPlayersList].playerVelocity.x -= 1.f;
							playersList[_positionInPlayersList].directionFacing = 0;

							playersList[_positionInPlayersList].currentFrameX = 1;
							playersList[_positionInPlayersList].currentFrameY = 1;
						/*}
						else
						{*/
							playersList[_positionInPlayersList].playerVelocity.x = -3.f;
							playersList[_positionInPlayersList].NPCState = 2;
						//}
					}
					else if ((playersList[_positionInPlayersList].mSprite.getPosition().x - 50) < bulletsList[i].mSprite.getPosition().x &&
						((playersList[_positionInPlayersList].mSprite.getPosition().x) > bulletsList[i].mSprite.getPosition().x))
					{
						/*if (playersList[_positionInPlayersList].playerVelocity.x > -playersList[_positionInPlayersList].playerMaxVelocity.x)
						{*/
							playersList[_positionInPlayersList].directionFacing = 1;
							playersList[_positionInPlayersList].playerVelocity.x += 1.f;

							playersList[_positionInPlayersList].currentFrameX = 1;
							playersList[_positionInPlayersList].currentFrameY = 1;
						/*}
						else
						{*/
							playersList[_positionInPlayersList].playerVelocity.x = 3.f;
							playersList[_positionInPlayersList].NPCState = 2;
						//}
					}
				}
			}
		}
		else if (playersList[_positionInPlayersList].NPCState == 2)
		{
			playersList[_positionInPlayersList].playerVelocity.x = 0.f;
			playersList[_positionInPlayersList].NPCState = 1;
		}
		else if (playersList[_positionInPlayersList].NPCState == 3)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 4)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 5)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 6)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 7)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 8)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 9)
		{

		}
		else if (playersList[_positionInPlayersList].NPCState == 10)
		{

		}
	}
}