#include "stdafx.h"
#include "tallMountainGem.h"
#include "enemies.h"
#include "globalVariables.h"
#include "player.h"
#include "updateToServer.h"

#include <iostream>

namespace enemyMonster
{
	tallMountainGem::tallMountainGem(const sf::Texture& mountainGemTexture, float _posX, float _posY)
	{
		gemSprite.setPosition(_posX, _posY);
		gemSprite.setTexture(mountainGemTexture);
	}

	tallMountainGem::~tallMountainGem()
	{
		//TODO
	}

	void tallMountainGem::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		target.draw(gemSprite, states);
	}

	void tallMountainGem::update()
	{
		if ( view1.getCenter().y /32 > 650)
		{
			gemList[0].gemSprite.setPosition(view1.getCenter().x - 24, view1.getCenter().y - 200);
		}
		else
		{
			gemList[0].gemSprite.setPosition(73 * 32, 633 *32);
		}

		for (unsigned int i = 0; i < playersList.size(); i++)
		{
			if (playersList[localPlayerNumber].mSprite.getGlobalBounds().intersects(gemList[0].gemSprite.getGlobalBounds()))
			{
				if (playersList[i].teamAffiliation == 1)
				{
					redTeamKills++;
				}
				else if (playersList[i].teamAffiliation == 2)
				{
					blueTeamKills++;
				}

				/*std::cout << "Red team kills: " << redTeamKills << " Blue team kills: " << blueTeamKills << std::endl;
				if (isGameLocal == false)
				{
					updateToServer(3, redTeamKills, blueTeamKills);
				}
				gemList.clear();*/
			}
		}
	}
}

std::vector < enemyMonster::tallMountainGem > gemList;