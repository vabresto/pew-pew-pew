#ifndef ROGUE_CPP_
#define ROGUE_CPP_

extern void givePlayerRogueSlashAbility(int _playerNumber);
extern void givePlayerRogueDashAbility(int _playerNumber);

extern void rogueSlashStart(unsigned int actor);
extern void rogueSlash(unsigned int actor);
extern void rogueSlashEnd(unsigned int actor);

extern void rogueDashStart(unsigned int actor);
extern void rogueDash(unsigned int actor);
extern void rogueDashEnd(unsigned int actor);

#endif