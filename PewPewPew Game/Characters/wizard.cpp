#include "stdafx.h"
#include "wizard.h"
#include <SFML/Graphics.hpp>
#include "../globalVariables.h"
#include "../player.h"
#include "../updateToServer.h"
#include "../audioManager.h"
#include "../animationManager.h"

void givePlayerShootFireballAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[7].ability = 15;
	playersList[_playerNumber].abilitiesVector[7].cooldownTime = sf::seconds((float)1);
	
	playersList[_playerNumber].abilitiesVector[7].channelTime = sf::seconds((float)0.2);
	playersList[_playerNumber].abilitiesVector[7].channelLeft = sf::seconds((float)0.2);

	playersList[_playerNumber].abilitiesVector[7].hasChannel = true;
	playersList[_playerNumber].abilitiesVector[7].canCharge = false;
	playersList[_playerNumber].abilitiesVector[7].hasPersistentEffect = false;
	playersList[_playerNumber].abilitiesVector[7].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].damageDealt = 10.f;
}
void givePlayerThunderclapAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[8].ability = 15;
	playersList[_playerNumber].abilitiesVector[8].cooldownTime = sf::seconds(14.f);
	playersList[_playerNumber].abilitiesVector[8].durationTotal = sf::seconds(1.f);
	playersList[_playerNumber].abilitiesVector[8].hasChannel = true;
	playersList[_playerNumber].abilitiesVector[8].canCharge = true;
	playersList[_playerNumber].abilitiesVector[8].hasPersistentEffect = false;
	playersList[_playerNumber].abilitiesVector[8].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].damageDealt = 20.f;

	playersList[_playerNumber].playerMaxVelocity.x = (float)2.8;
}

void shootFireballStart(unsigned int actor)
{
	if (playersList[actor].abilitiesVector[7].isActive == true && playersList[actor].abilitiesVector[7].channelLeft > sf::Time::Zero)
	{
		playersList[actor].isUsingAbilityAnimation = true;
		playersList[actor].abilitiesVector[7].channelLeft -= timePerFrame;

		playersList[actor].currentFrameX = 1;
		playersList[actor].currentFrameY = 4;
	}
	else
	{
		playersList[actor].abilitiesVector[7].isActive = false;

		playersList[actor].abilitiesVector[7].isStartingAbility = false;
		playersList[actor].abilitiesVector[7].isDoingAbility = true;
		playersList[actor].abilitiesVector[7].isEndingAbility = false;
	}
}
void shootFireball(unsigned int actor)
{
	spawnFireballParticles(actor);

	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 8);
	}

	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = false;
	playersList[actor].abilitiesVector[7].isEndingAbility = true;
}
void shootFireballEnd(unsigned int actor)
{
	playersList[actor].isUsingAbilityAnimation = false;

	playersList[actor].abilitiesVector[7].channelLeft = playersList[actor].abilitiesVector[7].channelTime;
	playersList[actor].abilitiesVector[7].cooldownLeft = playersList[actor].abilitiesVector[7].cooldownTime;

	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 2;

	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = false;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;
}

void thunderclapStart(unsigned int actor)
{
	if (playersList[actor].abilitiesVector[8].timeCharged == sf::Time::Zero)
	{
		spawnThunderclapMarkerParticles(actor);
		if (isGameLocal == false)
		{
			updateParticlesToServer(2, actor, 10);
		}
	}

	spawnThunderclapChargeParticles(actor);

	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 9);
	}

	//Charging
	playersList[actor].abilitiesVector[8].timeCharged += timePerFrame;
	playersList[actor].abilitiesVector[8].isActive = true;
	if (playersList[actor].abilitiesVector[8].timeCharged > sf::seconds(4.f))
	{
		playersList[actor].abilitiesVector[8].timeCharged = sf::seconds(4.f);
	}
	if (isGameLocal == false)
	{
		updateAbilitiesToServer(0, actor, 8, 15, playersList[actor].abilitiesVector[8].cooldownTime, playersList[actor].abilitiesVector[8].damageDealt, playersList[actor].abilitiesVector[8].timeCharged);
	}
}
void thunderclap(unsigned int actor)
{
	//Draw straight line down from marker to first solid block
	//Make the entire area be a lightning bolt attack for 0.25s

	spawnThunderclapCloudParticles(actor);

	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 11);
	}

	
	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		if (particleEntitiesList[i].particleType == 11 && particleEntitiesList[i].spawnerPlayerNumber == actor)
		{
			for (unsigned int j = 1; j < 15; j++)
			{
				spawnThunderclapParticles(j, actor);

				if (isGameLocal == false)
				{
					updateParticlesToServer(2, actor, 12, j);
				}
			}
		}
	}
	//spawnThunderclapParticles();



	playersList[actor].abilitiesVector[8].isActive = false;

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = false;
	playersList[actor].abilitiesVector[8].isEndingAbility = true;
}
void thunderclapEnd(unsigned int actor)
{
	//Clean up
	//Possibly add a cool sound effect

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = false;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;

	playersList[actor].abilitiesVector[8].cooldownLeft = playersList[actor].abilitiesVector[8].cooldownTime;
}