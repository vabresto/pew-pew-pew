#ifndef ARCHER_CPP_
#define ARCHER_CPP_

extern void givePlayerShootArrowAbility(int _playerNumber);
extern void givePlayerTransformHawkAbility(int _playerNumber);

extern void shootArrowStart(unsigned int actor);
extern void shootArrow(unsigned int actor);
extern void shootArrowEnd(unsigned int actor);

extern void transformHawkStart(unsigned int actor);
extern void transformHawk(unsigned int actor);
extern void transformHawkEnd(unsigned int actor);

#endif