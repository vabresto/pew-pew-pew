#include "stdafx.h"
#include "shieldGuy.h"
#include <SFML/Graphics.hpp>
#include "../globalVariables.h"
#include "../player.h"
#include "../updateToServer.h"
#include "../audioManager.h"
#include "../animationManager.h"

void givePlayerSwordSlashAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[7].ability = 8;
	playersList[_playerNumber].abilitiesVector[7].cooldownTime = sf::seconds((float)0.4);
	playersList[_playerNumber].abilitiesVector[7].hasChannel = false;
	playersList[_playerNumber].abilitiesVector[7].channelTime = sf::seconds((float)0);
	playersList[_playerNumber].abilitiesVector[7].channelLeft = sf::seconds((float)0);
	playersList[_playerNumber].abilitiesVector[7].canCharge = false;
	playersList[_playerNumber].abilitiesVector[7].hasPersistentEffect = false;
	playersList[_playerNumber].abilitiesVector[7].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].damageDealt = 10.f;

	/*statusEffectStruct newEffect;

	newEffect.damageDealt = 0;
	newEffect.effectID = 1;
	newEffect.value = 1;

	playersList[_playerNumber].statusEffectsVector.push_back(newEffect);*/
}
void givePlayerShieldBlockAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[8].ability = 8;
	playersList[_playerNumber].abilitiesVector[8].cooldownTime = sf::seconds(15.f);
	playersList[_playerNumber].abilitiesVector[8].durationTotal = sf::seconds(3.f);
	playersList[_playerNumber].abilitiesVector[8].hasChannel = false;
	playersList[_playerNumber].abilitiesVector[8].canCharge = false;
	playersList[_playerNumber].abilitiesVector[8].hasPersistentEffect = true;
	playersList[_playerNumber].abilitiesVector[8].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].damageDealt = 0.f;

	playersList[_playerNumber].playerMaxVelocity.x = 2.5;
}

void swordSlashUpStart(unsigned int actor)
{
	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 4;

	spawnSwordSlashUpParticles(actor);
	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 5);
	}

	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = true;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;
}
void swordSlashUp(unsigned int actor)
{
	if (playersList[actor].timeSinceAnimChange > sf::seconds((float)0.045))
	{
		playersList[actor].currentFrameX++;
		playersList[actor].timeSinceAnimChange = sf::Time::Zero;
	}
	if (playersList[actor].currentFrameX > 3)
	{
		playersList[actor].abilitiesVector[7].isStartingAbility = false;
		playersList[actor].abilitiesVector[7].isDoingAbility = false;
		playersList[actor].abilitiesVector[7].isEndingAbility = true;
	}	
}
void swordSlashUpEnd(unsigned int actor)
{
	playersList[actor].abilitiesVector[7].isActive = false;
	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = false;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;

	playersList[actor].currentFrameX = 4;
	playersList[actor].currentFrameY = 4;

	for (unsigned int i = 0; i < playersList[actor].statusEffectsVector.size(); i++)
	{
		if (playersList[actor].statusEffectsVector[i].effectID == 1)
		{
			playersList[actor].statusEffectsVector[i].value = 2;
			break;
		}
	}
}

void swordSlashDownStart(unsigned int actor)
{
	playersList[actor].currentFrameX = 4;
	playersList[actor].currentFrameY = 4;

	spawnSwordSlashDownParticles(actor);
	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 7);
	}

	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = true;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;
}
void swordSlashDown(unsigned int actor)
{
	if (playersList[actor].timeSinceAnimChange > sf::seconds((float)0.05))
	{
		playersList[actor].currentFrameX--;
		playersList[actor].timeSinceAnimChange = sf::Time::Zero;
	}
	if (playersList[actor].currentFrameX < 2)
	{
		playersList[actor].abilitiesVector[7].isStartingAbility = false;
		playersList[actor].abilitiesVector[7].isDoingAbility = false;
		playersList[actor].abilitiesVector[7].isEndingAbility = true;
	}	
}
void swordSlashDownEnd(unsigned int actor)
{
	playersList[actor].abilitiesVector[7].isActive = false;
	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = false;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;

	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 4;

	for (unsigned int i = 0; i < playersList[actor].statusEffectsVector.size(); i++)
	{
		if (playersList[actor].statusEffectsVector[i].effectID == 1)
		{
			playersList[actor].statusEffectsVector[i].value = 1;
			break;
		}
	}
}

void shieldBlockStart(unsigned int actor)
{
	playersList[actor].abilitiesVector[8].durationLeft = playersList[actor].abilitiesVector[8].durationTotal;

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = true;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;

	spawnShieldBlockParticles(actor);
	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 6);
	}
}
void shieldBlock(unsigned int actor)
{
	if (playersList[actor].abilitiesVector[8].durationLeft <= sf::Time::Zero)
	{
		playersList[actor].abilitiesVector[8].isStartingAbility = false;
		playersList[actor].abilitiesVector[8].isDoingAbility = false;
		playersList[actor].abilitiesVector[8].isEndingAbility = true;
	}
}
void shieldBlockEnd(unsigned int actor)
{
	playersList[actor].abilitiesVector[8].isActive = false;

	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 1;

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = false;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;
}