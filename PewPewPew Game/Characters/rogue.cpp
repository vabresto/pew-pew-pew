#include "stdafx.h"
#include "rogue.h"
#include <SFML/Graphics.hpp>
#include "../globalVariables.h"
#include "../player.h"
#include "../updateToServer.h"
#include "../audioManager.h"
#include "../animationManager.h"

void givePlayerRogueSlashAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[7].ability = 22;
	playersList[_playerNumber].abilitiesVector[7].cooldownTime = sf::seconds((float)0.25);
	playersList[_playerNumber].abilitiesVector[7].hasChannel = false;
	playersList[_playerNumber].abilitiesVector[7].channelTime = sf::seconds((float)0);
	playersList[_playerNumber].abilitiesVector[7].channelLeft = sf::seconds((float)0);
	playersList[_playerNumber].abilitiesVector[7].canCharge = false;
	playersList[_playerNumber].abilitiesVector[7].hasPersistentEffect = false;
	playersList[_playerNumber].abilitiesVector[7].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].damageDealt = 12.f;
}
void givePlayerRogueDashAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[8].ability = 22;
	playersList[_playerNumber].abilitiesVector[8].cooldownTime = sf::seconds(10.f);
	playersList[_playerNumber].abilitiesVector[8].durationTotal = sf::seconds(2.f);
	playersList[_playerNumber].abilitiesVector[8].hasChannel = false;
	playersList[_playerNumber].abilitiesVector[8].canCharge = false;
	playersList[_playerNumber].abilitiesVector[8].hasPersistentEffect = true;
	playersList[_playerNumber].abilitiesVector[8].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].damageDealt = 0.f;

	playersList[_playerNumber].playerMaxVelocity.x = (float)3.2;
}

void rogueSlashStart(unsigned int actor)
{
	playersList[actor].isUsingAbilityAnimation = true;
	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 4;

	spawnRogueSlashParticles(actor);

	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 13);
	}

	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = true;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;

	/*if (actor == localPlayerNumber)
	{
		if (isGameLocal == false)
		{
			updateAbilitiesToServer(2, localPlayerNumber, 7, playersList[localPlayerNumber].abilitiesVector[7].ability, playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[7].isActive);
		}
	}*/
}
void rogueSlash(unsigned int actor)
{
	if (playersList[actor].timeSinceAnimChange > sf::seconds((float)0.045))
	{
		playersList[actor].currentFrameX++;
		playersList[actor].timeSinceAnimChange = sf::Time::Zero;
	}
	if (playersList[actor].currentFrameX > 3)
	{
		playersList[actor].abilitiesVector[7].isStartingAbility = false;
		playersList[actor].abilitiesVector[7].isDoingAbility = false;
		playersList[actor].abilitiesVector[7].isEndingAbility = true;
	}	

	/*if (actor == localPlayerNumber)
	{
		if (isGameLocal == false)
		{
			updateAbilitiesToServer(2, localPlayerNumber, 7, playersList[localPlayerNumber].abilitiesVector[7].ability, playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[7].isActive);
		}
	}*/

}
void rogueSlashEnd(unsigned int actor)
{
	playersList[actor].abilitiesVector[7].isActive = false;
	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = false;
	playersList[actor].abilitiesVector[7].isEndingAbility = false;

	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 4;

	playersList[actor].isUsingAbilityAnimation = false;

	/*if (actor == localPlayerNumber)
	{
		if (isGameLocal == false)
		{
			updateAbilitiesToServer(2, localPlayerNumber, 7, playersList[localPlayerNumber].abilitiesVector[7].ability, playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[7].isActive);
		}
	}*/

}

void rogueDashStart(unsigned int actor)
{
	playersList[actor].abilitiesVector[8].durationLeft = playersList[actor].abilitiesVector[8].durationTotal;
	spawnRogueDashParticles(actor);

	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 14);
	}

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = true;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;

	playersList[actor].playerMaxVelocity.x = (float)6.4;
}
void rogueDash(unsigned int actor)
{
	if (playersList[actor].abilitiesVector[8].durationLeft <= sf::Time::Zero)
	{
		rogueDashEnd(actor);
		playersList[actor].abilitiesVector[8].isStartingAbility = false;
		playersList[actor].abilitiesVector[8].isDoingAbility = false;
		playersList[actor].abilitiesVector[8].isEndingAbility = true;
	}
}
void rogueDashEnd(unsigned int actor)
{
	playersList[actor].abilitiesVector[8].isActive = false;

	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 2;

	playersList[actor].playerMaxVelocity.x = (float)3.2;

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = false;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;
}