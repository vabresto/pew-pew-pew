#include "stdafx.h"
#include "archer.h"
#include <SFML/Graphics.hpp>
#include "../globalVariables.h"
#include "../player.h"
#include "../updateToServer.h"
#include "../audioManager.h"
#include "../animationManager.h"

void givePlayerShootArrowAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[7].ability = 1;
	playersList[_playerNumber].abilitiesVector[7].cooldownTime = sf::seconds((float)0.35);
	playersList[_playerNumber].abilitiesVector[7].hasChannel = true;
	playersList[_playerNumber].abilitiesVector[7].channelTime = sf::seconds((float)0.2);
	playersList[_playerNumber].abilitiesVector[7].channelLeft = sf::seconds((float)0.2);
	playersList[_playerNumber].abilitiesVector[7].canCharge = true;
	playersList[_playerNumber].abilitiesVector[7].hasPersistentEffect = false;
	playersList[_playerNumber].abilitiesVector[7].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[7].damageDealt = 15.f;
}
void givePlayerTransformHawkAbility(int _playerNumber)
{
	playersList[_playerNumber].abilitiesVector[8].ability = 1;
	playersList[_playerNumber].abilitiesVector[8].cooldownTime = sf::seconds(14.f);
	playersList[_playerNumber].abilitiesVector[8].durationTotal = sf::seconds(4.f);
	playersList[_playerNumber].abilitiesVector[8].hasChannel = false;
	playersList[_playerNumber].abilitiesVector[8].canCharge = false;
	playersList[_playerNumber].abilitiesVector[8].hasPersistentEffect = true;
	playersList[_playerNumber].abilitiesVector[8].isStartingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isDoingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].isEndingAbility = false;
	playersList[_playerNumber].abilitiesVector[8].damageDealt = 0.f;
}

void shootArrowStart(unsigned int actor)
{
	//Knocking Arrow
	if (playersList[actor].abilitiesVector[7].isActive == true && playersList[actor].abilitiesVector[7].channelLeft > sf::Time::Zero)
	{
		if (playersList[actor].isJumping == false)
		{
			playersList[actor].currentFrameX = 1;
			playersList[actor].currentFrameY = 4;
		}
		else if (playersList[actor].isJumping == true)
		{
			if (playersList[actor].playerVelocity.y > 0)
			{
				playersList[actor].currentFrameX = 11;
				playersList[actor].currentFrameY = 4;
			}
			if (playersList[actor].playerVelocity.y < 0)
			{
				playersList[actor].currentFrameX = 6;
				playersList[actor].currentFrameY = 4;
			}
		}

		playersList[actor].isShooting = false;
		playersList[actor].isUsingAbilityAnimation = true;
		playersList[actor].abilitiesVector[7].channelLeft -= timePerFrame;
	}

	//Aiming
	if (playersList[actor].abilitiesVector[7].isActive == true && playersList[actor].abilitiesVector[7].channelLeft <= sf::Time::Zero)
	{
		if (playersList[actor].isJumping == false)
		{
			playersList[actor].currentFrameX = 2;
			playersList[actor].currentFrameY = 4;
		}
		else if (playersList[actor].isJumping == true)
		{
			if (playersList[actor].playerVelocity.y > 0)
			{
				playersList[actor].currentFrameX = 12;
				playersList[actor].currentFrameY = 4;
			}
			if (playersList[actor].playerVelocity.y < 0)
			{
				playersList[actor].currentFrameX = 7;
				playersList[actor].currentFrameY = 4;
			}
		}
		playersList[actor].isShooting = true;
		playersList[actor].abilitiesVector[7].timeCharged += timePerFrame;
		if (playersList[actor].abilitiesVector[7].timeCharged > sf::seconds(2.f))
		{
			playersList[actor].abilitiesVector[7].timeCharged = sf::seconds(2.f);
		}

		spawnArrowChargeTimeParticles(actor);
		if (isGameLocal == false)
		{
			updateParticlesToServer(2, actor, 4);
			updateAbilitiesToServer(0, actor, 7, 1, playersList[actor].abilitiesVector[7].cooldownTime, playersList[actor].abilitiesVector[7].damageDealt, playersList[actor].abilitiesVector[7].timeCharged);

		}
	}
	/*if (isGameLocal == false)
	{
		updateCharacterToServer(1, actor, playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().x, playersList[actor].directionFacing, playersList[actor].currentFrameX, playersList[actor].currentFrameY, playersList[actor].health);
		updateAbilitiesToServer(0, actor, 7, 1, playersList[actor].abilitiesVector[7].cooldownTime, playersList[actor].abilitiesVector[7].damageDealt, playersList[actor].abilitiesVector[7].timeCharged);
	}*/
}
void shootArrow(unsigned int actor)
{
	if (playersList[actor].isJumping == false)
	{
		playersList[actor].currentFrameX = 3;
		playersList[actor].currentFrameY = 4;
	}
	else if (playersList[actor].isJumping == true)
	{
		if (playersList[actor].playerVelocity.y > 0)
		{
			playersList[actor].currentFrameX = 13;
			playersList[actor].currentFrameY = 4;
		}
		if (playersList[actor].playerVelocity.y < 0)
		{
			playersList[actor].currentFrameX = 8;
			playersList[actor].currentFrameY = 4;
		}
	}

	playersList[actor].isShooting = false;
	bowShootSound.play();
	playersList[actor].timeSinceAnimChange = sf::Time::Zero;

	if (playersList[actor].directionFacing == 0)
	{
		bulletStartPos.x = playersList[actor].mSprite.getPosition().x;
		bulletStartPos.y = playersList[actor].mSprite.getPosition().y + 20.f;

		if (playersList[actor].abilitiesVector[7].timeCharged.asSeconds() > 1.f)
		{
			bulletVel.x = -7 * playersList[actor].abilitiesVector[7].timeCharged.asSeconds();
		}
		else
		{
			bulletVel.x = -7;
		}
		bulletVel.y = 0;

		if (playersList[actor].abilitiesVector[7].timeCharged.asSeconds() < 1.f)
		{
			playersList[actor].abilitiesVector[7].timeCharged = sf::seconds(1.f);
		}

		bulletsList.push_back(myPlayerNamespace::projectile (bulletTexture, bulletStartPos, bulletVel, bulletCount, playersList[actor].teamAffiliation, playersList[actor].abilitiesVector[7].damageDealt * playersList[actor].abilitiesVector[7].timeCharged.asSeconds()));

		if (isGameLocal == false)
		{
			//updateToServer(4, myPlayerNamespace::projectile (bulletTexture, bulletStartPos, bulletVel, bulletCount, playersList[actor].teamAffiliation, playersList[actor].abilitiesVector[7].damageDealt * playersList[actor].abilitiesVector[7].timeCharged.asSeconds()));
			updateParticlesToServer(1, bulletStartPos, bulletVel, playersList[actor].teamAffiliation, playersList[actor].abilitiesVector[7].damageDealt * playersList[actor].abilitiesVector[7].timeCharged.asSeconds());
		}
	}
	else
	{
		bulletStartPos.x = playersList[actor].mSprite.getPosition().x + 25.f;
		bulletStartPos.y = playersList[actor].mSprite.getPosition().y + 20.f;

		if (playersList[actor].abilitiesVector[7].timeCharged.asSeconds() > 1.f)
		{
			bulletVel.x = 7 * playersList[actor].abilitiesVector[7].timeCharged.asSeconds();
		}
		else
		{
			bulletVel.x = 7;
		}
		bulletVel.y = 0;

		if (playersList[actor].abilitiesVector[7].timeCharged.asSeconds() < 1.f)
		{
			playersList[actor].abilitiesVector[7].timeCharged = sf::seconds(1.f);
		}

		bulletsList.push_back(myPlayerNamespace::projectile (bulletTexture, bulletStartPos, bulletVel, bulletCount, playersList[actor].teamAffiliation, playersList[actor].abilitiesVector[7].damageDealt * playersList[actor].abilitiesVector[7].timeCharged.asSeconds()));
		if (isGameLocal == false)
		{
			//updateToServer(4, myPlayerNamespace::projectile (bulletTexture, bulletStartPos, bulletVel, bulletCount, playersList[actor].teamAffiliation,playersList[actor].abilitiesVector[7].damageDealt * playersList[actor].abilitiesVector[7].timeCharged.asSeconds()));
			updateParticlesToServer(1, bulletStartPos, bulletVel, playersList[actor].teamAffiliation, playersList[actor].abilitiesVector[7].damageDealt * playersList[actor].abilitiesVector[7].timeCharged.asSeconds());
		}
	}
	bulletCount++;
	playersList[actor].playerVelocity.x = 0.f;


	playersList[actor].abilitiesVector[7].isStartingAbility = false;
	playersList[actor].abilitiesVector[7].isDoingAbility = false;
	playersList[actor].abilitiesVector[7].isEndingAbility = true;

	playersList[actor].abilitiesVector[7].channelLeft = playersList[actor].abilitiesVector[7].channelTime;
	playersList[actor].abilitiesVector[7].timeCharged = sf::Time::Zero;
	if (isGameLocal == false)
	{
		updateAbilitiesToServer(0, actor, 7, 1, playersList[actor].abilitiesVector[7].cooldownTime, playersList[actor].abilitiesVector[7].damageDealt, playersList[actor].abilitiesVector[7].timeCharged);
	}

	/*if (isGameLocal == false)
	{
		updateCharacterToServer(1, actor, playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().x, playersList[actor].directionFacing, playersList[actor].currentFrameX, playersList[actor].currentFrameY, playersList[actor].health);
		updateAbilitiesToServer(0, actor, 7, 1, playersList[actor].abilitiesVector[7].cooldownTime, playersList[actor].abilitiesVector[7].damageDealt, playersList[actor].abilitiesVector[7].timeCharged);
	}*/
}
void shootArrowEnd(unsigned int actor)
{
	if (playersList[actor].isJumping == false)
		{
			playersList[actor].currentFrameX = 4;
			playersList[actor].currentFrameY = 4;
		}
		else if (playersList[actor].isJumping == true)
		{
			if (playersList[actor].playerVelocity.y > 0)
			{
				playersList[actor].currentFrameX = 14;
				playersList[actor].currentFrameY = 4;
			}
			if (playersList[actor].playerVelocity.y < 0)
			{
				playersList[actor].currentFrameX = 9;
				playersList[actor].currentFrameY = 4;
			}
		}

	playersList[actor].isShooting = false;
	
	/*if (isGameLocal == false)
	{
		updateCharacterToServer(1, actor, playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().x, playersList[actor].directionFacing, playersList[actor].currentFrameX, playersList[actor].currentFrameY, playersList[actor].health);
		updateAbilitiesToServer(0, actor, 7, 1, playersList[actor].abilitiesVector[7].cooldownTime, playersList[actor].abilitiesVector[7].damageDealt, playersList[actor].abilitiesVector[7].timeCharged);
	}*/
}

void transformHawkStart(unsigned int actor)
{
	playersList[actor].abilitiesVector[8].durationLeft = playersList[actor].abilitiesVector[8].durationTotal;
	playersList[actor].mSprite.setTexture(hawkTexture);

	HawkWingsFlapSound.play();

	spawnHawkTransformParticles(actor);
	if (isGameLocal == false)
	{
		updateParticlesToServer(2, actor, 3);
		updateCharacterToServer(3, actor, 1, 2, 1);
	}

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = true;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;

}
void transformHawk(unsigned int actor)
{
	if (actor == localPlayerNumber)
	{
		if (playersList[actor].playerVelocity.y < 0)
		{
			scrollSpeed = 14.f;
		}
		else if (playersList[actor].playerVelocity.y > 0)
		{
			scrollSpeed = 6.f;
		}
	}

	if (playersList[actor].abilitiesVector[8].durationLeft <= sf::Time::Zero)
	{
		transformHawkEnd(actor);
		playersList[actor].abilitiesVector[8].isStartingAbility = false;
		playersList[actor].abilitiesVector[8].isDoingAbility = false;
		playersList[actor].abilitiesVector[8].isEndingAbility = true;
	}
}
void transformHawkEnd(unsigned int actor)
{
	//Hawk Transform Particles
	if (playersList[actor].abilitiesVector[8].durationLeft <= sf::Time::Zero)
	{
		spawnHawkTransformParticles(actor);

		if (isGameLocal == false)
		{
			updateParticlesToServer(2, actor, 3);
			updateCharacterToServer(3, actor, 1, 1, 1);
		}
	}
	playersList[actor].abilitiesVector[8].isActive = false;
	playersList[actor].mSprite.setTexture(playerTexture);

	playersList[actor].currentFrameX = 1;
	playersList[actor].currentFrameY = 1;

	playersList[actor].abilitiesVector[8].isStartingAbility = false;
	playersList[actor].abilitiesVector[8].isDoingAbility = false;
	playersList[actor].abilitiesVector[8].isEndingAbility = false;
}