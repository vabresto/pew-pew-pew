#ifndef WIZARD_CPP_
#define WIZARD_CPP_

extern void givePlayerShootFireballAbility(int _playerNumber);
extern void givePlayerThunderclapAbility(int _playerNumber);

extern void shootFireballStart(unsigned int actor);
extern void shootFireball(unsigned int actor);
extern void shootFireballEnd(unsigned int actor);

extern void thunderclapStart(unsigned int actor);
extern void thunderclap(unsigned int actor);
extern void thunderclapEnd(unsigned int actor);

#endif