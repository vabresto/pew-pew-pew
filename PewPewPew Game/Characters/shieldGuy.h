#ifndef SHIELDGUY_CPP_
#define SHIELDGUY_CPP_

extern void givePlayerSwordSlashAbility(int _playerNumber);
extern void givePlayerShieldBlockAbility(int _playerNumber);

extern void swordSlashUpStart(unsigned int actor);
extern void swordSlashUp(unsigned int actor);
extern void swordSlashUpEnd(unsigned int actor);

extern void swordSlashDownStart(unsigned int actor);
extern void swordSlashDown(unsigned int actor);
extern void swordSlashDownEnd(unsigned int actor);

extern void shieldBlockStart(unsigned int actor);
extern void shieldBlock(unsigned int actor);
extern void shieldBlockEnd(unsigned int actor);

#endif