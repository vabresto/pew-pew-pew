#ifndef MAPGENERATOR_CPP_
#define MAPGENERATOR_CPP_

extern void generateMap(unsigned int _mapSizeX, unsigned int _mapSizeY, float difficulty, std::string tilesetLocation);

#endif