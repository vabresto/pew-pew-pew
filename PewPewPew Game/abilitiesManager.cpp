#include "stdafx.h"
#include "abilitiesManager.h"
#include "player.h"
#include "globalVariables.h"
#include "updateToServer.h"

#include "Characters/archer.h"
#include "Characters/shieldGuy.h"
#include "Characters/wizard.h"
#include "Characters/rogue.h"

//Forward declare functions
void startAbility(int _abilityNum);
void doAbility(int _abilityNum);
void endAbility(int _abilityNum);
void doAllAbilities();

void startAbility(int _abilityNum, unsigned int actor)
{
	if (_abilityNum == 7)
	{
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 1)
		{
			shootArrowStart(actor);
		}
		else if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 8)
		{
			for (unsigned int i = 0; i < playersList[actor].statusEffectsVector.size(); i++)
			{
				if (playersList[actor].statusEffectsVector[i].effectID == 1)
				{
					if (playersList[actor].statusEffectsVector[i].value == 1)
					{
						swordSlashUpStart(actor);
					}
					else
					{
						swordSlashDownStart(actor);
					}
				}
			}
		}
		else if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 15)
		{
			shootFireballStart(actor);
		}
		else if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 22)
		{
			rogueSlashStart(actor);
		}
	}
	else if (_abilityNum == 8)
	{
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 1)
		{
			transformHawkStart(actor);
		}
		else if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 8)
		{
			shieldBlockStart(actor);
		}
		else if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 15)
		{
			thunderclapStart(actor);
		}
		else if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 22)
		{
			rogueDashStart(actor);
		}
	}
}
void doAbility(int _abilityNum, unsigned int actor)
{
	if (_abilityNum == 7)
	{
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 1)
		{
			shootArrow(actor);
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 8)
		{
			for (unsigned int i = 0; i < playersList[actor].statusEffectsVector.size(); i++)
			{
				if (playersList[actor].statusEffectsVector[i].effectID == 1)
				{
					if (playersList[actor].statusEffectsVector[i].value == 1)
					{
						swordSlashUp(actor);
					}
					else
					{
						swordSlashDown(actor);
					}
				}
			}
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 15)
		{
			shootFireball(actor);
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 22)
		{
			rogueSlash(actor);
		}
	}
	else if (_abilityNum == 8)
	{
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 1)
		{
			transformHawk(actor);
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 8)
		{
			shieldBlock(actor);
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 15)
		{
			thunderclap(actor);
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 22)
		{
			rogueDash(actor);
		}
	}
}
void endAbility(int _abilityNum, unsigned int actor)
{
	if (_abilityNum == 7)
	{
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 1)
		{
			shootArrowEnd(actor);
			playersList[actor].isUsingAbilityAnimation = false;
			playersList[actor].abilitiesVector[(unsigned int)_abilityNum].isActive = false;

			playersList[actor].abilitiesVector[7].isStartingAbility = false;
			playersList[actor].abilitiesVector[7].isDoingAbility = false;
			playersList[actor].abilitiesVector[7].isEndingAbility = false;
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 8)
		{
			for (unsigned int i = 0; i < playersList[actor].statusEffectsVector.size(); i++)
			{
				if (playersList[actor].statusEffectsVector[i].effectID == 1)
				{
					if (playersList[actor].statusEffectsVector[i].value == 1)
					{
						swordSlashUpEnd(actor);
					}
					else
					{
						swordSlashDownEnd(actor);
					}
				}
			}
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 15)
		{
			shootFireballEnd(actor);
			playersList[actor].isUsingAbilityAnimation = false;
			playersList[actor].abilitiesVector[(unsigned int)_abilityNum].isActive = false;

			playersList[actor].abilitiesVector[7].isStartingAbility = false;
			playersList[actor].abilitiesVector[7].isDoingAbility = false;
			playersList[actor].abilitiesVector[7].isEndingAbility = false;
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 22)
		{
			rogueSlashEnd(actor);
		}
	}
	else if (_abilityNum == 8)
	{
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 1)
		{
			transformHawkEnd(actor);
			playersList[actor].isUsingAbilityAnimation = false;
			playersList[actor].abilitiesVector[(unsigned int)_abilityNum].isActive = false;
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 8)
		{
			shieldBlockEnd(actor);
			playersList[actor].isUsingAbilityAnimation = false;
			playersList[actor].abilitiesVector[(unsigned int)_abilityNum].isActive = false;
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 15)
		{
			thunderclapEnd(actor);
			playersList[actor].isUsingAbilityAnimation = false;
			playersList[actor].abilitiesVector[(unsigned int)_abilityNum].isActive = false;

			playersList[actor].abilitiesVector[8].isStartingAbility = false;
			playersList[actor].abilitiesVector[8].isDoingAbility = false;
			playersList[actor].abilitiesVector[8].isEndingAbility = false;
		}
		if (playersList[actor].abilitiesVector[(unsigned int)_abilityNum].ability == 22)
		{
			rogueDashEnd(actor);
			playersList[actor].isUsingAbilityAnimation = false;
			playersList[actor].abilitiesVector[(unsigned int)_abilityNum].isActive = false;

			playersList[actor].abilitiesVector[8].isStartingAbility = false;
			playersList[actor].abilitiesVector[8].isDoingAbility = false;
			playersList[actor].abilitiesVector[8].isEndingAbility = false;
		}

		/*playersList[localPlayerNumber].abilitiesVector[8].isStartingAbility = false;
		playersList[localPlayerNumber].abilitiesVector[8].isDoingAbility = false;
		playersList[localPlayerNumber].abilitiesVector[8].isEndingAbility = false;
		/*if (playersList[localPlayerNumber].abilitiesVector[(unsigned int)_abilityNum].hasPersistentEffect == false)
		{
			shootArrowEnd();
			playersList[localPlayerNumber].isUsingAbilityAnimation = false;
			playersList[localPlayerNumber].abilitiesVector[(unsigned int)_abilityNum].isActive = false;

			playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility = false;
			playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility = false;
			playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility = false;
		}
		else
		{
			if (playersList[localPlayerNumber].abilitiesVector[(unsigned int)_abilityNum].durationLeft <= sf::Time::Zero)
			{

			}
		}*/
	}
}

void doAllAbilities(unsigned int actor)
{
	//Do primary abilities
	if(playersList[actor].abilitiesVector[7].isStartingAbility == true)
	{
		playersList[actor].abilitiesVector[7].cooldownLeft = playersList[actor].abilitiesVector[7].cooldownTime;
		startAbility(7, actor);
	}
	else if (playersList[actor].abilitiesVector[7].isDoingAbility == true)
	{
		doAbility(7, actor);
	}
	else if (playersList[actor].abilitiesVector[7].isEndingAbility == true)
	{
		endAbility(7, actor);
	}


	//Do secondary abilities
	if(playersList[actor].abilitiesVector[8].isStartingAbility == true)
	{
		playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft = playersList[localPlayerNumber].abilitiesVector[8].cooldownTime;
		startAbility(8, actor);

		if (playersList[actor].abilitiesVector[8].ability == 1 && playersList[actor].abilitiesVector[7].ability == 1)
		{
			playersList[actor].isShooting = false;
			playersList[actor].abilitiesVector[7].isActive = false;
		}
	}
	else if (playersList[actor].abilitiesVector[8].isDoingAbility == true)
	{
		doAbility(8, actor);
	}
	else if (playersList[actor].abilitiesVector[8].isEndingAbility == true)
	{
		endAbility(8, actor);
	}
}