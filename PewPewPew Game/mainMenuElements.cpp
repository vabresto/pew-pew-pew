#include "stdafx.h"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>
#include <fstream>

#include "mainMenuElements.h"
#include "globalVariables.h"
#include "updateToServer.h"
#include "serverCode.h"
#include "animationManager.h"
#include "mapGenerator.h"
#include "audioManager.h"
#include "drawGameToScreen.h"

#include "Characters/archer.h"
#include "Characters/shieldGuy.h"
#include "Characters/wizard.h"
#include "Characters/rogue.h"

//Functions
void mainMenu();
void singleplayerCampaign();
void initializeMenuText();
void startSingleplayerMap(sf::Vector2f _position, sf::Vector2f _velocity, std::string _mapLocation);
void serverSelectMenu();
void serverLobbyMenu(unsigned int _serverNumber);
void serverTeamAndNameSelect(unsigned int _serverNumber);
void endOfMultiplayerGameScreen();
void gameEndScreen();
void initializeSettings();
void characterSelectScreen();
void initializeCharacterIcons();

//Server list
std::vector < rememberedServersList > knownServers;

//Map location
std::string mapLocation;

//Textures and Sprites
sf::Texture mainMenuBackgroundTexture;
sf::Texture mainMenuTitleTextTexture;
sf::Texture mainMenuBackTexture;

sf::Sprite mainMenuBackgroundSprite;
sf::Sprite mainMenuTitleSprite;
sf::Sprite mainMenuBackSprite;


sf::Texture mainMenuSingleplayerTexture;
sf::Texture mainMenuMultiplayerTexture;
sf::Texture mainMenuExitTexture;

sf::Sprite mainMenuSingleplayerSprite;
sf::Sprite mainMenuMultiplayerSprite;
sf::Sprite mainMenuExitSprite;


sf::Texture mainMenuPlayTexture;
sf::Sprite mainMenuPlaySprite;
sf::Texture serverConnectTexture;
sf::Sprite serverConnectSprite;

sf::Texture mainMenuPlayMountainLevelTexture;
sf::Sprite mainMenuPlayMountainLevelSprite;
sf::Texture mainMenuPlayTutorialLevelTexture;
sf::Sprite mainMenuPlayTutorialLevelSprite;
sf::Texture mainMenuPlayTestLevelTexture;
sf::Sprite mainMenuPlayTestLevelSprite;

sf::Texture serverSelectBoxTexture;
sf::Sprite serverSelectBoxSprite;

sf::Texture lobbyTextBarTexture;
sf::Texture lobbyTextBoxTexture;

sf::Sprite lobbyTextBarSprite;
sf::Sprite lobbyTextBoxSprite;

sf::Texture redTeamButtonTexture;
sf::Texture blueTeamButtonTexture;

sf::Sprite redTeamButtonSprite;
sf::Sprite blueTeamButtonSprite;

sf::Texture readyButtonTexture;
sf::Sprite readyButtonSprite;

sf::Texture serverButtonTexture;
sf::Sprite serverButtonSprite;

sf::Texture spectatorButtonTexture;
sf::Sprite spectatorButtonSprite;

sf::Texture selectButtonTexture;
sf::Sprite selectButtonSprite;

sf::Texture selectScreenTexture;
sf::Sprite selectScreenSprite;

sf::Sprite characterPreviewSprite;

sf::Sprite abilityOnePreviewSprite;
sf::Sprite abilityTwoPreviewSprite;
sf::Sprite abilityThreePreviewSprite;
sf::Texture abilityOnePreviewTexture;
sf::Texture abilityTwoPreviewTexture;
sf::Texture abilityThreePreviewTexture;

sf::Texture archerButtonTexture;
sf::Sprite archerButtonSprite;
sf::Texture rogueButtonTexture;
sf::Sprite rogueButtonSprite;
sf::Texture warriorButtonTexture;
sf::Sprite warriorButtonSprite;
sf::Texture wizardButtonTexture;
sf::Sprite wizardButtonSprite;

//Clocks + Timekeeping
sf::Clock menuClock;
sf::Time timeUntilCanAct = sf::seconds(0.25);

//Other
unsigned int characterSelected = 1;

void mainMenu()
{
	timeSinceLastUpdate = sf::Time::Zero;
	menuClock.restart();

	int menuItemSelected = 1;
	redTeamKills = 0;
	blueTeamKills = 0;

	initializeSettings();

	initializeSoundEffects();

	while (gameWindow.isOpen())
    {
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;

        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
			{
                gameWindow.close();
				exit(0);
			}
			if (event.type == sf::Event::GainedFocus)
			{
				gameHasFocus = true;
			}
			if (event.type == sf::Event::LostFocus)
			{
				gameHasFocus = false;
			}
			if (event.type == sf::Event::KeyReleased && (timeSinceLastUpdate > timeUntilCanAct) && gameHasFocus == true)
			{
				if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
				{
					if (menuItemSelected > 1)
					{
						menuItemSelected--;
					}
					else
					{
						menuItemSelected = 3;
					}
				}
				if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
				{
					if (menuItemSelected < 3)
					{
						menuItemSelected++;
					}
					else
					{
						menuItemSelected = 1;
					}
				}
				if (event.key.code == sf::Keyboard::Escape)
				{
					gameWindow.close();
					exit(0);
				}
				if (event.key.code == sf::Keyboard::Return)
				{
					if (menuItemSelected == 1)
					{
						isGameLocal = true;
						singleplayerCampaign();	
					}
					else if (menuItemSelected == 2)
					{
						isGameLocal = false;
						serverSelectMenu();
					}
					else if (menuItemSelected == 3)
					{
						gameWindow.close();
						exit(0);
					}
				}
			}
        }

		sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
		sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

		if (mainMenuSingleplayerSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			menuItemSelected = 1;
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				isGameLocal = true;
				singleplayerCampaign();	
			}
		}


		if (menuItemSelected == 1)
		{
			mainMenuSingleplayerSprite.setScale((float)1.2, (float)1.2);
		}
		else
		{
			mainMenuSingleplayerSprite.setScale((float)1.f, (float)1.f);
		}


		if (mainMenuMultiplayerSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuMultiplayerSprite.setScale((float)1.2, (float)1.2);
			menuItemSelected = 2;
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				isGameLocal = false;

				{
					serverSelectMenu();
					/**************************************************/
					//Establishing connection to server
					/*************************************************/
					/*std::cout << "Which team would you like to join?\n[R]ed Team or [B]lue Team: ";
					std::cin >> temp;
					if (temp == 'r' || temp == 'R' || temp == '1')
					{
						whichTeam = 1;
					}
					else if (temp == 'b' || temp == 'B' || temp == '2')
					{
						whichTeam = 2;
					}
					else if (temp == 's')
					{
						runServer();
						return;
					}
					else
					{
						std::cout << "Invalid input!\n";
						playLevelOne();
					}

					std::cout << "Enter server's IP: ";
					std::cin >> recipientIPAddress;

					std::cout << "Enter server's connection port: ";
					std::cin >> UDPSendPort;

					std::cout << "Please enter your username: ";
					std::cin >> username;

					std::cout << "Would you like to enable the 'Manly Background'? [Y]es or [N]o: ";
					std::cin >> temp;

					std::fstream connectionInfoFile("connectionInfo.txt");
					if (connectionInfoFile.is_open())
					{
						connectionInfoFile >> UDPRecievePort;
					}
					connectionInfoFile.close();


					if (UDPSocket.bind(UDPRecievePort) != sf::Socket::Done)
					{
						std::cout << "Error connecting to port " << UDPSocket.getLocalPort() << "! \n";
					}

					UDPSocket.setBlocking(false);
					socketSelector.add(UDPSocket);

					updateToServer(0);


					//Implementing lobby system
					while (!startGame)
					{
						//Test for recieved packets
						while (UDPSocket.receive(inPacket, senderIPAddress, UDPRecievePort) == sf::Socket::Done)
						{
							if (inPacket >> packetHeader)
							{
								if (packetHeader == 0)
								{
									inPacket >> testData >> iAmPlayerNumber >> numOfRedPlayers >> numOfBluePlayers >> serverStartGameCode;
									if (serverStartGameCode == "totallysecrectpassword")
									{
										std::cout << "Received start game code from server!\n";
										startGame = true;
										inPacket.clear();
										messagesList.clear();
									}
								}
								else if (packetHeader == 1)
								{
									inPacket >> textMessage;

									if (textMessage == "simonsaysshello")
									{
										std::cout << "Recieved confirmation code from server.\n";
										inPacket.clear();
									}
									else
									{
										std::cout << textMessage << std::endl;
									}

									textMessage.clear();
									inPacket.clear();
								}
								else if (packetHeader == 2)
								{
									sf::Vector2f position, velocity;
									playerStatsClient newPlayerStats;


									inPacket >> newPlayerStats.computerIDValue >> newPlayerStats.playerName >> newPlayerStats.team >> position.x >> position.y >> velocity.x >> velocity.y;

									myPlayerNamespace::player newPlayer(playerTexture, position.x, position.y, newPlayerStats.team);

									newPlayer.mSprite.setPosition(position);
									newPlayer.playerVelocity.x = velocity.x;
									newPlayer.playerVelocity.y = velocity.y;
									newPlayer.playerID = newPlayerStats.computerIDValue;
									newPlayer.playerName = newPlayerStats.playerName;


									newPlayerStats.playerLevel = 0;

									newPlayerStats.characterStats[0] = 0;
									newPlayerStats.characterStats[1] = 0;
									newPlayerStats.characterStats[2] = 0;
									newPlayerStats.characterStats[3] = 0;
									newPlayerStats.characterStats[4] = 0;

									for (unsigned int i = 0; i < playersList.size(); i++)
									{
										if (playersList[i].playerID == newPlayer.playerID)
										{
											addPlayerToList = false;
										}
									}

									if (addPlayerToList == true)
									{
										playersList.push_back(newPlayer);

										if (newPlayerStats.team == 1)
										{
											redTeamPlayerStatsClient.push_back(newPlayerStats);
										}
										else if (newPlayerStats.team == 2)
										{
											blueTeamPlayerStatsClient.push_back(newPlayerStats);
										}
									}
									addPlayerToList = true;
									inPacket.clear();
								}
							}
						}

						//Test if user wants to send a message
						if (std::getline(std::cin, messageToBeSent))
						{
							updateToServer(1, messageToBeSent, whichTeam);
						}
					}

					//Start game code received from server
					startGame = false;

					//Load the map
					openMap(testData);*/
				}				
			}
		}
		if (menuItemSelected == 2)
		{
			mainMenuMultiplayerSprite.setScale((float)1.2, (float)1.2);
		}
		else
		{
			mainMenuMultiplayerSprite.setScale(1.f, 1.f);
		}

		if (mainMenuExitSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuExitSprite.setScale((float)1.2, (float)1.2);
			menuItemSelected = 3;
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				gameWindow.close();
				exit(0);
			}
		}
		if (menuItemSelected == 3)
		{
			mainMenuExitSprite.setScale((float)1.2, (float)1.2);
		}
		else
		{
			mainMenuExitSprite.setScale(1.f, 1.f);
		}

		gameWindow.setView(gameWindow.getDefaultView());

		gameWindow.clear();

		gameWindow.draw(mainMenuBackgroundSprite);
		gameWindow.draw(mainMenuTitleSprite);

		gameWindow.draw(mainMenuSingleplayerSprite);
		gameWindow.draw(mainMenuMultiplayerSprite);
		gameWindow.draw(mainMenuExitSprite);

		gameWindow.display();

	//playLevelOne();
    }
}
void singleplayerCampaign()
{
	unsigned int levelSelected = 1;
	sf::Vector2f position, velocity;

	timeSinceLastUpdate = sf::Time::Zero;
	menuClock.restart();
	//timeSpentInCurrentLevel = sf::Time::Zero;

	while (gameWindow.isOpen())
    {
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            // "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				gameWindow.close();
			}
			if (event.type == sf::Event::GainedFocus)
			{
				gameHasFocus = true;
			}
			if (event.type == sf::Event::LostFocus)
			{
				gameHasFocus = false;
			}
			//Keyboard controls for scrolling the menu
			if (event.type == sf::Event::KeyReleased && (timeSinceLastUpdate > timeUntilCanAct))
			{
				if (event.key.code == sf::Keyboard::A || event.key.code == sf::Keyboard::Left)
				{
					if (levelSelected > 1)
					{
						levelSelected--;
					}
					else
					{
						levelSelected = 3;
					}
				}
				if (event.key.code == sf::Keyboard::D || event.key.code == sf::Keyboard::Right)
				{
					if (levelSelected < 3)
					{
						levelSelected++;
					}
					else
					{
						levelSelected = 1;
					}
				}
				if (event.key.code == sf::Keyboard::Escape)
				{
					mainMenu();
				}
				if (event.key.code == sf::Keyboard::Return)
				{
					startSingleplayerMap(position, velocity, mapLocation);
				}
			}
        }

		sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
		sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

		//Back Button
		if (mainMenuBackSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuBackSprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				mainMenu();
			}
		}
		else
		{
			mainMenuBackSprite.setScale((float)1, (float)1);
		}

		//Play Button
		if (mainMenuPlaySprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuPlaySprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				closeWindow = false;
				startSingleplayerMap(position, velocity, mapLocation);	
			}
		}
		else
		{
			mainMenuPlaySprite.setScale((float)1, (float)1);
		}

		//Mountains Level Button
		if (mainMenuPlayMountainLevelSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuPlayMountainLevelSprite.setColor(sf::Color::Magenta);
			if (levelSelected == 1 && sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > (timeUntilCanAct)/(float)2))
			{
				closeWindow = false;
				startSingleplayerMap(position, velocity, mapLocation);
			}
			else if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				levelSelected = 1;
				timeSinceLastUpdate = sf::Time::Zero;
			}	
		}
		if (levelSelected == 1)
		{
			mainMenuPlayMountainLevelSprite.setColor(sf::Color::Green);
			mapLocation = "Resources/testPlatformerTallMountainLevel.txt";
			position.x = 800.f;
			position.y = 28480.f;


			redTeamSpawnX = 800.f;
			redTeamSpawnY = 28480.f;
			blueTeamSpawnX = 400.f;
			blueTeamSpawnY = 28480.f;

			enableWinCondition = 1;
			randomMapBeingPlayed = false;

			view1.setCenter(850.f, 28480.f);
		}
		else
		{
			mainMenuPlayMountainLevelSprite.setColor(sf::Color::Blue);
		}

		//Tutorial Level Button
		if (mainMenuPlayTutorialLevelSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuPlayTutorialLevelSprite.setColor(sf::Color::Magenta);
			if (levelSelected == 2 && sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > (timeUntilCanAct)/(float)2))
			{
				closeWindow = false;
				startSingleplayerMap(position, velocity, mapLocation);
			}
			else if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				levelSelected = 2;
				timeSinceLastUpdate = sf::Time::Zero;
			}
			
		}
		if (levelSelected == 2)
		{
			mainMenuPlayTutorialLevelSprite.setColor(sf::Color::Green);

			mapLocation = "Resources/arenaMap.txt";
			position.x = 520.f;
			position.y = 1440.f;

			redTeamSpawnX = 520.f;
			redTeamSpawnY = 1440.f;
			blueTeamSpawnX = 3200.f;
			blueTeamSpawnY = 1440.f;

			view1.setCenter(550.f, 1274.f);
		}
		else
		{
			mainMenuPlayTutorialLevelSprite.setColor(sf::Color::Blue);
		}

		//Test Level Button
		if (mainMenuPlayTestLevelSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuPlayTestLevelSprite.setColor(sf::Color::Magenta);
			if (levelSelected == 3 && sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > (timeUntilCanAct)/(float)2))
			{
				closeWindow = false;
				startSingleplayerMap(position, velocity, mapLocation);
			}
			else if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				levelSelected = 3;
				timeSinceLastUpdate = sf::Time::Zero;
			}
			
		}
		if (levelSelected == 3)
		{
			mainMenuPlayTestLevelSprite.setColor(sf::Color::Green);
			mapLocation = "Resources/randomMap.txt";
			position.x = 520.f;
			position.y = 12700.f;

			redTeamSpawnX = 520.f;
			redTeamSpawnY = 12700.f;
			blueTeamSpawnX = 300.f;
			blueTeamSpawnY = 12700.f;

			enableWinCondition = 350;
			randomMapBeingPlayed = true;

			view1.setCenter(550.f, 12434.f);
		}
		else
		{
			mainMenuPlayTestLevelSprite.setColor(sf::Color::Blue);
		}
		
		//Draw to window
		gameWindow.clear();

		gameWindow.draw(mainMenuBackgroundSprite);

		gameWindow.draw(mainMenuPlayMountainLevelSprite);
		gameWindow.draw(mainMenuPlayTutorialLevelSprite);
		gameWindow.draw(mainMenuPlayTestLevelSprite);

		gameWindow.draw(mainMenuBackSprite);
		gameWindow.draw(mainMenuPlaySprite);

		gameWindow.display();

	}
}
void initializeMenuText()
{
	initializeParticles();


	if (!arial.loadFromFile("Resources/arial.ttf"))
	{
		std::cout << "Error loading arial font!\n";
	}
	if(!mainMenuBackgroundTexture.loadFromFile("Resources/MainMenuTitleBackground.png"))
	{
		std::cout << "Error loading background!\n";
	}
	if(!mainMenuTitleTextTexture.loadFromFile("Resources/MainMenuTitleText.png"))
	{
		std::cout << "Error loading title text!\n";
	}
	if (!mainMenuSingleplayerTexture.loadFromFile("Resources/singlePlayerButtonText.png"))
	{
		std::cout << "Error loading singleplayer button texture!\n";
	}
	if (!mainMenuMultiplayerTexture.loadFromFile("Resources/multiPlayerButtonText.png"))
	{
		std::cout << "Error loading multiplayer button texture!\n";
	}
	if (!mainMenuExitTexture.loadFromFile("Resources/exitButton.png"))
	{
		std::cout << "Error loading exit button texture!\n";
	}
	if (!mainMenuBackTexture.loadFromFile("Resources/backButton.png"))
	{
		std::cout << "Error loading back button texture!\n";
	}
	if (!mainMenuPlayTexture.loadFromFile("Resources/playButton.png"))
	{
		std::cout << "Error loading play button texture!\n";
	}
	if (!mainMenuPlayMountainLevelTexture.loadFromFile("Resources/testBox.png"))
	{
		std::cout << "Error loading test box texture!\n";
	}
	if (!mainMenuPlayTutorialLevelTexture.loadFromFile("Resources/tutorialLevelBox.png"))
	{
		std::cout << "Error loading tutorial level box texture!\n";
	}
	if (!mainMenuPlayTestLevelTexture.loadFromFile("Resources/testLevelBox.png"))
	{
		std::cout << "Error loading test level box texture!\n";
	}
	if (!serverSelectBoxTexture.loadFromFile("Resources/serverBox.png"))
	{
		std::cout << "Error loading server select box texture!\n";
	}
	if (!lobbyTextBarTexture.loadFromFile("Resources/lobbyTextBar.png"))
	{
		std::cout << "Error loading lobby text bar texture!\n";
	}
	if (!lobbyTextBoxTexture.loadFromFile("Resources/lobbyTextBox.png"))
	{
		std::cout << "Error loading lobby text box texture!\n";
	}
	if (!redTeamButtonTexture.loadFromFile("Resources/redTeamButton.png"))
	{
		std::cout << "Error loading red team button texture!\n";
	}
	if (!blueTeamButtonTexture.loadFromFile("Resources/blueTeamButton.png"))
	{
		std::cout << "Error loading blue team button texture!\n";
	}
	if (!serverConnectTexture.loadFromFile("Resources/connectButton.png"))
	{
		std::cout << "Error loading connect button texture!\n";
	}
	if (!readyButtonTexture.loadFromFile("Resources/readyButton.png"))
	{
		std::cout << "Error loading ready button texture!\n";
	}
	if (!serverButtonTexture.loadFromFile("Resources/serverButton.png"))
	{
		std::cout << "Error loading server button texture!\n";
	}

	if (!selectButtonTexture.loadFromFile("Resources/selectButton.png"))
	{
		std::cout << "Error loading select button texture!\n";
	}
	selectButtonSprite.setTexture(selectButtonTexture);

	if (!spectatorButtonTexture.loadFromFile("Resources/spectatorButton.png"))
	{
		std::cout << "Error loading spectator button texture!\n";
	}
	spectatorButtonSprite.setTexture(spectatorButtonTexture);
	spectatorButtonSprite.setPosition(440.f, 550.f);

	mainMenuSingleplayerSprite.setTexture(mainMenuSingleplayerTexture);
	mainMenuSingleplayerSprite.setPosition(100.f, 300.f);

	mainMenuMultiplayerSprite.setTexture(mainMenuMultiplayerTexture);
	mainMenuMultiplayerSprite.setPosition(100.f, 400.f);

	mainMenuExitSprite.setTexture(mainMenuExitTexture);
	mainMenuExitSprite.setPosition(100.f, 500.f);

	mainMenuBackSprite.setTexture(mainMenuBackTexture);
	mainMenuBackSprite.setPosition(75.f, 550.f);

	mainMenuPlaySprite.setTexture(mainMenuPlayTexture);
	mainMenuPlaySprite.setPosition(900.f, 550.f);

	mainMenuBackgroundSprite.setTexture(mainMenuBackgroundTexture);

	mainMenuTitleSprite.setTexture(mainMenuTitleTextTexture);
	mainMenuTitleSprite.setPosition(150.f, 50.f);

	mainMenuPlayMountainLevelSprite.setTexture(mainMenuPlayMountainLevelTexture);
	mainMenuPlayMountainLevelSprite.setPosition(40.f, 75.f);

	mainMenuPlayTutorialLevelSprite.setTexture(mainMenuPlayTutorialLevelTexture);
	mainMenuPlayTutorialLevelSprite.setPosition(380.f, 75.f);

	mainMenuPlayTestLevelSprite.setTexture(mainMenuPlayTestLevelTexture);
	mainMenuPlayTestLevelSprite.setPosition(720.f, 75.f);

	serverSelectBoxSprite.setTexture(serverSelectBoxTexture);

	lobbyTextBarSprite.setTexture(lobbyTextBarTexture);
	lobbyTextBarSprite.setPosition(25.f, 500.f);
	lobbyTextBoxSprite.setTexture(lobbyTextBoxTexture);
	lobbyTextBoxSprite.setPosition(25.f, 25.f);

	redTeamButtonSprite.setTexture(redTeamButtonTexture);
	redTeamButtonSprite.setColor(sf::Color::Color(200,0,0,0));
	redTeamButtonSprite.setPosition(100.f, 125.f);

	blueTeamButtonSprite.setTexture(blueTeamButtonTexture);
	blueTeamButtonSprite.setColor(sf::Color::Color(0, 0, 200, 0));
	blueTeamButtonSprite.setPosition(590.f, 125.f);

	serverConnectSprite.setTexture(serverConnectTexture);
	serverConnectSprite.setPosition(900.f, 550.f);

	readyButtonSprite.setTexture(readyButtonTexture);
	readyButtonSprite.setPosition(900.f, 550.f);
	
	serverButtonSprite.setTexture(serverButtonTexture);
	serverButtonSprite.setPosition(440.f, 550.f);

	selectButtonSprite.setPosition(870.f, 550.f);

	playerHealthText.setFont(arial);
	playerHealthText.setCharacterSize(15);
	playerHealthText.setColor(sf::Color::Green);
	playerHealthText.setStyle(sf::Text::Bold);

	deathScreenTimerText.setFont(arial);
	deathScreenTimerText.setCharacterSize(70);
	deathScreenTimerText.setColor(sf::Color::Red);
	deathScreenTimerText.setStyle(sf::Text::Bold);
}
void startSingleplayerMap(sf::Vector2f _position, sf::Vector2f _velocity, std::string _mapLocation)
{
	std::string mapLocation;
	sf::Vector2f position, velocity;
	position = _position;
	velocity = _velocity;
	mapLocation = _mapLocation;

	//'Fake' loading from server
	playerStatsClient newPlayerStats;


	iAmPlayerNumber = 0;

	numOfRedPlayers = 1;
	numOfBluePlayers = 0;

	newPlayerStats.computerIDValue = 0;
	newPlayerStats.playerName = "Player";
	newPlayerStats.team = 1;
	velocity.x = 0.f;
	velocity.y = 0.f;

	characterSelectScreen();

	myPlayerNamespace::player newPlayer(rogueTexture, position.x, position.y, newPlayerStats.team, false);

	newPlayer.mSprite.setPosition(position);
	newPlayer.playerVelocity.x = velocity.x;
	newPlayer.playerVelocity.y = velocity.y;
	newPlayer.playerID = newPlayerStats.computerIDValue;
	newPlayer.playerName = newPlayerStats.playerName;

	newPlayer.currentFrameX = 1;
	newPlayer.currentFrameY = 1;

	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		if (playersList[i].playerID == newPlayer.playerID)
		{
			addPlayerToList = false;
			playersList[i].mSprite.setPosition(newPlayer.mSprite.getPosition());
		}
	}

	if (addPlayerToList == true)
	{
		playersList.push_back(newPlayer);

		if (characterSelected == 1)
		{
			playersList[playersList.size()-1].healthMax = 35.f;
			playersList[playersList.size()-1].health = playersList[playersList.size()-1].healthMax;

			playersList[playersList.size()-1].characterTexture = 1;
			playersList[playersList.size()-1].mSprite.setTexture(playerTexture);

			givePlayerShootArrowAbility(playersList.size()-1);
			givePlayerTransformHawkAbility(playersList.size()-1);
		}
		else if (characterSelected == 2)
		{
			playersList[playersList.size()-1].healthMax = 35.f;
			playersList[playersList.size()-1].health = playersList[playersList.size()-1].healthMax;

			playersList[playersList.size()-1].characterTexture = 4;
			playersList[playersList.size()-1].mSprite.setTexture(rogueTexture);

			givePlayerRogueSlashAbility(playersList.size()-1);
			givePlayerRogueDashAbility(playersList.size()-1);
		}
		else if (characterSelected == 3)
		{
			playersList[playersList.size()-1].healthMax = 60.f;
			playersList[playersList.size()-1].health = playersList[playersList.size()-1].healthMax;

			playersList[playersList.size()-1].characterTexture = 2;
			playersList[playersList.size()-1].mSprite.setTexture(warriorGuyTexture);

			givePlayerSwordSlashAbility(playersList.size()-1);
			givePlayerShieldBlockAbility(playersList.size()-1);
		}
		else if (characterSelected == 4)
		{
			playersList[playersList.size()-1].healthMax = 40.f;
			playersList[playersList.size()-1].health = playersList[playersList.size()-1].healthMax;

			playersList[playersList.size()-1].characterTexture = 3;
			playersList[playersList.size()-1].mSprite.setTexture(wizardTexture);

			givePlayerShootFireballAbility(playersList.size()-1);
			givePlayerThunderclapAbility(playersList.size()-1);
		}
		

		if (newPlayerStats.team == 1)
		{
			redTeamPlayerStatsClient.push_back(newPlayerStats);
		}
		else if (newPlayerStats.team == 2)
		{
			blueTeamPlayerStatsClient.push_back(newPlayerStats);
		}
	}

	if (mapLocation == "Resources/randomMap.txt")
	{
		generateMap(120, 400, 1, "Resources/tiles2.png");
	}

	//Load the map
	openMap(mapLocation);

	playLevelOne();
	//loadGem = false;
}
void serverSelectMenu()
{
	unsigned int selectedServer = 0;

	//Load servers from file
	std::fstream serversFile("Resources/servers.txt");
	unsigned int numOfServers;
	serversFile >> numOfServers;

	for (unsigned int i = 0; i < numOfServers; i++)
	{
		rememberedServersList newServer;
		knownServers.push_back(newServer);
		
		sf::IpAddress tempIP;
		unsigned short tempPort;
		std::string tempString, tempIconTexture, tempName, tempDescription, tempMessage;
		
		for (unsigned int j = 0; j < 7; j ++)
		{
			if (j == 0)
			{
				serversFile >> tempIP;
			}
			else if (j==1)
			{
				serversFile >> tempPort;
			}
			else if (j==2)
			{
				serversFile >> tempIconTexture;
			}
			else if (j==3)
			{
				std::getline(serversFile, tempString);
				tempName = tempString;
			}
			else if (j==4)
			{
				std::getline(serversFile, tempString);
				tempName = tempString;
			}
			else if (j==5)
			{
				std::getline(serversFile, tempString);
				tempDescription = tempString;
			}
			else if (j==6)
			{
				std::getline(serversFile, tempString);
				tempMessage = tempString;
			}
		}

		knownServers[i].IPAddress = tempIP;
		knownServers[i].port = tempPort;
		knownServers[i].iconTexture.loadFromFile(tempIconTexture);
		knownServers[i].name = tempName;
		knownServers[i].description = tempDescription;
		knownServers[i].messageOfTheDay = tempMessage;
		knownServers[i].selectionBox.setTexture(serverSelectBoxTexture);

		if (!knownServers[i].iconTexture.loadFromFile(tempIconTexture))
		{
			std::cout << "Error loading icon for server " << i << " from location " << tempIconTexture << std::endl;
		}
		knownServers[i].iconSprite.setTexture(knownServers[i].iconTexture);
	}
	serversFile.close();

	//Load text elements
	sf::Text displayText;
	displayText.setFont(arial);

	timeSinceLastUpdate = sf::Time::Zero;
	menuClock.restart();

	while (gameWindow.isOpen())
    {
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;

        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
            {
				gameWindow.close();
			}
			if (event.type == sf::Event::GainedFocus)
			{
				gameHasFocus = true;
			}
			if (event.type == sf::Event::LostFocus)
			{
				gameHasFocus = false;
			}

			if (event.type == sf::Event::KeyReleased && (timeSinceLastUpdate > timeUntilCanAct) && gameHasFocus == true)
			{
				if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
				{
					if (selectedServer > 1)
					{
						selectedServer--;
					}
					else
					{
						selectedServer = knownServers.size() + 1;
					}
				}
				if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
				{
					if (selectedServer < knownServers.size() + 1)
					{
						selectedServer++;
					}
					else
					{
						selectedServer = 0;
					}
				}
				if (event.key.code == sf::Keyboard::Escape)
				{
					mainMenu();
				}
				if (event.key.code == sf::Keyboard::Return)
				{
					if (selectedServer < knownServers.size())
					{
						serverTeamAndNameSelect(selectedServer);
					}
					else if (selectedServer == knownServers.size())
					{
						mainMenu();
					}
					else if (selectedServer == knownServers.size() + 1)
					{
						runServer();
					}
				}
			}
        }

		//Mouse input
		sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
		sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

		//Back Button
		if (mainMenuBackSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			selectedServer = knownServers.size();
			mainMenuBackSprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				mainMenu();
			}
		}
		if (selectedServer == knownServers.size())
		{
			mainMenuBackSprite.setScale((float)1.2, (float)1.2);
		}
		else
		{
			mainMenuBackSprite.setScale((float)1, (float)1);
		}

		//Server Button
		if (serverButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			selectedServer = knownServers.size() + 1;
			serverButtonSprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				runServer();
			}
		}
		if (selectedServer == knownServers.size() + 1)
		{
			serverButtonSprite.setScale((float)1.2, (float)1.2);
		}
		else
		{
			serverButtonSprite.setScale((float)1, (float)1);
		}

		//Servers Button
		for (unsigned int i = 0; i < knownServers.size(); i++)
		{
			if (knownServers[i].selectionBox.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				selectedServer = i;
				knownServers[i].selectionBox.setColor(sf::Color::Blue);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
				{
					//mainMenu();
					//serverLobbyMenu((unsigned int)i);
					serverTeamAndNameSelect((unsigned int)i);
				}
			}
			if (selectedServer == i)
			{
				knownServers[i].selectionBox.setColor(sf::Color::Blue);
			}
			else
			{
				knownServers[i].selectionBox.setColor(sf::Color::Red);
			}
		}

		//Draw to window
		gameWindow.clear();

		gameWindow.draw(mainMenuBackgroundSprite);
		gameWindow.draw(mainMenuBackSprite);
		gameWindow.draw(serverButtonSprite);

		//Draw servers
		for (unsigned int i = 0; i < numOfServers; i++)
		{
			knownServers[i].selectionBox.setPosition(100.f, (float) (250 * i + 25));
			gameWindow.draw(knownServers[i].selectionBox);

			knownServers[i].iconSprite.setPosition(110.f, (float) (250 * i + 35));
			knownServers[i].iconSprite.setTexture(knownServers[i].iconTexture);
			gameWindow.draw(knownServers[i].iconSprite);

			displayText.setCharacterSize(40);
			displayText.setColor(sf::Color::Red);
			displayText.setString(knownServers[i].name);
			displayText.setPosition(310.f, (float) (250 * i + 35));
			gameWindow.draw(displayText);

			displayText.setCharacterSize(30);
			displayText.setColor(sf::Color::Magenta);
			displayText.setString(knownServers[i].description);
			displayText.setPosition(310.f, (float) (250 * i + 85));
			gameWindow.draw(displayText);

			displayText.setCharacterSize(20);
			displayText.setColor(sf::Color::White);
			displayText.setString(knownServers[i].messageOfTheDay);
			displayText.setPosition(310.f, (float) (250 * i + 140));
			gameWindow.draw(displayText);
		}

		gameWindow.display();
	}
}
void serverLobbyMenu(unsigned int _serverNumber)
{
	lobbyTextBarSprite.setPosition(25.f, 500.f);
	lobbyTextBarSprite.setScale(1.f, 1.f);

	timeSinceLastUpdate = sf::Time::Zero;
	menuClock.restart();
	sf::Text chatText;
	chatText.setFont(arial);
	chatText.setCharacterSize(22);
	myText.setFont(arial);
	myText.setCharacterSize(20);

	/**************************************************/
	//Establishing connection to server
	/*************************************************/
	recipientIPAddress = knownServers[_serverNumber].IPAddress;
	UDPSendPort = knownServers[_serverNumber].port;
	//std::cin >> username;

	//Load local port preference from file
	std::fstream connectionInfoFile("connectionInfo.txt");
	if (connectionInfoFile.is_open())
	{
		connectionInfoFile >> UDPRecievePort;
	}
	connectionInfoFile.close();


	if (UDPSocket.bind(UDPRecievePort) != sf::Socket::Done)
	{
		std::cout << "Error connecting to port " << UDPSocket.getLocalPort() << "! \n";
	}

	UDPSocket.setBlocking(false);
	socketSelector.add(UDPSocket);

	//updateToServer(0);
	updateBackendToServer(0, "_codeSuppliedByFunction", username, whichTeam);

	sf::Time timeToServerResponse;
	timeToServerResponse = sf::Time::Zero;
	bool waitingForConfirmation = true;
	bool isPlayerReady = false;

	while (gameWindow.isOpen())
	{
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;
		timeToServerResponse += elapsedTime;

		//while (timeSinceLastUpdate > timePerFrame)
		{
			//timeSinceLastUpdate -= timePerFrame;

			//Test for recieved packets
			while (UDPSocket.receive(inPacket, senderIPAddress, UDPRecievePort) == sf::Socket::Done)
			{
				if (inPacket >> packetHeader)
				{
					if (packetHeader == 0)
					{
						if (inPacket >> packetSubHeader)
						{
							if (packetSubHeader == 0)
							{
								inPacket >> textMessage;

								if (textMessage == "simonsaysshello")
								{							
									messagingStruct newMessage;

									newMessage.message = "Server confirmed connection!";
									newMessage.team = 0;

									messagesList.push_back(newMessage);

									inPacket.clear();
									waitingForConfirmation = false;
								}
								else
								{
									messagingStruct newMessage;

									newMessage.message = textMessage;
									newMessage.team = teamWhichSentMessage;

									messagesList.push_back(newMessage);
								}

								textMessage.clear();
								inPacket.clear();
							}
							else if (packetSubHeader == 1)
							{
								inPacket >> testData >> localPlayerNumber >> numOfRedPlayers >> numOfBluePlayers >> serverStartGameCode >> redTeamSpawnX >> redTeamSpawnY >> blueTeamSpawnX >> blueTeamSpawnY >> enableWinCondition;
								if (serverStartGameCode == "totallysecrectpassword")
								{
									//std::cout << "Received start game code from server!\n";
									messagingStruct newMessage;

									newMessage.message = "Received start game code from server!";
									newMessage.team = 0;

									messagesList.push_back(newMessage);
									startGame = true;
									inPacket.clear();
									messagesList.clear();

									sf::Vector2f _pos, _vel;
									if (playersList[localPlayerNumber].teamAffiliation == 1)
									{
										view1.setCenter(redTeamSpawnX + 30, redTeamSpawnY - 166);
										_pos.x = redTeamSpawnX;
										_pos.y = redTeamSpawnY;
									}
									else
									{
										view1.setCenter(blueTeamSpawnX + 30, blueTeamSpawnY - 166);
										_pos.x = blueTeamSpawnX;
										_pos.y = blueTeamSpawnY;
									}

									//view1.setCenter((float)((redTeamSpawnX + blueTeamSpawnX)/2), (float)((redTeamSpawnY + blueTeamSpawnY)/2));

									timeSpentInCurrentLevel = sf::Time::Zero;

									startSingleplayerMap(_pos, _vel, testData);
									//characterSelectScreen();

									//Load the map
									//openMap(testData);

									//playLevelOne();
								}
							}
						}
					}
					else if (packetHeader == 1)
					{
						if (inPacket >> packetSubHeader)
						{
							if (packetSubHeader == 1)
							{
								if (inPacket >> textMessage >> teamWhichSentMessage)
								{
									messagingStruct newMessage;

									newMessage.message = textMessage;
									newMessage.team = teamWhichSentMessage;

									messagesList.push_back(newMessage);

									textMessage.clear();
									inPacket.clear();
								}
							}
						}
					}
					else if (packetHeader == 2)
					{
						if (inPacket >> packetSubHeader)
						{
							if (packetSubHeader == 0)
							{
								int playerNumber, team, characterType;
								if (inPacket >> playerNumber >> team >> characterType)
								{
									playersList[playerNumber].teamAffiliation = team;

									if (characterType == 1)
									{
										playersList[playerNumber].mSprite.setTexture(playerTexture);
									}
									else if (characterType == 2)
									{
										playersList[playerNumber].mSprite.setTexture(rogueTexture);
									}
									else if (characterType == 3)
									{
										playersList[playerNumber].mSprite.setTexture(warriorGuyTexture);
									}
									else if (characterType == 4)
									{
										playersList[playerNumber].mSprite.setTexture(wizardTexture);
									}
								}
							}
							else if (packetSubHeader == 1)
							{
								unsigned int playerNumber;
								float health, posX, posY;
								int frameX, frameY;

								inPacket >> playerNumber >> posX >> posY >> frameX >> frameY >> health;

								playerStatsClient newPlayerStats;

								myPlayerNamespace::player newPlayer(playerTexture, posX, posY, 1, false);

								newPlayer.playerVelocity.x = velocity.x;
								newPlayer.playerVelocity.y = velocity.y;
								newPlayer.playerID = playerNumber;
								//newPlayer.playerName = newPlayerStats.playerName;


								newPlayerStats.playerLevel = 0;

								newPlayerStats.characterStats[0] = 0;
								newPlayerStats.characterStats[1] = 0;
								newPlayerStats.characterStats[2] = 0;
								newPlayerStats.characterStats[3] = 0;
								newPlayerStats.characterStats[4] = 0;

								for (unsigned int i = 0; i < playersList.size(); i++)
								{
									if (playersList[i].playerID == newPlayer.playerID)
									{
										addPlayerToList = false;
									}
								}

								if (addPlayerToList == true)
								{
									playersList.push_back(newPlayer);

									if (newPlayerStats.team == 1)
									{
										redTeamPlayerStatsClient.push_back(newPlayerStats);
									}
									else if (newPlayerStats.team == 2)
									{
										blueTeamPlayerStatsClient.push_back(newPlayerStats);
									}
								}
								addPlayerToList = true;
								inPacket.clear();
							}
						}
					}
					else
					{
						inPacket.clear();
					}
					/*
					else if (packetHeader == 2)
					{
						sf::Vector2f position, velocity;
						playerStatsClient newPlayerStats;


						inPacket >> newPlayerStats.computerIDValue >> newPlayerStats.playerName >> newPlayerStats.team >> position.x >> position.y >> velocity.x >> velocity.y >> isNewPlayerNPC;

						myPlayerNamespace::player newPlayer(playerTexture, position.x, position.y, newPlayerStats.team, isNewPlayerNPC);

						newPlayer.mSprite.setPosition(position);
						newPlayer.playerVelocity.x = velocity.x;
						newPlayer.playerVelocity.y = velocity.y;
						newPlayer.playerID = newPlayerStats.computerIDValue;
						newPlayer.playerName = newPlayerStats.playerName;


						newPlayerStats.playerLevel = 0;

						newPlayerStats.characterStats[0] = 0;
						newPlayerStats.characterStats[1] = 0;
						newPlayerStats.characterStats[2] = 0;
						newPlayerStats.characterStats[3] = 0;
						newPlayerStats.characterStats[4] = 0;

						for (unsigned int i = 0; i < playersList.size(); i++)
						{
							if (playersList[i].playerID == newPlayer.playerID)
							{
								addPlayerToList = false;
							}
						}

						if (addPlayerToList == true)
						{
							playersList.push_back(newPlayer);

							if (newPlayerStats.team == 1)
							{
								redTeamPlayerStatsClient.push_back(newPlayerStats);
							}
							else if (newPlayerStats.team == 2)
							{
								blueTeamPlayerStatsClient.push_back(newPlayerStats);
							}
						}
						addPlayerToList = true;
						inPacket.clear();
					}
					*/
				}
			}

			//If no server response after 5 seconds, display error code
			if (timeToServerResponse > sf::seconds(5) && waitingForConfirmation == true)
			{
				messagingStruct newMessage;

				newMessage.message = "Request timed out!";
				newMessage.team = 0;

				messagesList.push_back(newMessage);
				waitingForConfirmation = false;
			}

			// check all the window's events that were triggered since the last iteration of the loop
			sf::Event event;
			while (gameWindow.pollEvent(event))
			{
				// "close requested" event: we close the window
				if (event.type == sf::Event::Closed)
				{
					serverSelectMenu();
					//updateToServer(0);
					updateBackendToServer(0, "_codeSuppliedByFunction");
				}
				if (event.type == sf::Event::TextEntered && gameHasFocus == true)
				{
					int textSize = messageToBeSent.size();
					unsigned short unicode = event.text.unicode;

					if (unicode == 13) //If return/enter
					{
						if (isTyping == true)
						{
							if (messageToBeSent.size() > 0)
							{
								//updateToServer(1, messageToBeSent, whichTeam);
								updateChatToServer(1, messageToBeSent, whichTeam);
							}
							messageToBeSent.clear();
							isTyping = false;
						}
						else
						{
							isTyping = true;
						}
					}
					if (isTyping == true)
					{
						if (unicode == 8) //If backspace
						{
							if (textSize > 0)
							{
								messageToBeSent.erase(textSize - 1, 1);
							}
						}
						else if (unicode >= 32 && unicode <= 126)
						{
							messageToBeSent += (char)unicode;
						}
						else if (unicode >= 192 && unicode <= 255)
						{
							messageToBeSent += (char)unicode;
						}
					}
				}
				if (event.type == sf::Event::GainedFocus)
				{
					gameHasFocus = true;
				}
				if (event.type == sf::Event::LostFocus)
				{
					gameHasFocus = false;
				}
				if (event.type == sf::Event::KeyReleased && (timeSinceLastUpdate > timeUntilCanAct) && gameHasFocus == true)
				{
					if (event.key.code == sf::Keyboard::Escape)
					{
						serverSelectMenu();
					}
				}
			}

			sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
			sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

			//Back Button
			if (mainMenuBackSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				mainMenuBackSprite.setScale((float)1.2, (float)1.2);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
				{
					messagesList.clear();
					socketSelector.clear();

					//updateToServer(0);
					updateBackendToServer(0, "_codeSuppliedByFunction");

					UDPSocket.unbind();

					wasConnectedTo = false;

					serverSelectMenu();
				}
			}
			else
			{
				mainMenuBackSprite.setScale((float)1, (float)1);
			}

			//Ready Button
			if (readyButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{	
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
				{
					isPlayerReady = !isPlayerReady;
					timeSinceLastUpdate = sf::Time::Zero;
					//updateToServer(1, "ready", whichTeam);
					updateChatToServer(1, "ready", whichTeam);
				}
			}
			if (isPlayerReady == true)
			{
				readyButtonSprite.setColor(sf::Color::White);
			}
			else
			{
				readyButtonSprite.setColor(sf::Color::Color(200,200,100,100));
			}

			//Entering text
			if (lobbyTextBarSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				lobbyTextBarSprite.setColor(sf::Color::Black);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
				{
					isTyping = true;
				}
			}
			else if (isTyping == true)
			{
				lobbyTextBarSprite.setColor(sf::Color::Black);
			}
			else
			{
				lobbyTextBarSprite.setColor(sf::Color::Cyan);
			}

			//Draw stuff
			gameWindow.clear();

			gameWindow.draw(mainMenuBackgroundSprite);
			gameWindow.draw(mainMenuBackSprite);

			gameWindow.draw(lobbyTextBarSprite);
			gameWindow.draw(lobbyTextBoxSprite);

			gameWindow.draw(readyButtonSprite);

			/*********************/
			//Chat 'box'
			/*********************/
			if (isGameLocal == false)
			{
				//Check size of chat list
				if (messagesList.size() > 10)
				{
					displayMessages = 10;
				}
				else
				{
					displayMessages = messagesList.size();
				}

				//Display chat
				for (unsigned int i = 0; i < displayMessages; i++)
				{
					if (displayMessages == 10)
					{
						myText.setString(messagesList[i + messagesList.size() - 10].message);
						if (messagesList[i + messagesList.size() - 10].team == 0)
						{
							myText.setColor(sf::Color::White);
						}
						else if (messagesList[i + messagesList.size() - 10].team == 1)
						{
							myText.setColor(sf::Color::Red);
						}
						else if (messagesList[i + messagesList.size() - 10].team == 2)
						{
							myText.setColor(sf::Color::Blue);
						}
						else
						{
							myText.setColor(sf::Color::Magenta);
						}
					}
					else
					{
						myText.setString(messagesList[i].message);

						if (messagesList[i].team == 0)
						{
							myText.setColor(sf::Color::Magenta);
						}
						else if (messagesList[i].team == 1)
						{
							myText.setColor(sf::Color::Red);
						}
						else if (messagesList[i].team == 2)
						{
							myText.setColor(sf::Color::Blue);
						}
						else
						{
							myText.setColor(sf::Color::White);
						}
					}

					myText.setPosition(27.f, (float)(25*i + 30));
					gameWindow.draw(myText);
				}
			}

			chatText.setString(messageToBeSent);
			chatText.setPosition(30.f, 498.f);
			gameWindow.draw(chatText);

			gameWindow.display();
		}
	}
}
void serverTeamAndNameSelect(unsigned int _serverNumber)
{
	lobbyTextBarSprite.setPosition(295.f, 50.f);
	lobbyTextBarSprite.setScale(0.5, 1.f);

	myText.setFont(arial);
	myText.setCharacterSize(20);

	timeSinceLastUpdate = sf::Time::Zero;
	menuClock.restart();

	while (gameWindow.isOpen())
    {
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;

        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
			{
                gameWindow.close();
			}
			if (event.type == sf::Event::TextEntered && gameHasFocus == true && isTyping == true)
			{
				int textSize = username.size();
				unsigned short unicode = event.text.unicode;

				if (unicode == 13) //If return/enter
				{
					if (isTyping == true)
					{
						isTyping = false;
					}
					else
					{
						isTyping = true;
					}
				}
				if (isTyping == true)
				{
					if (unicode == 8) //If backspace
					{
						if (textSize > 0)
						{
							username.erase(textSize - 1, 1);
						}
					}
					else if (unicode >= 32 && unicode <= 126)
					{
						username += (char)unicode;
					}
					else if (unicode >= 192 && unicode <= 255)
					{
						username += (char)unicode;
					}
				}
			}
			if (event.type == sf::Event::GainedFocus)
			{
				gameHasFocus = true;
			}
			if (event.type == sf::Event::LostFocus)
			{
				gameHasFocus = false;
			}
			if (event.type == sf::Event::KeyReleased && (timeSinceLastUpdate > timeUntilCanAct) && gameHasFocus == true)
			{
				if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
				{
					if (whichTeam > 1)
					{
						whichTeam--;
					}
					else
					{
						whichTeam = 3;
					}
				}
				if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
				{
					if (whichTeam < 3)
					{
						whichTeam++;
					}
					else
					{
						whichTeam = 1;
					}
				}
				if (event.key.code == sf::Keyboard::Escape)
				{
					serverSelectMenu();
				}
				if (event.key.code == sf::Keyboard::Return)
				{
					isTyping = !isTyping;

					if (whichTeam > 0)
					{
						if(username.find_first_not_of(' ') != std::string::npos)
						{
							//there exists a non-space character
							isTyping = false;
							serverLobbyMenu(_serverNumber);
						}
					}

				}
			} 
		}

		sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
		sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

		//Back Button
		if (mainMenuBackSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			mainMenuBackSprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				serverSelectMenu();
			}
		}
		else
		{
			mainMenuBackSprite.setScale((float)1, (float)1);
		}

		//Spectator Button
		if (spectatorButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			spectatorButtonSprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				//SPECTATOR MODE
				whichTeam = 3;
			}
		}
		else
		{
			spectatorButtonSprite.setScale((float)1, (float)1);
		}

		//Connect Button
		if (serverConnectSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			serverConnectSprite.setScale((float)1.2, (float)1.2);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				if (whichTeam > 0)
				{
					if(username.find_first_not_of(' ') != std::string::npos)
					{
						//there exists a non-space character
						isTyping = false;
						serverLobbyMenu(_serverNumber);
					}
				}
			}
		}
		else
		{
			serverConnectSprite.setScale((float)1, (float)1);
		}

		//Red Team
		if (redTeamButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			redTeamButtonSprite.setColor(sf::Color::Magenta);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				whichTeam = 1;				
			}
		}
		else
		{
			redTeamButtonSprite.setColor(sf::Color::Color(200,0,0,100));
		}

		//Blue Team
		if (blueTeamButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			blueTeamButtonSprite.setColor(sf::Color::Cyan);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				whichTeam = 2;				
			}
		}
		else
		{
			blueTeamButtonSprite.setColor(sf::Color::Color(200,0,0,100));
		}

		//Highlight selected team
		if (whichTeam == 1)
		{
			redTeamButtonSprite.setColor(sf::Color::Magenta);
			blueTeamButtonSprite.setColor(sf::Color::Color(200,0,0,100));
		}
		else if (whichTeam == 2)
		{
			redTeamButtonSprite.setColor(sf::Color::Color(200,0,0,100));
			blueTeamButtonSprite.setColor(sf::Color::Cyan);
		}
		else if (whichTeam == 3)
		{
			redTeamButtonSprite.setColor(sf::Color::Color(200,0,0,100));
			blueTeamButtonSprite.setColor(sf::Color::Color(200,0,0,100));
			spectatorButtonSprite.setScale((float)1.2, (float)1.2);
		}

		//Entering text
		if (lobbyTextBarSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
		{
			lobbyTextBarSprite.setColor(sf::Color::Black);
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeSinceLastUpdate > timeUntilCanAct))
			{
				isTyping = true;
			}
		}
		else if (isTyping == true)
		{
			lobbyTextBarSprite.setColor(sf::Color::Black);
		}
		else
		{
			lobbyTextBarSprite.setColor(sf::Color::Cyan);
		}


		//Display to Window
		gameWindow.clear();

		gameWindow.draw(mainMenuBackgroundSprite);

		gameWindow.draw(redTeamButtonSprite);
		gameWindow.draw(blueTeamButtonSprite);
		gameWindow.draw(mainMenuBackSprite);
		gameWindow.draw(lobbyTextBarSprite);

		gameWindow.draw(serverConnectSprite);

		gameWindow.draw(spectatorButtonSprite);

		//Draw username
		if (whichTeam == 0)
		{
			myText.setColor(sf::Color::Magenta);
		}
		else if (whichTeam == 1)
		{
			myText.setColor(sf::Color::Red);
		}
		else if (whichTeam == 2)
		{
			myText.setColor(sf::Color::Blue);
		}
		else if (whichTeam == 3)
		{
			myText.setColor(sf::Color::White);
		}
		myText.setString(username);
		myText.setPosition(295.f, 50.f);
		gameWindow.draw(myText);

		gameWindow.display();
    }
}
void endOfMultiplayerGameScreen()
{
	//wasConnectedTo = false;
}
void gameEndScreen(unsigned int _winningTeam)
{
	timeSinceLastUpdate = sf::Time::Zero;
	sf::Time timeUntilCanExit;
	menuClock.restart();

	redTeamKills = 0;
	blueTeamKills = 0;

	ambientAbyssMusic.stop();
	sunflowerDanceMusic.stop();

	victoryText.setFont(arial);
	victoryText.setCharacterSize(125);
	victoryText.setPosition(50.f, 50.f);

	playersList[localPlayerNumber].mSprite.setTexture(playerTexture);

	playersList[localPlayerNumber].abilitiesVector[8].isActive = false;
	playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft = sf::Time::Zero;
	playersList[localPlayerNumber].abilitiesVector[8].durationLeft = sf::Time::Zero;

	playersList[localPlayerNumber].playerVelocity.x = 0;
	playersList[localPlayerNumber].playerVelocity.y = 0;

	sf::Text mapSeedText;
	mapSeedText.setFont(arial);
	mapSeedText.setCharacterSize(50);
	mapSeedText.setPosition(50.f, 200.f);
	mapSeedText.setColor(sf::Color::White);
	mapSeedText.setString("Map seed: " + std::to_string((_ULonglong)mapGeneratorSeed));


	sf::Text timeSpentInLevelText;
	timeSpentInLevelText.setFont(arial);
	timeSpentInLevelText.setCharacterSize(50);
	timeSpentInLevelText.setPosition(50.f, 300.f);
	timeSpentInLevelText.setColor(sf::Color::White);
	timeSpentInLevelText.setString("Time spent in level: " + std::to_string((_ULonglong)timeSpentInCurrentLevel.asSeconds()) + " seconds");
	

	if (_winningTeam == 0)
	{
		victoryText.setColor(sf::Color::White);
		victoryText.setString("Game Quit!");
	}
	else if (_winningTeam == 1)
	{
		victoryText.setColor(sf::Color::Red);
		victoryText.setString("Red Team Wins!");
	}
	else if (_winningTeam == 2)
	{
		victoryText.setColor(sf::Color::Blue);
		victoryText.setString("Blue Team Wins!");
	}

	playersList.clear();
	bulletsList.clear();

	while (gameWindow.isOpen())
	{
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;
		timeUntilCanExit += elapsedTime;

		while (timeSinceLastUpdate > timePerFrame)
		{
			timeSinceLastUpdate -= timePerFrame;

			// check all the window's events that were triggered since the last iteration of the loop
			sf::Event event;
			while (gameWindow.pollEvent(event))
			{
				// "close requested" event: we close the window
				if (event.type == sf::Event::Closed)
				{
					mainMenu();
				}
				if (event.type == sf::Event::GainedFocus)
				{
					gameHasFocus = true;
				}
				if (event.type == sf::Event::LostFocus)
				{
					gameHasFocus = false;
				}
				if (event.type == sf::Event::KeyReleased)
				{
					if (event.key.code == sf::Keyboard::Return)
					{
						mainMenu();
					}
					if (event.key.code == sf::Keyboard::Escape)
					{
						mainMenu();
					}
				}
			}

			sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
			sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

			//Back Button
			if (mainMenuBackSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				mainMenuBackSprite.setScale((float)1.2, (float)1.2);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					mainMenu();
				}
			}
			else
			{
				mainMenuBackSprite.setScale((float)1, (float)1);
			}

			//Draw to screen
			gameWindow.setView(gameWindow.getDefaultView());

			gameWindow.clear();

			gameWindow.draw(mainMenuBackgroundSprite);
			gameWindow.draw(mainMenuBackSprite);
			gameWindow.draw(victoryText);
			gameWindow.draw(mapSeedText);
			gameWindow.draw(timeSpentInLevelText);

			gameWindow.display();
		}
	}
}
void initializeSettings()
{
	std::fstream settingsFile("Resources/settings.txt");

	settingsFile >> soundEffectsVolume;
	settingsFile >> musicVolume;
}
void characterSelectScreen()
{
	timeSinceLastUpdate = sf::Time::Zero;
	sf::Time timeUntilCanExit;
	menuClock.restart();

	characterSelected = 1;
	unsigned int buttonSelected = 1;
	bool selectingCharacter = true;
	int displayTooltip = 0;

	std::string tooltipName;
	std::string tooltipCooldown;
	std::string tooltipAbilityDuration;
	std::string tooltipDescription;

	sf::Text tooltipText;
	tooltipText.setFont(arial);
	tooltipText.setCharacterSize(35);

	sf::Text hpText;
	hpText.setFont(arial);
	hpText.setCharacterSize(25);
	hpText.setPosition(550.f, 450.f);

	sf::Text msText;
	msText.setFont(arial);
	msText.setCharacterSize(25);
	msText.setPosition(700.f, 450.f);

	sf::Sprite tooltipBox;
	sf::Texture tooltipBoxTexture;

	tooltipBoxTexture.loadFromFile("Resources/Character Icons/tooltip.png");
	tooltipBox.setTexture(tooltipBoxTexture);
	tooltipBox.setPosition(550.f, 120.f);

	initializeCharacterIcons();
	characterPreviewSprite.setTextureRect(sf::IntRect(0,0,32,48));
	characterPreviewSprite.setPosition(80.f, 245.f);
	characterPreviewSprite.setScale(4.f, 4.f);

	hpText.setString("Health: 35");
	msText.setString("Movement Speed: 3");

	characterPreviewSprite.setTexture(playerTexture);
	abilityOnePreviewTexture.loadFromFile("Resources/HUD/shootArrowIcon.png");
	abilityOnePreviewSprite.setTexture(abilityOnePreviewTexture);

	abilityTwoPreviewTexture.loadFromFile("Resources/HUD/transformHawkIcon.png");
	abilityTwoPreviewSprite.setTexture(abilityTwoPreviewTexture);

	abilityThreePreviewTexture.loadFromFile("Resources/HUD/lockedAbilityIcon.png");
	abilityThreePreviewSprite.setTexture(abilityThreePreviewTexture);

	abilityOnePreviewSprite.setPosition(295.f, 181.f);
	abilityOnePreviewSprite.setScale((float)1.7, (float)1.43);
	abilityTwoPreviewSprite.setPosition(295.f, 286.f);
	abilityTwoPreviewSprite.setScale((float)1.7, (float)1.43);
	abilityThreePreviewSprite.setPosition(295.f, 391.f);
	abilityThreePreviewSprite.setScale((float)1.7, (float)1.43);

	while (gameWindow.isOpen())
	{
		elapsedTime = menuClock.restart();
		timeSinceLastUpdate += elapsedTime;
		timeUntilCanExit += elapsedTime;

		while (timeSinceLastUpdate > timePerFrame)
		{
			timeSinceLastUpdate -= timePerFrame;

			// check all the window's events that were triggered since the last iteration of the loop
			sf::Event event;
			while (gameWindow.pollEvent(event))
			{
				// "close requested" event: we close the window
				if (event.type == sf::Event::Closed)
				{
					mainMenu();
				}
				if (event.type == sf::Event::GainedFocus)
				{
					gameHasFocus = true;
				}
				if (event.type == sf::Event::LostFocus)
				{
					gameHasFocus = false;
				}
				if (event.type == sf::Event::KeyReleased)
				{
					if (event.key.code == sf::Keyboard::Return)
					{
						return;
					}
					if (event.key.code == sf::Keyboard::Escape)
					{
						singleplayerCampaign();
					}
				}
			}

			sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
			sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

			//Archer Button
			if (archerButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				characterSelected = 1;
				
				hpText.setString("Health: 35");
				msText.setString("Movement Speed: 3");

				characterPreviewSprite.setTexture(playerTexture);
				abilityOnePreviewTexture.loadFromFile("Resources/HUD/shootArrowIcon.png");
				abilityOnePreviewSprite.setTexture(abilityOnePreviewTexture);

				abilityTwoPreviewTexture.loadFromFile("Resources/HUD/transformHawkIcon.png");
				abilityTwoPreviewSprite.setTexture(abilityTwoPreviewTexture);

				abilityThreePreviewTexture.loadFromFile("Resources/HUD/lockedAbilityIcon.png");
				abilityThreePreviewSprite.setTexture(abilityThreePreviewTexture);

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					//mainMenu();
				}
			}
			if (characterSelected == 1)
			{
				archerButtonSprite.setColor(sf::Color::Cyan);
			}
			else
			{
				archerButtonSprite.setColor(sf::Color::White);
			}
			//Rogue Button
			if (rogueButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				characterSelected = 2;
					
				hpText.setString("Health: 35");
				msText.setString("Movement Speed: 3.2");

				characterPreviewSprite.setTexture(rogueTexture);
				abilityOnePreviewTexture.loadFromFile("Resources/HUD/rogueSlashIcon.png");
				abilityOnePreviewSprite.setTexture(abilityOnePreviewTexture);

				abilityTwoPreviewTexture.loadFromFile("Resources/HUD/rogueDashIcon.png");
				abilityTwoPreviewSprite.setTexture(abilityTwoPreviewTexture);

				abilityThreePreviewTexture.loadFromFile("Resources/HUD/lockedAbilityIcon.png");
				abilityThreePreviewSprite.setTexture(abilityThreePreviewTexture);

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					//mainMenu();
				}
			}
			if (characterSelected == 2)
			{
				rogueButtonSprite.setColor(sf::Color::Cyan);
			}
			else
			{
				rogueButtonSprite.setColor(sf::Color::White);
			}
			//Warrior Button
			if (warriorButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				characterSelected = 3;
								
				hpText.setString("Health: 60");
				msText.setString("Movement Speed: 2.5");

				characterPreviewSprite.setTexture(warriorGuyTexture);
				abilityOnePreviewTexture.loadFromFile("Resources/HUD/sliceDownIcon.png");
				abilityOnePreviewSprite.setTexture(abilityOnePreviewTexture);

				abilityTwoPreviewTexture.loadFromFile("Resources/HUD/shieldBlockIcon.png");
				abilityTwoPreviewSprite.setTexture(abilityTwoPreviewTexture);

				abilityThreePreviewTexture.loadFromFile("Resources/HUD/lockedAbilityIcon.png");
				abilityThreePreviewSprite.setTexture(abilityThreePreviewTexture);

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					//mainMenu();
				}
			}
			if (characterSelected == 3)
			{
				warriorButtonSprite.setColor(sf::Color::Cyan);
			}
			else
			{
				warriorButtonSprite.setColor(sf::Color::White);
			}
			//Wizard Button
			if (wizardButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				characterSelected = 4;
								
				hpText.setString("Health: 40");
				msText.setString("Movement Speed: 2.8");

				characterPreviewSprite.setTexture(wizardTexture);
				abilityOnePreviewTexture.loadFromFile("Resources/HUD/shootFireballIcon.png");
				abilityOnePreviewSprite.setTexture(abilityOnePreviewTexture);

				abilityTwoPreviewTexture.loadFromFile("Resources/HUD/thunderclapIcon.png");
				abilityTwoPreviewSprite.setTexture(abilityTwoPreviewTexture);

				abilityThreePreviewTexture.loadFromFile("Resources/HUD/lockedAbilityIcon.png");
				abilityThreePreviewSprite.setTexture(abilityThreePreviewTexture);

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					//mainMenu();
				}
			}
			if (characterSelected == 4)
			{
				wizardButtonSprite.setColor(sf::Color::Cyan);
			}
			else
			{
				wizardButtonSprite.setColor(sf::Color::White);
			}

			//Back Button
			if (mainMenuBackSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				buttonSelected = 4;
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					singleplayerCampaign();
				}
			}
			if (buttonSelected == 4)
			{
				mainMenuBackSprite.setScale((float)1.2, (float)1.2);
			}
			else
			{
				mainMenuBackSprite.setScale((float)1, (float)1);
			}
			//Select Button
			if (selectButtonSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				buttonSelected = 5;
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (timeUntilCanExit > timeUntilCanAct))
				{
					if (isGameLocal == false)
					{
						updateCharacterToServer(0, localPlayerNumber, username, whichTeam, characterSelected);
					}
					return;
				}
			}
			if (buttonSelected == 5)
			{
				selectButtonSprite.setScale((float)1.2, (float)1.2);
			}
			else
			{
				selectButtonSprite.setScale((float)1, (float)1);
			}

			//Ability Tooltips
			if (abilityOnePreviewSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				displayTooltip = 1;
				if (characterSelected == 1)
				{
					tooltipName = "Shoot Arrow";
					tooltipCooldown = "CD: 0.35 seconds";
					tooltipAbilityDuration = "Dur: N/A";
					tooltipDescription = "Draw an arrow. (Charge up to 2 seconds).\nDamage scales with duration charged. \nDeals 15-30 damage.";
				}
				else if (characterSelected == 2)
				{
					tooltipName = "Slice/Backstab";
					tooltipCooldown = "CD: 0.25 seconds";
					tooltipAbilityDuration = "Dur: N/A";
					tooltipDescription = "Slice in front of you. \nIf you hit a target from behind, deal \n 150% damage. \nDeals 12-18 damage.";
				}
				else if (characterSelected == 3)
				{
					tooltipName = "Slash Down/Slash Up";
					tooltipCooldown = "CD: 0.4 seconds";
					tooltipAbilityDuration = "Dur: N/A";
					tooltipDescription = "Slash your sword. \nEnemies hit when you slice up will be \n knocked up slightly. \nDeals 10 damage.";
				}
				else if (characterSelected == 4)
				{
					tooltipName = "Shoot Fireball";
					tooltipCooldown = "CD: 1 second";
					tooltipAbilityDuration = "Dur: 0.2 second charge time";
					tooltipDescription = "Shoot a fireball. \nVery slow moving, but deals more\n damage when it has travelled  \n less distance. \nDeals 10-40 damage.";
				}
			}
			else if (abilityTwoPreviewSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				displayTooltip = 2;
				if (characterSelected == 1)
				{
					tooltipName = "Shapeshift";
					tooltipCooldown = "CD: 14 seconds (10 seconds effective)";
					tooltipAbilityDuration = "Dur: 4 seconds";
					tooltipDescription = "Transform into a hawk for 4 seconds. \n While in this form, you fall slower, \n and jump high and faster.";
				}
				else if (characterSelected == 2)
				{
					tooltipName = "Dash";
					tooltipCooldown = "CD: 10 seconds (8 seconds effective)";
					tooltipAbilityDuration = "Dur: 2 seconds";
					tooltipDescription = "Gain double movement speed for \n the duration. \n(Does not affect jumps)";
				}
				else if (characterSelected == 3)
				{
					tooltipName = "Shield Block";
					tooltipCooldown = "CD: 15 seconds (12 seconds effective)";
					tooltipAbilityDuration = "Dur: 3 seconds";
					tooltipDescription = "Use your shield to block projectiles. \n Destroys fireballs and arrows hitting \n it from the front.";
				}
				else if (characterSelected == 4)
				{
					tooltipName = "Thunderclap";
					tooltipCooldown = "CD: 14 seconds";
					tooltipAbilityDuration = "Dur: 1 second (after casting)";
					tooltipDescription = "Prepare a lighting storm. \nThe longer you charge, the further away \n from you it will spawn, and\n deal more damage.\n Deals 20-80 damage.";
				}
			}
			else if (abilityThreePreviewSprite.getGlobalBounds().contains(mousePositionGlobal.x, mousePositionGlobal.y) && gameHasFocus == true)
			{
				displayTooltip = 3;
				tooltipName = "LOCKED ABILITY";
				tooltipCooldown = "CD: ???";
				tooltipAbilityDuration = "Dur: ???";
				tooltipDescription = "This ability is so cool, \n you would die of awe \n if you saw it.";
			}
			else
			{
				displayTooltip = 0;
			}

			//Draw to screen
			gameWindow.setView(gameWindow.getDefaultView());

			gameWindow.clear();

			gameWindow.draw(mainMenuBackgroundSprite);
			gameWindow.draw(mainMenuBackSprite);
			gameWindow.draw(selectButtonSprite);

			gameWindow.draw(selectScreenSprite);

			gameWindow.draw(characterPreviewSprite);

			gameWindow.draw(abilityOnePreviewSprite);
			gameWindow.draw(abilityTwoPreviewSprite);
			gameWindow.draw(abilityThreePreviewSprite);

			gameWindow.draw(hpText);
			gameWindow.draw(msText);

			if (displayTooltip != 0)
			{
				gameWindow.draw(tooltipBox);

				tooltipText.setColor(sf::Color::Red);
				tooltipText.setCharacterSize(35);
				tooltipText.setPosition(570.f, 130.f);
				tooltipText.setString(tooltipName);
				gameWindow.draw(tooltipText);

				tooltipText.setColor(sf::Color::Magenta);
				tooltipText.setCharacterSize(25);
				tooltipText.setPosition(570.f, 170.f);
				tooltipText.setString(tooltipCooldown);
				gameWindow.draw(tooltipText);

				tooltipText.setPosition(570.f, 200.f);
				tooltipText.setString(tooltipAbilityDuration);
				gameWindow.draw(tooltipText);

				tooltipText.setColor(sf::Color::White);
				tooltipText.setPosition(570.f, 230.f);
				tooltipText.setString(tooltipDescription);
				gameWindow.draw(tooltipText);
			}

			gameWindow.draw(archerButtonSprite);
			gameWindow.draw(rogueButtonSprite);
			gameWindow.draw(warriorButtonSprite);
			gameWindow.draw(wizardButtonSprite);

			gameWindow.display();
		}
	}
}
void initializeCharacterIcons()
{
	if (!selectScreenTexture.loadFromFile("Resources/Character Icons/selectScreen.png"))
	{
		std::cout << "Error loading select screen texture!\n";
	}
	selectScreenSprite.setTexture(selectScreenTexture);
	selectScreenSprite.setPosition(0.f, 0.f);
	if (!archerButtonTexture.loadFromFile("Resources/Character Icons/archer.png"))
	{
		std::cout << "Error loading archer button texture!\n";
	}
	archerButtonSprite.setTexture(archerButtonTexture);
	archerButtonSprite.setPosition(100.f, 20.f);
	if (!rogueButtonTexture.loadFromFile("Resources/Character Icons/rogue.png"))
	{
		std::cout << "Error loading rogue button texture!\n";
	}
	rogueButtonSprite.setTexture(rogueButtonTexture);
	rogueButtonSprite.setPosition(240.f, 20.f);
	if (!warriorButtonTexture.loadFromFile("Resources/Character Icons/warrior.png"))
	{
		std::cout << "Error loading warrior button texture!\n";
	}
	warriorButtonSprite.setTexture(warriorButtonTexture);
	warriorButtonSprite.setPosition(380.f, 20.f);
	if (!wizardButtonTexture.loadFromFile("Resources/Character Icons/wizard.png"))
	{
		std::cout << "Error loading wizard button texture!\n";
	}
	wizardButtonSprite.setTexture(wizardButtonTexture);
	wizardButtonSprite.setPosition(520.f, 20.f);

	//Player texture
	if (!playerTexture.loadFromFile("Resources/ArcherTest.png")) 
	{
		std::cout << "Error loading player texture!" << std::endl;
	}
	if (!warriorGuyTexture.loadFromFile("Resources/shieldGuyTest.png"))
	{
		std::cout << "Error loading shield guy texture!\n";
	}

	if (!wizardTexture.loadFromFile("Resources/wizardTest.png"))
	{
		std::cout << "Error loading wizard texture!\n";
	}

	if (!rogueTexture.loadFromFile("Resources/rogue.png"))
	{
		std::cout << "Error loading rogue texture!\n";
	}
}