#include "stdafx.h"
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

#include "audioManager.h"
#include "globalVariables.h"

//Forward declare functions
void initializeSoundEffects();

std::vector < soundEffect > soundEffectsList;

//Sound Effects
sf::SoundBuffer bowShootSoundBuffer;
sf::Sound bowShootSound;

sf::SoundBuffer arrowImpactSoundBuffer;
sf::Sound arrowImpactSound;

sf::SoundBuffer rubbleExplosionSoundBuffer;
sf::Sound rubbleExplosionSound;

sf::SoundBuffer playerJumpSoundBuffer;
sf::Sound playerJumpSound;

sf::SoundBuffer playerHitGroundSoundBuffer;
sf::Sound playerHitGroundSound;

sf::SoundBuffer hawkWingsFlapSoundBuffer;
sf::Sound HawkWingsFlapSound;

//Music
sf::Music ambientAbyssMusic;
sf::Music sunflowerDanceMusic;

void initializeSoundEffects()
{
	bowShootSoundBuffer.loadFromFile("Resources/Sound Effects/shootBow.wav");
	bowShootSound.setBuffer(bowShootSoundBuffer);

	arrowImpactSoundBuffer.loadFromFile("Resources/Sound Effects/arrowImpact.wav");
	arrowImpactSound.setBuffer(arrowImpactSoundBuffer);

	rubbleExplosionSoundBuffer.loadFromFile("Resources/Sound Effects/explosionImpact.wav");
	rubbleExplosionSound.setBuffer(rubbleExplosionSoundBuffer);

	playerJumpSoundBuffer.loadFromFile("Resources/Sound Effects/jumpSound.wav");
	playerJumpSound.setBuffer(playerJumpSoundBuffer);

	playerHitGroundSoundBuffer.loadFromFile("Resources/Sound Effects/playerLandImpact.wav");
	playerHitGroundSound.setBuffer(playerHitGroundSoundBuffer);

	hawkWingsFlapSoundBuffer.loadFromFile("Resources/Sound Effects/flappingWings.wav");
	HawkWingsFlapSound.setBuffer(hawkWingsFlapSoundBuffer);

	bowShootSound.setVolume(soundEffectsVolume);
	arrowImpactSound.setVolume(soundEffectsVolume);
	rubbleExplosionSound.setVolume(soundEffectsVolume);
	playerJumpSound.setVolume(soundEffectsVolume);
	playerHitGroundSound.setVolume(soundEffectsVolume);
	HawkWingsFlapSound.setVolume(soundEffectsVolume);
}