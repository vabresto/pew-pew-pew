#ifndef UPDATETOSERVER_CPP
#define UPDATETOSERVER_CPP
#pragma once
#include <SFML/System.hpp>

/*extern void updateToServer(int _packetHeader);
extern void updateToServer(int _packetHeader, std::string textMessage, int _teamAffiliation);
extern void updateToServer(int _packetHeader, sf::Vector2f newPlayerPosition, sf::Vector2f newPlayerVelocity);
extern void updateToServer(int _packetHeader, int _redTeamKills, int _blueTeamKills);
/*extern void updateToServer(int _packetHeader, std::vector < myPlayerNamespace::projectile > _bulletsList);*//*
extern void updateToServer(int _packetHeader, myPlayerNamespace::projectile _projectile);*/

extern void updateBackendToServer(unsigned int _packetSubHeader, std::string _code, std::string _username = " ", int _team = 1);
extern void updateBackendToServer(unsigned int _packetSubHeader, unsigned int _characterSelected, unsigned int _localPlayerNumber, int _team);
extern void updateBackendToServer(unsigned int _packetSubHeader, sf::Time _timeSpentInCurrentLevel);


extern void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage);
extern void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage, int _team);
extern void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage, unsigned int _playerNumber);
extern void updateChatToServer(unsigned int _packetSubHeader, std::string _chatMessage, bool _spectatorsOnly);


extern void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, std::string _username, int _team, int _characterType);
extern void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, 
	float _posX, float _posY, int _directionFacing, int _currentFrameX, int _currentFrameY, float _health);
extern void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, sf::Time _deathTime);
extern void updateCharacterToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _characterType, unsigned int _textureNumber, unsigned int _textureSubNumber);


extern void updateScoreToServer(unsigned int _packetSubHeader, int _changeInScore, int _team);
extern void updateScoreToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _changeInResource, int _resource, int _team);


extern void updateParticlesToServer(unsigned int _packetSubHeader, sf::Vector2f _bulletPos, sf::Vector2f _velocity, int _team, float _damageDealt);
extern void updateParticlesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _particleType, unsigned int _posY = 0);


extern void updateAbilitiesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownTime, 
	float _damageDealt, sf::Time _timeCharged);
extern void updateAbilitiesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownLeft);
extern void updateAbilitiesToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, bool _isStarting, bool _isDoing, bool _isEnding, bool _isActive);


extern void updateStatusEffectsToServer(unsigned int _packetSubHeader, unsigned int _playerNumber, unsigned int _effectID, int _value, 
	float _damageDealt, sf::Time _durationLeft);


#endif