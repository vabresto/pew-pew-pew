#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H
#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include "serverCode.h"
#include "player.h"
#include "tallMountainGem.h"

extern sf::Time elapsedTime;
//extern sf::Time timeSinceAnimChange;

extern sf::IpAddress localLANIPAddress;
extern sf::IpAddress localIPAddress;
extern sf::IpAddress recipientIPAddress;
extern sf::IpAddress senderIPAddress;

extern sf::SocketSelector socketSelector;
extern sf::UdpSocket UDPSocket;
extern sf::TcpSocket TCPSocket;
extern sf::TcpListener TCPListener;

extern sf::Packet outPacket, inPacket;
extern int packetHeader;
extern int packetSubHeader;

extern std::string testData;

extern unsigned short UDPSendPort, TCPSendPort, UDPRecievePort, TCPRecievePort;

extern bool gameHasFocus;
extern bool isNewPlayerNPC;

extern sf::RectangleShape outline;

extern const sf::Time timePerFrame;
extern sf::Clock mainGameClock;
extern sf::Time timeSinceLastUpdate;
extern sf::Clock slowDebuffClock;

extern int main();
extern void openMap(std::string fileLocation);
extern void playLevelOne();
extern void scroll(float x, float y);
extern void FillTargetsList();


extern sf::RenderWindow gameWindow;
extern int currentFrameX, currentFrameY;

extern bool isGameOngoing;

extern bool displayFPS;
extern bool closeWindow;

extern float playerPosX, playerPosY;
extern int bulletPosX, bulletPosY;
extern std::vector < std::vector < sf::Vector2i > > map;
extern std::vector < sf::Vector2i > tempMap;

extern bool bulletsfired;
extern bool leaveDestroyedArrows;

extern int mapSizeX, mapSizeY, mapTileSize;
extern sf::Texture tileTexture;
extern sf::Sprite tiles;

extern bool enableScroll;
extern bool scrollRight, scrollLeft, scrollUp, scrollDown;

extern 	sf::Time timeSinceLastWalkParticle;

extern std::fstream openfile;
extern std::string fileLocation;
extern std::fstream connectionInfoFile;
extern std::string tileLocation;

extern int updatedNPC;

extern int screenSizeX;
extern int screenSizeY;
extern sf::View view1;
extern sf::Font arial;

extern float redTeamSpawnX, redTeamSpawnY, blueTeamSpawnX, blueTeamSpawnY;

extern sf::Sprite backgroundSprite;

extern sf::Vector2f viewCenter;
extern sf::Vector2f viewSize;

extern float scrollSpeed;

extern sf::Vector2f mousePositionGlobal;
extern sf::Vector2i mousePositionLocal;

extern const float maxY;
extern sf::Vector2f velocity;

extern int bulletCount;
extern int targetsCount;
extern int targetsKilledCount;

extern int redTeamKills;
extern int blueTeamKills;

extern sf::Text playerHealthText;
extern sf::Text deathScreenTimerText;

extern sf::Texture playerTexture;
extern sf::Texture bulletTexture;
extern sf::Texture warriorGuyTexture;
extern sf::Texture wizardTexture;
extern sf::Texture rogueTexture;
extern sf::Texture deadPlayerTexture;

extern sf::Texture hawkTexture;


extern sf::Texture deathOverlayGreyTexture;
extern sf::Sprite deathOverlayGreySprite;
extern sf::Texture healthFloatyBoxTexture;
extern sf::Sprite healthFloatyBoxSprite;


extern sf::Texture enemyMonsterBaseTexture;
extern sf::Texture mountainGemTexture;

extern const sf::Vector2f gravity;

extern std::string username;

extern sf::Vector2f oldPlayerVelocity;
extern sf::Vector2f oldPlayerPosition;

extern bool isTyping;
extern bool canType;
extern std::string textMessage;
extern bool toAll;
extern int teamAffiliation;


extern sf::Text myText;
extern sf::Text typingText;
extern sf::Text redTeamScore, blueTeamScore;

struct messagingStruct
{
	std::string message;
	int team;
};

extern std::vector < messagingStruct > messagesList;

extern int numOfMessages;
extern unsigned int displayMessages;

extern int enableWinCondition;
extern bool startGame;
extern int teamWhichSentMessage;

extern sf::Vector2f bulletPos;
extern sf::Vector2f bulletVel;

extern int numOfRedPlayers;
extern int numOfBluePlayers;
extern int whichTeam;

extern std::vector < playerStatsServer > redTeamPlayerStatsServer;
extern std::vector < playerStatsServer > blueTeamPlayerStatsServer;

extern std::vector < playerStatsClient > redTeamPlayerStatsClient;
extern std::vector < playerStatsClient > blueTeamPlayerStatsClient;

extern std::vector < myPlayerNamespace::player > redTeamPlayers;
extern std::vector < myPlayerNamespace::player > blueTeamPlayers;

extern std::vector < myPlayerNamespace::player > playersList;
extern std::vector < playerStatsServer > playerStatsServerList;

extern bool redTeamPlayerUpdate, blueTeamPlayerUpdate;
extern int updatedPlayer;

extern int iAmPlayerNumber;
extern int localPlayerNumber;

extern sf::Texture characterTexture;

extern sf::Vector2f updatePlayerPositionVar;
extern sf::Vector2f updatePlayerVelocityVar;

extern std::string updatePlayerStringVar;
extern int updatePlayerTeamVar;

extern sf::Vector2f bulletStartPos;

extern int bulletTypeReceived, teamReceived;
extern float damageDealtReceieved;
extern bool isDestroyedReceived;
extern sf::Vector2f bulletPositionReceived, bulletVelocityReceived;

extern bool wasConnectedTo, isStillConnectedTo;

extern std::string messageToBeSent;

extern sf::Vector2f enemyStartPosition;

extern std::vector < computerID > connections;

extern bool addPlayerToList;

extern std::string serverStartGameCode;

extern int numberOfGameLoopsRan;

extern bool isLocalPlayer;

extern unsigned int drawMapX1, drawMapX2, drawMapY1, drawMapY2, tilesDrawn;

extern sf::Text playerNameDrawerText;

extern bool serverResendUpdateToSender;

extern sf::Text victoryText;

extern bool allPlayersReady;

extern int numOfPlayersReady;
extern unsigned int mapGeneratorSeed;

extern bool loadGem;

extern bool isGameLocal;

extern sf::Time timeSinceJumpParticles;

extern bool randomMapBeingPlayed;
extern sf::Time timeSpentInCurrentLevel;
extern sf::Time hawkFlapSpeed;
extern sf::Time timeSinceWingsFlap;

extern float soundEffectsVolume, musicVolume;

//extern float gemPosX, gemPosY;

//extern enemyMonster::tallMountainGem tallMountainGemObject;

#endif