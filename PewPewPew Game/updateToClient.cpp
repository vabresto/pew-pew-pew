#include "stdafx.h"
#include "player.h"
#include "globalVariables.h"
#include "enemies.h"
#include "updateToServer.h"
#include "updateToClient.h"

#include <iostream>
#include <stdio.h>
#include <SFML/Network.hpp>
/*
void updateToClient(int _packetHeader, std::string mapResourceToOpen, int playerNumber, std::string otherData, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	if (_packetHeader == 0)
	{
		packetHeader = 0;

		outPacket << packetHeader << mapResourceToOpen << playerNumber << numOfRedPlayers << numOfBluePlayers << otherData;

		if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
		{
			std::cout << "Error sending 'Start Game' to client!\n";
		}
		else
		{
			std::cout << "Sent 'Start Game' to client!\n";
		}

		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match requested operation!\n";
	}
}
void updateToClient(int _packetHeader, std::string textMessage, int _team, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	if (_packetHeader == 1)
	{
		packetHeader = 1;

		outPacket << packetHeader << textMessage << _team;
		if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
		{
			std::cout << "Error sending message to client: " << clientIP << ":" << clientPort << "!\n";
		}

		outPacket.clear();
	}
	else
	{
		std::cout << "Packet header does not match requested operation!\n";
	}
}
void updateToClient(int _packetHeader, int playerID, std::string playerName, int playerTeam, sf::Vector2f newPlayerPosition, sf::Vector2f newPlayerVelocity, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	if (_packetHeader == 2)
	{
		packetHeader = 2;
		outPacket << packetHeader << playerID << playerName << playerTeam << newPlayerPosition.x << newPlayerPosition.y << newPlayerVelocity.x << newPlayerVelocity.y;

		if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
		{
			std::cout << "Error updating player's position & velocity to server!\n";
		}
	}
	else
	{
		std::cout << "Packet header does not match requested operation!\n";
	}
}
void updateToClient(int _packetHeader, int _redTeamKills, int _blueTeamKills, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	if (_packetHeader == 3)
	{
		packetHeader = 3;
		outPacket << packetHeader << redTeamKills << blueTeamKills;

		if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
		{
			std::cout << "Error updating score to server!\n";
		}
	}
	else
	{
		std::cout << "Packet header does not match requested operation!\n";
	}
}
void updateToClient(int _packetHeader, int _bulletType, sf::Vector2f _bulletPosition, sf::Vector2f _bulletVelocity, int _team, bool _isDestroyed, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	if (_packetHeader == 4)
	{
		bulletTypeReceived = _bulletType;
		teamReceived = _team;
		isDestroyedReceived = _isDestroyed;
		bulletPositionReceived = _bulletPosition;
		bulletVelocityReceived = _bulletVelocity;

		packetHeader = 4;
		outPacket << packetHeader << bulletTypeReceived << bulletPositionReceived.x << bulletPositionReceived.y << bulletVelocityReceived.x << bulletVelocityReceived.y << teamReceived << isDestroyedReceived;

		if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
		{
			std::cout << "Error updating bullets to server!\n";
		}
	}
	else
	{
		std::cout << "Packet header does not match requested operation!\n";
	}
}
*/

void updateBackendToClient(unsigned int _packetSubHeader, std::string _code, int _team, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 0;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for backend confirmation packets!\n";
	}	

	outPacket << packetHeader << packetSubHeader << _code;

	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending code to client: " << clientIP << ":" << clientPort << "!\n";
	}
	else
	{
		std::cout << "Sent code to client: " << clientIP << ":" << clientPort << "!\n";
	}

	outPacket.clear();
}
void updateBackendToClient(unsigned int _packetSubHeader, std::string _mapLocation, int _iAmPlayerNumber, std::string _code, float _redTeamSpawnX, float _redTeamSpawnY, float _blueTeamSpawnX, float _blueTeamSpawnY, int _scoreToWin, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 0;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for backend confirmation packets!\n";
	}	

	outPacket << packetHeader << packetSubHeader << _mapLocation << _iAmPlayerNumber << numOfRedPlayers << numOfBluePlayers << _code << _redTeamSpawnX << _redTeamSpawnY << _blueTeamSpawnX << _blueTeamSpawnY << _scoreToWin;

	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending 'Start Game' to client!\n";
	}
	else
	{
		std::cout << "Sent 'Start Game' to client!\n";
	}

	outPacket.clear();
}

void updateBackendToClient(unsigned int _packetSubHeader, sf::Time _timeSpentInCurrentLevel, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 0;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 4)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for time + score update packets!\n";
	}	

	outPacket << packetHeader << packetSubHeader << _timeSpentInCurrentLevel.asSeconds() << redTeamKills << blueTeamKills;

	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending time + score to client: " << clientIP << ":" << clientPort << "!\n";
	}

	outPacket.clear();
}


void updateChatToClient(unsigned int _packetSubHeader, std::string _chatMessage, int _team, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 1;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for team chat message packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _chatMessage << _team;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending team chat message!\n";
	}
	outPacket.clear();
}



void updateCharacterToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, std::string _username, int _team, int _characterType, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for player spawn packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _username << _team << _characterType;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending player spawn packet!\n";
	}
	outPacket.clear();
}
void updateCharacterToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, 
	float _posX, float _posY, int _directionFacing, int _currentFrameX, int _currentFrameY, float _health, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for player update packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _posX << _posY << _directionFacing << _currentFrameX << _currentFrameY << _health;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending player update packet!\n";
	}
	outPacket.clear();
}
void updateCharacterToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _characterType, unsigned int _textureNumber, unsigned int _textureSubNumber, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 2;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 3)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for texture change packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _characterType << _textureNumber << _textureSubNumber;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending texture change packet!\n";
	}
	outPacket.clear();
}




void updateScoreToClient(unsigned int _packetSubHeader, int _changeInScore, int _team, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 3;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for score packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _changeInScore << _team;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending score message!\n";
	}
	outPacket.clear();
}




void updateParticlesToClient(unsigned int _packetSubHeader, sf::Vector2f _bulletPos, sf::Vector2f _velocity, int _team, float _damageDealt, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 4;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for arrow packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _bulletPos.x << _bulletPos.y << _velocity.x << _velocity.y << _team << _damageDealt;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending arrow message!\n";
	}
	outPacket.clear();
}
void updateParticlesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _particleType, unsigned int _posY, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 4;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for particle spawn packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _particleType << _posY;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending particle spawn message!\n";
	}
	outPacket.clear();
}




void updateAbilitiesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownTime, float _damageDealt, sf::Time _timeCharged, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 5;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for particle spawn packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _ability << _abilityNumber << _cooldownTime.asMilliseconds() << _damageDealt << _timeCharged.asMilliseconds();
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending particle spawn message!\n";
	}
	outPacket.clear();
}
void updateAbilitiesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, sf::Time _cooldownLeft, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 5;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 1)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for ability cooldown packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _ability << _abilityNumber << _cooldownLeft.asMilliseconds();
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending ability cooldown message!\n";
	}
	outPacket.clear();
}
void updateAbilitiesToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, int _ability, int _abilityNumber, bool _isStarting, bool _isDoing, bool _isEnding, bool _isActive, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 5;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 2)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for ability management packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _ability << _abilityNumber << _isStarting << _isDoing << _isEnding << _isActive;
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending ability management message!\n";
	}
	outPacket.clear();
}



void updateStatusEffectsToClient(unsigned int _packetSubHeader, unsigned int _playerNumber, unsigned int _effectID, int _value, float _damageDealt, sf::Time _durationLeft, sf::IpAddress clientIP, unsigned short clientPort)
{
	outPacket.clear();
	packetHeader = 6;
	packetSubHeader = _packetSubHeader;

	if (packetSubHeader != 0)
	{
		std::cout << "Packet header " << packetSubHeader << " does not match proper sub header for status effect packets!\n";
	}

	outPacket << packetHeader << packetSubHeader << _playerNumber << _effectID << _value << _damageDealt << _durationLeft.asMilliseconds();
	if (UDPSocket.send(outPacket, clientIP, clientPort) != sf::Socket::Done)
	{
		std::cout << "Error sending ability cooldown message!\n";
	}
	outPacket.clear();
}