// PewPewPew Game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <SFML/Graphics.hpp>

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <cctype>
#include <string>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include <time.h>

#include "globalVariables.h"
#include "mainMenuElements.h"
#include "player.h"
#include "enemies.h"
#include "updateToServer.h"
#include "serverCode.h"
#include "tallMountainGem.h"
#include "animationManager.h"
#include "audioManager.h"
#include "HUDManager.h"
#include "drawGameToScreen.h"
#include "updateLocalGame.h"
#include "scrollView.h"
#include "receiveUpdateFromServer.h"
#include "updateTimers.h"
#include "abilitiesManager.h"
#include "NPC.h"

#include "Characters/archer.h"

//Declerations of functions
int main();
void openMap(std::string fileLocation);
void playLevelOne();
void scroll(float x, float y);
void FillTargetsList();

//Definitions of functions
int main()
{
	std::cout << "Pewpewpew Game is in the 'Prototype' stage at the moment!\nNothing is final!\nCopyright (haha) Victor Brestoiu 2015\n";
	gameWindow.create(sf::VideoMode(screenSizeX, screenSizeY, mapTileSize), "Pewpewpew Game - Prototype", sf::Style::Close);

	initializeMenuText();

	mainMenu();
	
	return 0;
}
void openMap(std::string fileLocation)
{
	map.clear();
	//std::cout << "Please enter a file location to be opened:\n";
	//std::cin >> fileLocation;

	//fileLocation = "Resources/testPlatformer.txt";

	std::fstream openfile(fileLocation);

	//Read the first line of the file, which has the location of the tilemap image

	//Open the map file itself
	//std::ifstream openfile("Resources/map.txt");

	//2D vector (array) of sf::Vector2i (which are basically co-ordinate pairs)
	//This will be our map itself
	/*std::vector < std::vector < sf::Vector2i > > map;
	//Variable tempMap is used to store the 'line' of the map (x,y) (0,0),(0,1),(0,2) etc
	//Used to make life simpler
	std::vector < sf::Vector2i > tempMap;*/


	//NOTE: THIS METHOD LEAVES AN EMPTY BAR AT THE TOP
	//OF EXACTLY ONE TILE
	//
	//For this project, that's fine - I can put the GUI there
	//For other projects, the method may need to be redone

	//Checking if the file is opened
	if (openfile.is_open())
	{
		std::cout << "File location: " << fileLocation << " was opened successfully!\n";

		//Store the location so we can access it
		openfile >> tileLocation;

		std::cout << tileLocation << std::endl;

		openfile >> mapSizeX;
		openfile >> mapSizeY;
		openfile >> mapTileSize;


		std::cout << mapSizeX << " " << mapSizeY << " " << mapTileSize << std::endl;

		//Load the texture
		//Note: We can't utilize it until we leave the loop
		//or else the compiler will get confused
		if(!tileTexture.loadFromFile(tileLocation))
		{
			std::cout << "Error loading texture!\n";
		}
		tiles.setTexture(tileTexture);

		/***********************************************/
		//Configure some settings
		//Code works fine - Commented for now to make life a bit easier
		/**********************************************/
		/*
		int textureMode;
		std::cout << "Which texture settings mode would you like to use?\n(Please enter either '0' for false or '1' for true)\n";
		std::cin >> textureMode;

		if (textureMode == 0)
		{
		textureInputMode = false;
		}
		else if (textureMode == 1)
		{
		textureInputMode = true;
		}
		else
		{
		std::cout << "Invalid input. Defaulting to true.\n";
		textureInputMode = true;
		}
		*/



		//While we haven't reached the end of the file (eof)
		for (int i = 0; i <= mapSizeY; i++)
		{
			//Declare two variables we will use to parse the contents of the map.txt file
			std::string str, value;

			//Read the first line of the map (file)
			std::getline(openfile, str);
			//Declare a string stream - used to convert to and from strings
			std::stringstream stream(str);

			//Use this to seperate the stream (which contains one row of the map)
			//Stream represents input, value represents the value we want to extract from the stream
			//and ' ' tells the computer that the data is seperated by spaces - we want to
			//process the data between the spaces
			while (std::getline(stream, value, ' '))
			{
				if (value.length() > 0)
				{
					//variable xx is the value between 0 (the start of the co-ordinate pair)
					//and ',' - the comma that we use to seperate the x and y info
					std::string xx = value.substr(0, value.find(','));
					//Same as above, but y starts at the ',' and goes until the end of the co-ordinate pair
					//+1 for formatting reasons
					std::string yy = value.substr(value.find(',') + 1);

					//Declare working variables 
					int x,y;
					unsigned int xIterator,yIterator;

					//Check if the x co-ordinate is a number (designating a tile) or not
					//Which shows up as a blank space (also protection against typos or other errors)
					//This means we know all the characters are digits
					//
					//NOTE: DO NOT PUT int xIterator! We are not redeclaring it!
					//
					for (xIterator = 0; xIterator < xx.length(); xIterator++)
					{
						if(!isdigit(xx[xIterator]))
							break;
					}

					//Same check as above, for y co-ordinate
					for (yIterator = 0; yIterator < yy.length(); yIterator++)
					{
						if(!isdigit(yy[yIterator]))
							break;
					}

					//If xIterator is equal to the length of xx, convert that string to an integer
					//If it is NOT equal, set it to -1
					//Set to 0 since we know that at least one character is NOT a digit

					//NOTE: We are setting any undefined slots to (0,0) because that is where the 'default' tile
					//is located on the spritesheet
					x = (xIterator == xx.length()) ? atoi(xx.c_str()) : 0;
					//Same as for X
					y = (yIterator == yy.length()) ? atoi(yy.c_str()) : 0;


					//Now that we know the data of our co-ordinate, send it back to the temporary storage
					//So we can insert it into the final map vector array
					tempMap.push_back(sf::Vector2i(x,y));
				}
			}

			//Transfer contents from tempMap to actual map
			map.push_back(tempMap);

			//Clear tempMap so we can start working on the next row
			tempMap.clear();
		}

		//Next stuff to do with file
		std::cout << "Yay!" << std::endl;
	}	

	else
	{
		std::cout << "Woops!" << std::endl;
	}
	openfile.close();
}
void playLevelOne()
{
	//Set up
	
	/*char localOrOnline;
	std::cout << "[L]ocal or [O]nline: ";
	std::cin >> localOrOnline;

	if (localOrOnline == 'o' || localOrOnline == 'O')
	{
		isGameLocal = false;
	}
	else
	{
		isGameLocal = true;
	}*/

	/*********************/
	//Creating Game Objects
	/*********************/
	//Player texture
	if (!playerTexture.loadFromFile("Resources/ArcherTest.png")) 
	{
		std::cout << "Error loading player texture!" << std::endl;
	}

	if (!hawkTexture.loadFromFile("Resources/hawk.png"))
	{
		std::cout << "Error loading hawk texture!\n";
	}

	if (!warriorGuyTexture.loadFromFile("Resources/shieldGuyTest.png"))
	{
		std::cout << "Error loading shield guy texture!\n";
	}

	if (!wizardTexture.loadFromFile("Resources/wizardTest.png"))
	{
		std::cout << "Error loading wizard texture!\n";
	}

	if (!rogueTexture.loadFromFile("Resources/rogue.png"))
	{
		std::cout << "Error loading rogue texture!\n";
	}

	if (!deadPlayerTexture.loadFromFile("Resources/Particles/deadPlayer.png"))
	{
		std::cout << "Error loading dead player texture!\n";
	}

	if (!deathOverlayGreyTexture.loadFromFile("Resources/HUD/deathOverlayGrey.png"))
	{
		std::cout << "Error loading death overlay (grey) texture!\n";
	}
	deathOverlayGreySprite.setTexture(deathOverlayGreyTexture);

	if (!healthFloatyBoxTexture.loadFromFile("Resources/HUD/healthFloatyBox.png"))
	{
		std::cout << "Error loading health floaty box texture!\n";
	}
	healthFloatyBoxSprite.setTexture(healthFloatyBoxTexture);

	//Outline
	outline.setOutlineColor(sf::Color::Red);
	outline.setOutlineThickness(1);
	outline.setFillColor(sf::Color(0,0,0,0));

	//Bullet Texture
	if (!bulletTexture.loadFromFile("Resources/arrowTestSprite.png")) 
	{
		std::cout << "Error loading bullet texture!" << std::endl;

	}

	//Tall Mountain Gem Texture
	if (!mountainGemTexture.loadFromFile("Resources/tallMountainGem.png"))
	{
		std::cout << "Eror loading gem texture!" << std::endl;
	}

	gemList.push_back(enemyMonster::tallMountainGem(mountainGemTexture, 400.f, 200.f));

	//Target Dummies Texture
	if (!enemyMonsterBaseTexture.loadFromFile("Resources/testTargetSprite.png"))
	{
		std::cout << "Error loading enemy texture!" << std::endl;
	}

	/***********************/
	//Create the Window
	/***********************/
	//sf::RenderWindow gameWindow(sf::VideoMode(screenSizeX, screenSizeY, mapTileSize), "Pewpewpew Game", sf::Style::Close);
	//gameWindow.create(sf::VideoMode(screenSizeX, screenSizeY, mapTileSize), "Pewpewpew Game", sf::Style::Close);
	//Artificially cap FPS to keep memory and CPU usage low
	gameWindow.setFramerateLimit(60);
	//view1.setCenter((float)(screenSizeX / 2), (float)(screenSizeY / 2 + mapTileSize));

	gameWindow.setKeyRepeatEnabled(false);


	sf::Texture backgroundTexture;
	if (mapLocation != "Resources/randomMap.txt")
	{
		if (!backgroundTexture.loadFromFile("Resources/manlyBackground.jpg")) 
		{
			std::cout << "Error loading manly background texture!" << std::endl;
		}
	}
	else
	{
		if (!backgroundTexture.loadFromFile("Resources/greenCavernBackground.jpg")) 
		{
			std::cout << "Error loading green cavern background texture!" << std::endl;
		}
	}

	if (mapLocation == "Resources/arenaMap.txt" || testData == "Resources/arenaMap.txt")
	{
		if (!backgroundTexture.loadFromFile("Resources/cavernBackground.png")) 
		{
			std::cout << "Error loading cavern background texture!" << std::endl;
		}
	}

	
	backgroundSprite.setTexture(backgroundTexture);

	myText.setFont(arial);
	myText.setCharacterSize(15);

	playerNameDrawerText.setFont(arial);
	playerNameDrawerText.setCharacterSize(20);

	typingText.setFont(arial);
	typingText.setCharacterSize(15);
	typingText.setColor(sf::Color::Yellow);

	redTeamScore.setFont(arial);
	redTeamScore.setCharacterSize(30);
	redTeamScore.setColor(sf::Color::Red);

	blueTeamScore.setFont(arial);
	blueTeamScore.setCharacterSize(30);
	blueTeamScore.setColor(sf::Color::Blue);

	if (mapLocation == "Resources/randomMap.txt")
	{
		if (!ambientAbyssMusic.openFromFile("Resources/Ambient Sound/abyssSound1.wav"))
		{
			std::cout << "Error loading abyss sound!\n";
		}
		ambientAbyssMusic.setLoop(true);
		ambientAbyssMusic.play();
		ambientAbyssMusic.setVolume(musicVolume);
	}
	else if (mapLocation == "Resources/testPlatformerTallMountainLevel.txt")
	{
		if(!sunflowerDanceMusic.openFromFile("Resources/Ambient Sound/sunflowerMusic.wav"))
		{
			std::cout << "Error loading sunflower music!\n";
		}
		sunflowerDanceMusic.setLoop(true);
		sunflowerDanceMusic.play();
		sunflowerDanceMusic.setVolume(musicVolume);
	}

	//Populate initial game with target dummies
	//fillTargetsList();
	
	//Identify which player this client is controlling
	/*for (unsigned int i = 0; i < playersList.size(); i++)
	{
		if (iAmPlayerNumber == playersList[i].playerID)
		{
			localPlayerNumber = i;
		}
	}*/

	//Set view center before game starts (it's wrong atm)
	//view1.setCenter(playersList[localPlayerNumber].mSprite.getPosition().x + 536, playersList[localPlayerNumber].mSprite.getPosition().y - 19 * mapTileSize);

	//view1.setCenter((float)536, (float)302);

	//Clear anything from before initialization
	inPacket.clear();
	outPacket.clear();

	messagesList.clear();

	gameWindow.clear();
	if (isGameLocal == true)
	{
		timeSpentInCurrentLevel = sf::Time::Zero;
	}

	initializeGameHUD();
	HUDAbilitiesGraphicSprite.setPosition(view1.getCenter().x - 200, view1.getCenter().y + 206);
	backgroundSprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 304);

	/*sf::Vector2f warriorGuySpawnPoint;
	warriorGuySpawnPoint.x = 400.f;
	warriorGuySpawnPoint.y = 12700.f;
	spawnWarrior(warriorGuySpawnPoint);*/

	/***********************/
	//Run the window
	/**********************/
	while (gameWindow.isOpen())
	{	
		elapsedTime = mainGameClock.restart();
		timeSinceLastUpdate += elapsedTime;		

		//Check if enough time has elapsed to run calculations again
		if (closeWindow == false)
		{
			while (timeSinceLastUpdate > timePerFrame && closeWindow == false)
			{
				timeSinceLastUpdate -= timePerFrame;

				updateTimers();

				receiveUpdateFromServer();

				/***********************************/
				//Window events loop
				/**********************************/
				sf::Event event;
				while (gameWindow.pollEvent(event))
				{
					if (event.type == sf::Event::KeyPressed && gameHasFocus == true)
					{
						if ((event.key.code == sf::Keyboard::C || event.key.code == sf::Keyboard::K) && isTyping == false)
						{	
							if (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft <= sf::Time::Zero && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
						 	{
								/*if (playersList[localPlayerNumber].abilitiesVector[8].ability != 15)
								{
									playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft = playersList[localPlayerNumber].abilitiesVector[8].cooldownTime;
								}*/
								playersList[localPlayerNumber].abilitiesVector[8].isActive = true;
								playersList[localPlayerNumber].abilitiesVector[8].isStartingAbility = true;

								/*if (isGameLocal == false)
								{
									updateAbilitiesToServer(2, localPlayerNumber, 8, playersList[localPlayerNumber].abilitiesVector[8].ability, playersList[localPlayerNumber].abilitiesVector[8].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[8].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[8].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[8].isActive);
								}*/

								/*if (playersList[localPlayerNumber].abilitiesVector[8].ability == 1 && playersList[localPlayerNumber].abilitiesVector[7].ability == 1)
								{
									playersList[localPlayerNumber].isShooting = false;
									playersList[localPlayerNumber].abilitiesVector[7].isActive = false;
								}*/
							}
						}
						if ((event.key.code == sf::Keyboard::Space) && isTyping == false)
						{
							if (playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft <= sf::Time::Zero && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								//playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft = playersList[localPlayerNumber].abilitiesVector[7].cooldownTime;

								playersList[localPlayerNumber].abilitiesVector[7].isActive = true;
								playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility = true;

								/*if (isGameLocal == false)
								{
								updateAbilitiesToServer(2, localPlayerNumber, 7, playersList[localPlayerNumber].abilitiesVector[7].ability, playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[7].isActive);
								}*/
							}
						}
					}

					/************************************/
					//Mouse Input Defined Here - THIS MAY NOT BE THE RIGHT PLACE FOR IT
					//BUT I DON't PLAN ON USING MOUSE CONTROL TOO MUCH
					/***********************************/
					//Get mouse position relative to the window

					//sf::Vector2f mousePositionGlobal = gameWindow.mapPixelToCoords(sf::Mouse::getPosition(gameWindow));
					//sf::Vector2i mousePositionLocal = sf::Mouse::getPosition(gameWindow);

					/**********************************/
					//Handling window events
					/*********************************/

					/*********************************/
					//DON'T PUT ANY OTHER EVENTS IN HERE
					//OR YOU'LL GET THE STUPID MOUSE-MOVEMENT-RUNS-GAMEPLAY
					//BUG, AND LIFE WILL SUCK
					//
					//DON'T FORGET!!!
					//
					/*******************************/

					if (event.type == sf::Event::Closed)
					{
						closeWindow = true;
						//gameWindow.close();
						if (isGameLocal == false)
						{
							//Send Disconnect code
							//updateToServer(0);
							updateBackendToServer(0, "_codeSuppliedByFunction");
						}
						gameEndScreen(0);
					}
					if (event.type == sf::Event::TextEntered && gameHasFocus == true && isGameLocal == false)
					{
						int textSize = messageToBeSent.size();
						unsigned short unicode = event.text.unicode;

						if (unicode == 13) //If return/enter
						{
							if (isTyping == true)
							{
								if (messageToBeSent.size() > 0)
								{
									//updateToServer(1, messageToBeSent, whichTeam);
									updateChatToServer(1, messageToBeSent, whichTeam);
								}
								messageToBeSent.clear();
								isTyping = false;
							}
							else
							{
								isTyping = true;
							}
						}
						if (isTyping == true)
						{
							if (unicode == 8) //If backspace
							{
								if (textSize > 0)
								{
									messageToBeSent.erase(textSize - 1, 1);
								}
							}
							else if (unicode >= 32 && unicode <= 126)
							{
								messageToBeSent += (char)unicode;
							}
							else if (unicode >= 192 && unicode <= 255)
							{
								messageToBeSent += (char)unicode;
							}
						}
					}
					if (event.type == sf::Event::GainedFocus)
					{
						gameHasFocus = true;
					}
					if (event.type == sf::Event::LostFocus)
					{
						gameHasFocus = false;
					}
					if (event.type == sf::Event::KeyReleased)
					{
						if (event.key.code == sf::Keyboard::Escape)
						{
							if (isGameLocal == false)
							{
								//Send Disconnect code
								//updateToServer(0);
								updateBackendToServer(0, "_codeSuppliedByFunction");
							}
							gameEndScreen(0);
						}
					}
				}

				//Update mouse position (again) (?)
				//Might be better to do it here
				/*if (event.type == sf::Event::MouseMoved)
				{
				mousePositionLocal.x = event.mouseMove.x;
				mousePositionLocal.y = event.mouseMove.y;
				}*/

				//spawnShieldBlockParticles();
				scrollView();

				/************************/
				//Game Updates
				/***********************/
				//Player movement
				if (isTyping == false && gameHasFocus == true)
				{
					//Key Released
					if (event.type == sf::Event::KeyReleased)
					{
						if (event.key.code == sf::Keyboard::D || event.key.code == sf::Keyboard::Right)
						{

							if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
							{
								playersList[localPlayerNumber].playerVelocity.x -= playersList[localPlayerNumber].playerSpeed;
							}
							else
							{
								playersList[localPlayerNumber].playerVelocity.x = 0.f;
							}
						}
						if (event.key.code == sf::Keyboard::A || event.key.code == sf::Keyboard::Left)
						{

							if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
							{
								playersList[localPlayerNumber].playerVelocity.x += playersList[localPlayerNumber].playerSpeed;
							}
							else
							{
								playersList[localPlayerNumber].playerVelocity.x = 0.f;
							}
						}
						if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
						{	
							if(playersList[localPlayerNumber].doubleJumpAllowed == false)
							{
								playersList[localPlayerNumber].doubleJump = false;
								playersList[localPlayerNumber].doubleJumpAllowed = true;
							}
						}
						if ((event.key.code == sf::Keyboard::Space) && isTyping == false)
						{
							if (playersList[localPlayerNumber].isShooting == true && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility = false;
								playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility = true;
								playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility = false;
								playersList[localPlayerNumber].abilitiesVector[7].cooldownLeft = playersList[localPlayerNumber].abilitiesVector[7].cooldownTime;
							}
							/*if (isGameLocal == false)
							{
								updateAbilitiesToServer(2, localPlayerNumber, 7, playersList[localPlayerNumber].abilitiesVector[7].ability, playersList[localPlayerNumber].abilitiesVector[7].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[7].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[7].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[7].isActive);
							}*/
							
						}
						if ((event.key.code == sf::Keyboard::C || event.key.code == sf::Keyboard::K) && isTyping == false)
						{
							if (playersList[localPlayerNumber].abilitiesVector[8].ability == 15 && playersList[localPlayerNumber].abilitiesVector[8].isStartingAbility == true && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								playersList[localPlayerNumber].abilitiesVector[8].isStartingAbility = false;
								playersList[localPlayerNumber].abilitiesVector[8].isDoingAbility = true;
								playersList[localPlayerNumber].abilitiesVector[8].isEndingAbility = false;

								/*if (isGameLocal == false)
								{
									updateAbilitiesToServer(2, localPlayerNumber, 8, playersList[localPlayerNumber].abilitiesVector[8].ability, playersList[localPlayerNumber].abilitiesVector[8].isStartingAbility, playersList[localPlayerNumber].abilitiesVector[8].isDoingAbility, playersList[localPlayerNumber].abilitiesVector[8].isEndingAbility, playersList[localPlayerNumber].abilitiesVector[8].isActive);
								}*/
							}
						}
						
						if (event.key.code == sf::Keyboard::Return)
						{
							canType = false;
						}
					}

					//TODO: Place this where it belongs
					playersList[localPlayerNumber].abilitiesVector[8].durationLeft -= timePerFrame;

					

					//Direct input for movement
					//Remove part after '&&' on A and D for slightly more fluid movement
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
					{
						if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false  && playersList[localPlayerNumber].abilitiesVector[8].ability == 1)
						{
							//For independent jump vs fall speed, comment below line
							if (playersList[localPlayerNumber].playerVelocity.y >= -playersList[localPlayerNumber].maxNormalJumpSpeed * 2)
							{
								//playersList[localPlayerNumber].playerVelocity.y -= playersList[localPlayerNumber].playerJumpSpeed;
								if (playersList[localPlayerNumber].isJumping == false)
								{
									playersList[localPlayerNumber].playerVelocity.y -= playersList[localPlayerNumber].playerJumpSpeed;	
									playersList[localPlayerNumber].isJumping = true;

									if (timeSinceJumpParticles > sf::seconds((float)0.2) && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
									{
										spawnJumpDustParticles(localPlayerNumber);
										if (isGameLocal == false)
										{
											updateParticlesToServer(2, localPlayerNumber, 1);
										}
										playerJumpSound.play();
										timeSinceJumpParticles = sf::Time::Zero;
									}
								}

								if (playersList[localPlayerNumber].isJumping == true && playersList[localPlayerNumber].doubleJump == false)
								{
									playersList[localPlayerNumber].playerVelocity.y = 0;
									playersList[localPlayerNumber].playerVelocity.y -= playersList[localPlayerNumber].playerJumpSpeed;
									playersList[localPlayerNumber].doubleJump = true;

									if (timeSinceJumpParticles > sf::seconds((float)0.2) && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
									{
										spawnJumpDustParticles(localPlayerNumber);
										if (isGameLocal == false)
										{
											updateParticlesToServer(2, localPlayerNumber, 1);
										}
										playerJumpSound.play();
										timeSinceJumpParticles = sf::Time::Zero;
									}
								}
							}
						}
						else if (playersList[localPlayerNumber].abilitiesVector[8].isActive == true && playersList[localPlayerNumber].abilitiesVector[8].ability == 1)
						{
							if (timeSinceWingsFlap > hawkFlapSpeed)
							{
								timeSinceWingsFlap = sf::Time::Zero;
								playersList[localPlayerNumber].playerVelocity.y -= (float)(playersList[localPlayerNumber].playerJumpSpeed * 1.5);
								if (playersList[localPlayerNumber].playerVelocity.y < - 18.f)
								{
									playersList[localPlayerNumber].playerVelocity.y = - 18.f;
								}
							}
						}
						else if (playersList[localPlayerNumber].abilitiesVector[8].ability != 1)
						{
							if (playersList[localPlayerNumber].playerVelocity.y >= -playersList[localPlayerNumber].maxNormalJumpSpeed * 2)
							{
								//playersList[localPlayerNumber].playerVelocity.y -= playersList[localPlayerNumber].playerJumpSpeed;
								if (playersList[localPlayerNumber].isJumping == false)
								{
									playersList[localPlayerNumber].playerVelocity.y -= playersList[localPlayerNumber].playerJumpSpeed;	
									playersList[localPlayerNumber].isJumping = true;

									if (timeSinceJumpParticles > sf::seconds((float)0.2) && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
									{
										spawnJumpDustParticles(localPlayerNumber);
										if (isGameLocal == false)
										{
											updateParticlesToServer(2, localPlayerNumber, 1);
										}
										playerJumpSound.play();
										timeSinceJumpParticles = sf::Time::Zero;
									}
								}

								if (playersList[localPlayerNumber].isJumping == true && playersList[localPlayerNumber].doubleJump == false)
								{
									playersList[localPlayerNumber].playerVelocity.y = 0;
									playersList[localPlayerNumber].playerVelocity.y -= playersList[localPlayerNumber].playerJumpSpeed;
									playersList[localPlayerNumber].doubleJump = true;

									if (timeSinceJumpParticles > sf::seconds((float)0.2) && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
									{
										spawnJumpDustParticles(localPlayerNumber);
										if (isGameLocal == false)
										{
											updateParticlesToServer(2, localPlayerNumber, 1);
										}
										playerJumpSound.play();
										timeSinceJumpParticles = sf::Time::Zero;
									}
								}
							}
						}
					}
					if ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) && (!sf::Keyboard::isKeyPressed(sf::Keyboard::D) || !sf::Keyboard::isKeyPressed(sf::Keyboard::Right)))
					{
						playersList[localPlayerNumber].directionFacing = 0;
						if (timeSinceLastWalkParticle > sf::seconds((float)0.025) && playersList[localPlayerNumber].isJumping == false)
						{
							if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false && playersList[localPlayerNumber].abilitiesVector[8].ability == 1 && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								spawnWalkDustParticles(localPlayerNumber);
								if (isGameLocal == false)
								{
									updateParticlesToServer(2, localPlayerNumber, 2);
								}
								timeSinceLastWalkParticle = sf::Time::Zero;
							}
							else if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false && playersList[localPlayerNumber].abilitiesVector[8].ability != 1 && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								spawnWalkDustParticles(localPlayerNumber);
								if (isGameLocal == false)
								{
									updateParticlesToServer(2, localPlayerNumber, 2);
								}
								timeSinceLastWalkParticle = sf::Time::Zero;
							}
							else
							{
								timeSinceLastWalkParticle = sf::Time::Zero;
							}
						}
						if (playersList[localPlayerNumber].playerVelocity.x >= -playersList[localPlayerNumber].playerMaxVelocity.x)
						{
							playersList[localPlayerNumber].playerVelocity.x -= playersList[localPlayerNumber].playerSpeed;
						}
						else 
						{
							playersList[localPlayerNumber].playerVelocity.x = -playersList[localPlayerNumber].playerMaxVelocity.x;
						}
						//playersList[localPlayerNumber].mSprite.setTextureRect(sf::IntRect(32,0,-32,48));
					}
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
					{
						playersList[localPlayerNumber].mSprite.setPosition(playersList[localPlayerNumber].mSprite.getPosition().x, playersList[localPlayerNumber].mSprite.getPosition().y + 2);
						outline.setPosition(outline.getPosition().x, outline.getPosition().y + 2);
					}
					if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && (!sf::Keyboard::isKeyPressed(sf::Keyboard::A) || !sf::Keyboard::isKeyPressed(sf::Keyboard::Left)))
					{
						playersList[localPlayerNumber].directionFacing = 1;
						if (timeSinceLastWalkParticle > sf::seconds((float)0.025) && playersList[localPlayerNumber].isJumping == false)
						{
							if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false && playersList[localPlayerNumber].abilitiesVector[8].ability == 1 && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								spawnWalkDustParticles(localPlayerNumber);
								if (isGameLocal == false)
								{
									updateParticlesToServer(2, localPlayerNumber, 2);
								}
								timeSinceLastWalkParticle = sf::Time::Zero;
							}
							else if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false && playersList[localPlayerNumber].abilitiesVector[8].ability != 1 && playersList[localPlayerNumber].deathTimer <= sf::Time::Zero)
							{
								spawnWalkDustParticles(localPlayerNumber);
								if (isGameLocal == false)
								{
									updateParticlesToServer(2, localPlayerNumber, 2);
								}
								timeSinceLastWalkParticle = sf::Time::Zero;
							}
							else
							{
								timeSinceLastWalkParticle = sf::Time::Zero;
							}
						}
						if (playersList[localPlayerNumber].playerVelocity.x <= playersList[localPlayerNumber].playerMaxVelocity.x)
						{
							playersList[localPlayerNumber].playerVelocity.x += playersList[localPlayerNumber].playerSpeed;
						}
						else
						{
							playersList[localPlayerNumber].playerVelocity.x = playersList[localPlayerNumber].playerMaxVelocity.x;
						}
						//playersList[localPlayerNumber].mSprite.setTextureRect(sf::IntRect(0,0,32,48));
					}
				}

				//Stop player x movements if no arrow key is pressed
				//Should be removed
				if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) == false && (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) == false)
				{
					playersList[localPlayerNumber].playerVelocity.x = 0.f;
				}

				updateLocalGame();

				drawGameToScreen();
			}
		}
		else
		{
			map.clear();
			closeWindow = false;
			gameEndScreen(0);
		}
	}
}
void scroll(float x, float y)
{
	if (( x > 0 || x < 0))
	{
		x = playersList[localPlayerNumber].playerVelocity.x;
	}
	/*if ((playersList[localPlayerNumber].abilitiesVector[8].ability == 1 && playersList[localPlayerNumber].abilitiesVector[8].isActive == true))
	{
		if ((int)playersList[localPlayerNumber].playerVelocity.y % 2 == 0)
		{
			//y = playersList[localPlayerNumber].playerVelocity.y;
		}
		else
		{
			//y = playersList[localPlayerNumber].playerVelocity.y - ((int)playersList[localPlayerNumber].playerVelocity.y % 2);
		}
		//y = y * 4;
	}*/

	//Sissy approach
	//view1.setCenter(playersList[localPlayerNumber].mSprite.getPosition().x, playersList[localPlayerNumber].mSprite.getPosition().y);

	view1.move(x,(float)(y*0.5));
	backgroundSprite.move(x,(float)(y*0.5));


	/*view1.move(x,(float)(y));
	backgroundSprite.move(x,(float)(y));*/
}
void FillTargetsList()
{
	srand(666);
	unsigned int numberOfMonsters = rand() % 10 + 1;

	srand((unsigned int) time(NULL));
	for (unsigned int i = 0; i < numberOfMonsters; i++)
	{
		enemyStartPosition = sf::Vector2f((float)(rand() % 1000 + 32), (float)(rand() % 200 + 32) );
		targetsList.push_back(enemyMonster::enemyMonsterBase(enemyMonsterBaseTexture, enemyStartPosition.x, enemyStartPosition.y));
		for (unsigned int i = 0; i < connections.size(); i++)
		{
			//Send packet to each client informing them on the spawn of the new target
		}
	}
}
