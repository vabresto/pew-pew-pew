#ifndef ABILITIESMANAGER_CPP_
#define ABILITIESMANAGER_CPP_

extern void startAbility(int _abilityNum, unsigned int actor);
extern void doAbility(int _abilityNum, unsigned int actor);
extern void endAbility(int _abilityNum, unsigned int actor);
extern void doAllAbilities(unsigned int actor);

#endif