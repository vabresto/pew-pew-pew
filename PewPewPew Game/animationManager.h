#ifndef ANIMATIONMANAGER_CPP_
#define ANIMATIONMANAGER_CPP_

extern void spawnParticlesFromServer(unsigned int actor, unsigned int _particleType, int _posY);
extern void animatePlayer(int _animatedPlayer);
extern void animateOnlinePlayer(int _animatedPlayer);
extern void initializeParticles();
extern void spawnJumpDustParticles(unsigned int actor);
extern void spawnWalkDustParticles(unsigned int actor);
extern void spawnHawkTransformParticles(unsigned int actor);
extern void spawnArrowChargeTimeParticles(unsigned int actor);
extern void spawnSwordSlashUpParticles(unsigned int actor);
extern void spawnShieldBlockParticles(unsigned int actor);
extern void spawnSwordSlashDownParticles(unsigned int actor);
extern void spawnFireballParticles(unsigned int actor);
extern void spawnThunderclapChargeParticles(unsigned int actor);
extern void spawnThunderclapMarkerParticles(unsigned int actor);
extern void spawnThunderclapCloudParticles(unsigned int actor);
extern void spawnThunderclapParticles(unsigned int _posY, unsigned int actor);
extern void spawnRogueSlashParticles(unsigned int actor);
extern void spawnRogueDashParticles(unsigned int actor);
extern void spawnCorpseParticles(unsigned int actor);

extern sf::Time timeSinceAnimChange;

extern sf::Texture smokeLandingParticlesTexture;
extern sf::Sprite smokeLandingParticlesSprite;

extern sf::Texture smokeTransformParticlesTexture;
extern sf::Sprite smokeTransformParticlesSprite;

struct particleEntity
{
	sf::Sprite sprite;
	unsigned int currentFrameX, currentFrameY, particleType, drawLayer;
	unsigned int directionFacing;
	bool destroyParticle;

	sf::Time timeSinceAnimChange;

	unsigned int spawnerPlayerNumber;

	int teamAffiliation;

	void update();
};

extern std::vector < particleEntity > particleEntitiesList;

#endif