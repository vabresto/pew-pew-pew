#include "stdafx.h"
#include "updateTimers.h"
#include "globalVariables.h"

void updateTimers()
{
	timeSinceLastWalkParticle += timePerFrame;
	timeSinceJumpParticles += timePerFrame;
	timeSpentInCurrentLevel += timePerFrame;
	timeSinceWingsFlap += timePerFrame;
}