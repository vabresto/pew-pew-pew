#include "stdafx.h"

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include <vector>
#include <iostream>

#include "player.h"
#include "enemies.h"
#include "globalVariables.h"
#include "serverCode.h"
#include "clientCode.h"


/*void runClient()
{
TCPRecievePort = 5000;
bool doneRecieving = false;
char mode = 'r';

TCPSocket.connect(localLANIPA]ddress, TCPRecievePort);
socketSelector.add(TCPSocket);
std::string testData;
TCPSocket.receive(inPacket);
inPacket >> testData;
std::cout << testData << std::endl;

while (!doneRecieving)
{
if (mode == 's')
{
std::getline(std::cin, testData);
outPacket << testData;

TCPSocket.send(outPacket);
mode = 'r';
outPacket.clear();
}
else if (mode == 'r')
{
TCPSocket.receive(inPacket);
if (inPacket.getDataSize() > 0)
{
inPacket >> testData;
std::cout << "Recieved: " << testData << std::endl;
mode = 's';
inPacket.clear();
}
}
else
{
std::cout << "Invalid mode setting!\n";
}
}

system("pause");
}*/
void runClient()
{
	bool done = false;
	std::cout << localLANIPAddress << std::endl;

	std::cout << "Enter IP Address to connect to: ";
	std::cin >> recipientIPAddress;
	std::cout << recipientIPAddress << std::endl;

	std::cout << "Enter connection port: ";
	std::cin >> UDPSendPort;
	std::cout << UDPSendPort << std::endl;

	if (UDPSocket.bind(UDPSocket.getLocalPort()) != sf::Socket::Done)
	{
		std::cout << "Error connecting to port " << UDPSocket.getLocalPort() << "! \n";
	}

	UDPSocket.setBlocking(false);
	socketSelector.add(UDPSocket);

	std::string connectionPassword = "applesbananascoconutsdontexist";
	outPacket << connectionPassword;

	if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
	{
		std::cout << "Error sending authentication to server!\n";
	}
	else
	{
		std::cout << "Sent authentication code to server!\n";
	}

	outPacket.clear();

	while (done == false)
	{
		

		//While there are packets in recieved queue
		while (UDPSocket.receive(inPacket, senderIPAddress, UDPRecievePort) == sf::Socket::Done)
		{
			while (inPacket >> testData)
			{
				std::cout << "Recieved " << testData << " from " << senderIPAddress << " on port " << UDPRecievePort << std::endl;
				//UDPSendPort = UDPRecievePort;
				inPacket.clear();
			}
		}

		//Update the server for this game loop
		outPacket.clear();
		std::cout << "Enter packet data: ";
		//std::getline(std::cin, testData);
		outPacket << testData;
		std::cout << "Loaded packet.\n";
		if (UDPSocket.send(outPacket, recipientIPAddress, UDPSendPort) != sf::Socket::Done)
		{
			std::cout << "Error sending packet!\n";
		}
		else
		{
			std::cout << "Packet sent!\n";
		}

	}

	system("pause");
}
