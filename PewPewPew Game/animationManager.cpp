#include "stdafx.h"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>
#include <fstream>
#include <math.h>

#include "animationManager.h"
#include "globalVariables.h"
#include "player.h"
#include "updateToServer.h"
#include "audioManager.h"

void initializeParticles();
void spawnParticlesFromServer(unsigned int actor, unsigned int _particleType);
void spawnJumpDustParticles(unsigned int actor);
void spawnWalkDustParticles(unsigned int actor);
void spawnHawkTransformParticles(unsigned int actor);
void spawnSwordSlashUpParticles(unsigned int actor);
void spawnSwordSlashDownParticles(unsigned int actor);
void spawnFireballParticles(unsigned int actor);
void spawnThunderclapChargeParticles(unsigned int actor);
void spawnThunderclapMarkerParticles(unsigned int actor);
void spawnThunderclapCloudParticles(unsigned int actor);
void spawnThunderclapParticles(unsigned int _posY, unsigned int actor);
void spawnRogueSlashParticles(unsigned int actor);
void spawnRogueDashParticles(unsigned int actor);


sf::Texture smokeLandingParticlesTexture;
sf::Sprite smokeLandingParticlesSprite;

sf::Texture smokeWalkingParticlesTexture;
sf::Sprite smokeWalkingParticlesSprite;

sf::Texture smokeTransformParticlesTexture;
sf::Sprite smokeTransformParticlesSprite;

sf::Texture swordSlashUpParticlesTexture;
sf::Sprite swordSlashUpParticlesSprite;

sf::Texture swordSlashDownParticlesTexture;
sf::Sprite swordSlashDownParticlesSprite;

sf::Texture shieldBlockParticlesTexture;
sf::Sprite shieldBlockParticlesSprite;

sf::Texture fireballParticlesTexture;
sf::Sprite fireballParticlesSprite;

sf::Texture thunderclapChargeParticlesTexture;
sf::Sprite thunderclapChargeParticlesSprite;

sf::Texture thunderclapMarkerParticlesTexture;
sf::Sprite thunderclapMarkerParticlesSprite;

sf::Texture rogueSlashParticlesTexture;
sf::Sprite rogueSlashParticlesSprite;

sf::Texture rogueDashParticlesTexture;
sf::Sprite rogueDashParticlesSprite;

sf::Texture rogueDeadParticlesTexture;
sf::Sprite rogueDeadParticlesSprite;

sf::Texture graveParticlesTexture;
sf::Sprite graveParticlesSprite;

std::vector < particleEntity > particleEntitiesList;

void initializeParticles()
{
	if (!smokeLandingParticlesTexture.loadFromFile("Resources/smokeParticlesTest.png"))
	{
		std::cout << "Error loading landing smoke texture!\n";
	}

	smokeLandingParticlesSprite.setTexture(smokeLandingParticlesTexture);

	if (!smokeWalkingParticlesTexture.loadFromFile("Resources/dustParticles(blue).png"))
	{
		std::cout << "Error loading walking smoke texture!\n";
	}

	smokeWalkingParticlesSprite.setTexture(smokeWalkingParticlesTexture);

	if (!smokeTransformParticlesTexture.loadFromFile("Resources/Particles/hawkTransformSmoke.png"))
	{
		std::cout << "Error loading hawk transform smoke texture!\n";
	}
	smokeTransformParticlesSprite.setTexture(smokeTransformParticlesTexture);

	if (!swordSlashUpParticlesTexture.loadFromFile("Resources/Particles/swipeUpAnim.png"))
	{
		std::cout << "Error loading swipe up animation particles texture!\n";
	}
	swordSlashUpParticlesSprite.setTexture(swordSlashUpParticlesTexture);

	if (!shieldBlockParticlesTexture.loadFromFile("Resources/Particles/shieldBlockAnim.png"))
	{
		std::cout << "Error loading shield block particles texture!\n";
	}
	shieldBlockParticlesSprite.setTexture(shieldBlockParticlesTexture);

	if (!swordSlashDownParticlesTexture.loadFromFile("Resources/Particles/swipeDownAnim.png"))
	{
		std::cout << "Error loading swipe down animation particles texture!\n";
	}
	swordSlashDownParticlesSprite.setTexture(swordSlashDownParticlesTexture);

	if (!fireballParticlesTexture.loadFromFile("Resources/Particles/fireball.png"))
	{
		std::cout << "Error loading fireball animation texture!\n";
	}
	fireballParticlesSprite.setTexture(fireballParticlesTexture);

	if (!thunderclapChargeParticlesTexture.loadFromFile("Resources/Particles/thunderclapChargeSparkAnim.png"))
	{
		std::cout << "Error loading thunderclap charge particles texture!\n";
	}
	thunderclapChargeParticlesSprite.setTexture(thunderclapChargeParticlesTexture);

	if (!thunderclapMarkerParticlesTexture.loadFromFile("Resources/Particles/thunderclapMarker.png"))
	{
		std::cout << "Error loading thunderclap marker particles texture!\n";
	}
	thunderclapMarkerParticlesSprite.setTexture(thunderclapMarkerParticlesTexture);

	if (!rogueSlashParticlesTexture.loadFromFile("Resources/Particles/rogueSlashAnim.png"))
	{
		std::cout << "Error loading rogue slash anim texture!\n";
	}
	rogueSlashParticlesSprite.setTexture(rogueSlashParticlesTexture);

	if (!rogueDashParticlesTexture.loadFromFile("Resources/Particles/rogueDash.png"))
	{
		std::cout << "Error loading rogue dash particle texture!\n";
	}
	rogueDashParticlesSprite.setTexture(rogueDashParticlesTexture);

	if (!rogueDeadParticlesTexture.loadFromFile("Resources/Particles/rogueDead.png"))
	{
		std::cout << "Error loading rogue dead particle texture!\n";
	}
	rogueDeadParticlesSprite.setTexture(rogueDeadParticlesTexture);

	if (!graveParticlesTexture.loadFromFile("Resources/Particles/grave.png"))
	{
		std::cout << "Error loading grave texture!\n";
	}
	graveParticlesSprite.setTexture(graveParticlesTexture);
}
void animatePlayer(int _animatedPlayer)
{
	playersList[_animatedPlayer].timeSinceAnimChange += timePerFrame;

	//Animate archer
	if (playersList[_animatedPlayer].abilitiesVector[8].isActive == false  && playersList[_animatedPlayer].abilitiesVector[8].ability == 1)
	{
		//TODO remove the isNPC == false flag, make animations same length for everything
		if (playersList[_animatedPlayer].isUsingAbilityAnimation == false && playersList[_animatedPlayer].isNPC == false)
		{
			//Moving Left
			if (playersList[_animatedPlayer].playerVelocity.x < 0 && playersList[_animatedPlayer].isShooting == false)
			{
				playersList[_animatedPlayer].currentFrameY = 2;
				//playersList[_animatedPlayer].directionFacing = 0;
				if (playersList[_animatedPlayer].currentFrameX == 1 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(32, 48, -32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 2 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(64, 48, -32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 3 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(96, 48, -32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 4 && (float) (playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(128, 48, -32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 5 && (float) (playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(160, 48, -32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX = 1;
				}
			}
			//Moving Right
			if (playersList[_animatedPlayer].playerVelocity.x > 0 && playersList[_animatedPlayer].isShooting == false)
			{
				playersList[_animatedPlayer].currentFrameY = 2;
				//playersList[_animatedPlayer].directionFacing = 1;
				if (playersList[_animatedPlayer].currentFrameX == 1 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(0, 48, 32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 2 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(32, 48, 32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 3 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(64, 48, 32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 4 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(96, 48, 32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX++;
				}
				if (playersList[_animatedPlayer].currentFrameX == 5 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1)) && ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))))
				{
					playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(128, 48, 32, 48));
					playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
					playersList[_animatedPlayer].currentFrameX = 1;
				}
			}


			//Jumping
			if (playersList[_animatedPlayer].isJumping == true)
			{
				/*if (playersList[_animatedPlayer].playerVelocity.x < 0)
				{
				playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(64, 96, -32, 48));
				playersList[_animatedPlayer].currentFrameX = 2;
				playersList[_animatedPlayer].currentFrameY = 3;
				}
				if (playersList[_animatedPlayer].playerVelocity.x > 0)
				{
				playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect(32, 96, 32, 48));*/
				if (playersList[_animatedPlayer].playerVelocity.y < 0)
				{
					playersList[_animatedPlayer].currentFrameX = 1;
					playersList[_animatedPlayer].currentFrameY = 3;
				}
				else
				{
					playersList[_animatedPlayer].currentFrameX = 2;
					playersList[_animatedPlayer].currentFrameY = 3;
				}
				//}
			}

			//Landing state
			if ((((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+10)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+10)/32].y) != 0 ) || 
				((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+20)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+20)/32].y) != 0 )))
			{
				playersList[_animatedPlayer].currentFrameX = 3;
				playersList[_animatedPlayer].currentFrameY = 3;
				playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			}

			//Leave the landing state
			if (playersList[_animatedPlayer].currentFrameX == 3 && playersList[_animatedPlayer].currentFrameY == 3 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.25)))
			{
				playersList[_animatedPlayer].currentFrameX = 1;
				playersList[_animatedPlayer].currentFrameY = 1;
			}

			//Holding a block (two points)
			if ((((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+8)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+4)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+8)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+4)/32].y) != 0 ) && 
				((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+35)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+4)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+35)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+4)/32].y) != 0 )))
			{
				playersList[_animatedPlayer].directionFacing = 1;
				playersList[_animatedPlayer].currentFrameX = 1;
				playersList[_animatedPlayer].currentFrameY = 5;
				//playersList[_animatedPlayer].currentFrameY = 6;
			}

			if (((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+8)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+28)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+8)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+28)/32].y) != 0 ) && 
				((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+35)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+28)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+35)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+28)/32].y) != 0 ))
			{
				playersList[_animatedPlayer].directionFacing = 0;
				playersList[_animatedPlayer].currentFrameX = 1;
				playersList[_animatedPlayer].currentFrameY = 5;
				//playersList[_animatedPlayer].currentFrameY = 6;
			}

			//Holding a block (one point)
			if ((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y + 45)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+4)/32].x) != 0 || 
				((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y + 45)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+28)/32].x) != 0))
			{
				playersList[_animatedPlayer].currentFrameX = 1;
				playersList[_animatedPlayer].currentFrameY = 6;
				//playersList[_animatedPlayer].currentFrameY = 5;

				//Anti-Daniel walls
				if ((((map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].x) == 2 && (map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].y) == 3 ) || 
					((map[(int)(playerPosY+48)/32][(int)(playerPosX + 32)/32].x) == 2 && (map[((int)playerPosY+48)/32][(int)(playerPosX + 32)/32].y) == 3 )) ||
					(((map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].x) == 3 && (map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].y) == 3 ) || 
					((map[(int)(playerPosY+48)/32][(int)(playerPosX + 32)/32].x) == 3 && (map[((int)playerPosY+48)/32][(int)(playerPosX + 32)/32].y) == 3 )))
				{

				}
				else
				{
					//if (playersList[_animatedPlayer].playerVelocity.y > 0)
					//{
					//playersList[_animatedPlayer].playerVelocity.y = 0;
				}
			}

		}

	}
	//Draw bird flapping wings
	else if (playersList[_animatedPlayer].abilitiesVector[8].isActive == true  && playersList[_animatedPlayer].abilitiesVector[8].ability == 1)
	{
		playersList[_animatedPlayer].currentFrameY = 1;

		if (playersList[_animatedPlayer].currentFrameX == 1 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.075)))
		{
			playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			playersList[_animatedPlayer].currentFrameX++;
		}
		if (playersList[_animatedPlayer].currentFrameX == 2 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.075)))
		{
			playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			playersList[_animatedPlayer].currentFrameX++;
		}
		if (playersList[_animatedPlayer].currentFrameX == 3 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.075)))
		{
			playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			playersList[_animatedPlayer].currentFrameX++;
		}
		if (playersList[_animatedPlayer].currentFrameX == 4 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.075)))
		{
			playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			playersList[_animatedPlayer].currentFrameX = 1;
		}
		if (playersList[_animatedPlayer].currentFrameX > 4 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.075)))
		{
			playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			playersList[_animatedPlayer].currentFrameX = 1;
		}
	}

	//Animate Wizard
	else if (playersList[_animatedPlayer].abilitiesVector[7].ability == 15 && playersList[_animatedPlayer].abilitiesVector[8].ability == 15)
	{
		if (playersList[_animatedPlayer].isUsingAbilityAnimation == false)
		{
			if (playersList[_animatedPlayer].isJumping == false)
			{
				if (playersList[_animatedPlayer].playerVelocity.x != 0)
				{
					if (playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1))
					{
						if (playersList[_animatedPlayer].currentFrameX < 4)
						{
							playersList[_animatedPlayer].currentFrameY = 2;
							playersList[_animatedPlayer].currentFrameX++;
							playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
						}
						else
						{
							playersList[_animatedPlayer].currentFrameY = 2;
							playersList[_animatedPlayer].currentFrameX = 1;
							playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
						}
					}	
				}
				else
				{
					playersList[_animatedPlayer].currentFrameX = 1;
				}
			}
			else
			{
				playersList[_animatedPlayer].currentFrameX = 1;
				playersList[_animatedPlayer].currentFrameY = 3;
			}
			//Landing state
			if ((((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+10)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+10)/32].y) != 0 ) || 
				((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+20)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+20)/32].y) != 0 )))
			{
				//Leave the landing state
				if (playersList[_animatedPlayer].currentFrameX == 1 && playersList[_animatedPlayer].currentFrameY == 3 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.25)))
				{
					playersList[_animatedPlayer].currentFrameX = 1;
					playersList[_animatedPlayer].currentFrameY = 2;
				}
			}
		}
	}

	//Animate Rogue
	else if (playersList[_animatedPlayer].abilitiesVector[7].ability == 22 && playersList[_animatedPlayer].abilitiesVector[8].ability == 22)
	{
		if (playersList[_animatedPlayer].isUsingAbilityAnimation == false)
		{
			if (playersList[_animatedPlayer].isJumping == false)
			{
				if (playersList[_animatedPlayer].playerVelocity.x != 0)
				{
					if (playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.1))
					{
						if (playersList[_animatedPlayer].currentFrameX < 3)
						{
							playersList[_animatedPlayer].currentFrameY = 2;
							playersList[_animatedPlayer].currentFrameX++;
							playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
						}
						else
						{
							playersList[_animatedPlayer].currentFrameY = 2;
							playersList[_animatedPlayer].currentFrameX = 1;
							playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
						}
					}	
				}
				else
				{
					playersList[_animatedPlayer].currentFrameX = 1;
					playersList[_animatedPlayer].currentFrameY = 1;
				}
			}
			else
			{
				if (playersList[_animatedPlayer].playerVelocity.y < 0)
				{
					playersList[_animatedPlayer].currentFrameX = 1;
					playersList[_animatedPlayer].currentFrameY = 3;
				}
				else
				{
					playersList[_animatedPlayer].currentFrameX = 2;
					playersList[_animatedPlayer].currentFrameY = 3;
				}
			}
			//Landing state
			if ((((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+10)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+10)/32].y) != 0 ) || 
				((map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+20)/32].x) != 0 || (map[(int)(playersList[_animatedPlayer].mSprite.getPosition().y+46)/32][(int)(playersList[_animatedPlayer].mSprite.getPosition().x+20)/32].y) != 0 )))
			{
				playersList[_animatedPlayer].currentFrameX = 3;
				playersList[_animatedPlayer].currentFrameY = 3;
				playersList[_animatedPlayer].timeSinceAnimChange = sf::Time::Zero;
			}

			//Leave the landing state
			if (playersList[_animatedPlayer].currentFrameX == 3 && playersList[_animatedPlayer].currentFrameY == 3 && (float)(playersList[_animatedPlayer].timeSinceAnimChange > sf::seconds((float)0.25)))
			{
				playersList[_animatedPlayer].currentFrameX = 1;
				playersList[_animatedPlayer].currentFrameY = 1;
			}
		}
	}

	//Displaying the chosen frame
	//if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false && playersList[localPlayerNumber].abilitiesVector[8].ability == 1)
	//{
		
		if (playersList[_animatedPlayer].directionFacing == 0)
		{
			playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 32), (playersList[_animatedPlayer].currentFrameY * 48) - 48, -32, 48));
		}
		if (playersList[_animatedPlayer].directionFacing == 1)
		{
			playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 32) - 32, (playersList[_animatedPlayer].currentFrameY * 48) - 48, 32, 48));
		}
	//}
	if (playersList[_animatedPlayer].abilitiesVector[8].isActive == true  && playersList[_animatedPlayer].abilitiesVector[8].ability == 1)
	{
		if (playersList[_animatedPlayer].directionFacing == 0)
		{
			playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 48), (playersList[_animatedPlayer].currentFrameY * 48) - 48, -48, 48));
		}
		if (playersList[_animatedPlayer].directionFacing == 1)
		{
			playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 48) - 48, (playersList[_animatedPlayer].currentFrameY * 48) - 48, 48, 48));
		}
	}
}
void animateOnlinePlayer(int _animatedPlayer)
{
	playersList[_animatedPlayer].timeSinceAnimChange += timePerFrame;

	if (playersList[_animatedPlayer].directionFacing == 0)
	{
		playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 32), (playersList[_animatedPlayer].currentFrameY * 48) - 48, -32, 48));
	}
	if (playersList[_animatedPlayer].directionFacing == 1)
	{
		playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 32) - 32, (playersList[_animatedPlayer].currentFrameY * 48) - 48, 32, 48));
	}
	if (playersList[_animatedPlayer].textureSize == 2  && playersList[_animatedPlayer].abilitiesVector[8].ability == 1)
	{
		if (playersList[_animatedPlayer].directionFacing == 0)
		{
			playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 48), (playersList[_animatedPlayer].currentFrameY * 48) - 48, -48, 48));
		}
		if (playersList[_animatedPlayer].directionFacing == 1)
		{
			playersList[_animatedPlayer].mSprite.setTextureRect(sf::IntRect((playersList[_animatedPlayer].currentFrameX * 48) - 48, (playersList[_animatedPlayer].currentFrameY * 48) - 48, 48, 48));
		}
	}
}
void particleEntity::update()
{
	timeSinceAnimChange += timePerFrame;

	if (particleType == 1)
	{
		if (timeSinceAnimChange > sf::seconds((float)0.15))
		{
			currentFrameX++;
		}
		if (currentFrameX > 5)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 2)
	{
		if (timeSinceAnimChange > sf::seconds((float)0.15))
		{
			currentFrameX++;
		}
		if (currentFrameX > 4)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 3)
	{
		if (timeSinceAnimChange > sf::seconds((float)0.1))
		{
			currentFrameX++;
		}
		if (currentFrameX > 10)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 4)
	{
		if (timeSinceAnimChange > sf::seconds((float)0.05))
		{
			currentFrameX++;
		}
		if (currentFrameX > 2)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 5)
	{
		sprite.setPosition(playersList[spawnerPlayerNumber].mSprite.getPosition());
		if (timeSinceAnimChange > sf::seconds((float)0.05))
		{
			currentFrameX++;
		}
		if (currentFrameX > 6)
		{
			destroyParticle = true;
		}
		for (unsigned int i = 0; i < playersList.size(); i++)
		{
			if (playersList[spawnerPlayerNumber].mSprite.getGlobalBounds().intersects(playersList[i].mSprite.getGlobalBounds()))
			{
				if (playersList[i].teamAffiliation != teamAffiliation)
				{
					for (unsigned int j = 0; j < playersList[i].statusEffectsVector.size(); j++)
					{
						if (playersList[i].statusEffectsVector[j].effectID == 2 && 
							playersList[i].statusEffectsVector[j].durationLeft <= sf::Time::Zero)
						{
							playersList[i].health -= playersList[spawnerPlayerNumber].abilitiesVector[7].damageDealt;
							playersList[i].statusEffectsVector[j].durationLeft = playersList[spawnerPlayerNumber].abilitiesVector[7].cooldownTime;
							//Duration used to be cooldownLeft
							playersList[i].playerVelocity.y -= 12.f;
							break;
						}
					}
				}
			}
		}
	}
	if (particleType == 6)
	{
		if (timeSinceAnimChange < playersList[spawnerPlayerNumber].abilitiesVector[8].durationTotal)
		{			
			if (playersList[spawnerPlayerNumber].directionFacing == 0)
			{
				directionFacing = 0;
				sprite.setPosition(playersList[spawnerPlayerNumber].mSprite.getPosition().x - 5, playersList[spawnerPlayerNumber].mSprite.getPosition().y);
			}
			else
			{
				directionFacing = 1;
				sprite.setPosition(playersList[spawnerPlayerNumber].mSprite.getPosition().x + 5, playersList[spawnerPlayerNumber].mSprite.getPosition().y);
			}
		}
		else
		{
			destroyParticle = true;
		}
	}
	if (particleType == 7)
	{
		sprite.setPosition(playersList[spawnerPlayerNumber].mSprite.getPosition());
		if (timeSinceAnimChange > sf::seconds((float)0.05))
		{
			currentFrameX++;
		}
		if (currentFrameX > 6)
		{
			destroyParticle = true;
		}

		for (unsigned int i = 0; i < playersList.size(); i++)
		{
			if (playersList[spawnerPlayerNumber].mSprite.getGlobalBounds().intersects(playersList[i].mSprite.getGlobalBounds()))
			{
				if (playersList[i].teamAffiliation != teamAffiliation)
				{
					for (unsigned int j = 0; j < playersList[i].statusEffectsVector.size(); j++)
					{
						if (playersList[i].statusEffectsVector[j].effectID == 2 && 
							playersList[i].statusEffectsVector[j].durationLeft <= sf::Time::Zero)
						{
							playersList[i].health -= playersList[spawnerPlayerNumber].abilitiesVector[7].damageDealt;
							playersList[i].statusEffectsVector[j].durationLeft = playersList[spawnerPlayerNumber].abilitiesVector[7].cooldownTime;
							break;
						}
					}
				}
			}
		}
	}
	if (particleType == 8)
	{
		if (directionFacing == 0)
		{
			sprite.move(-2.f, 0.f);
			if ((map[(int)(sprite.getPosition().y+7)/32][(int)(sprite.getPosition().x+7)/32].x) != 0 || 
				(map[(int)(sprite.getPosition().y+7)/32][(int)(sprite.getPosition().x+7)/32].y) != 0)
			{
				destroyParticle = true;
			}
		}
		else
		{
			sprite.move(2.f, 0.f);
			if ((map[(int)(sprite.getPosition().y+7)/32][(int)(sprite.getPosition().x+7)/32].x) != 0 || 
				(map[(int)(sprite.getPosition().y+7)/32][(int)(sprite.getPosition().x+7)/32].y) != 0)
			{
				destroyParticle = true;
			}			
		}

		if (timeSinceAnimChange > sf::seconds(0.75))
		{
			currentFrameX --;
			timeSinceAnimChange = sf::Time::Zero;
			if (currentFrameX < 1)
			{
				destroyParticle = true;
			}
		}
		for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
		{
			if (particleEntitiesList[i].particleType == 6 && 
				particleEntitiesList[i].teamAffiliation != teamAffiliation && 
				particleEntitiesList[i].directionFacing != directionFacing &&
				particleEntitiesList[i].sprite.getGlobalBounds().intersects(sprite.getGlobalBounds()))
			{
				destroyParticle = true;
			}
		}
		for (unsigned int i = 0; i < playersList.size(); i++)
		{
			if (playersList[i].mSprite.getGlobalBounds().contains(sprite.getPosition()) && destroyParticle == false)
			{
				if (playersList[i].teamAffiliation != teamAffiliation)
				{
					playersList[i].health -= playersList[spawnerPlayerNumber].abilitiesVector[7].damageDealt * currentFrameX;
					destroyParticle = true;
				}
			}
		}
	}
	if (particleType == 9)
	{
		if (timeSinceAnimChange > sf::seconds((float)0.025))
		{
			currentFrameX++;
		}
		if (currentFrameX > 3)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 10)
	{
		if (playersList[spawnerPlayerNumber].directionFacing == 0)
		{
			directionFacing = 1;
			if ((((playersList[spawnerPlayerNumber].mSprite.getPosition().x/32) - (playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged.asSeconds() * 2))*32) < ((mapSizeX-1) * 32))
			{
				sprite.setPosition(((playersList[spawnerPlayerNumber].mSprite.getPosition().x/32) - (playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, ((playersList[spawnerPlayerNumber].mSprite.getPosition().y/32)*32));
			}
			else
			{
				sprite.setPosition((float)((mapSizeX-1) * 32), ((playersList[spawnerPlayerNumber].mSprite.getPosition().y/32)*32));
			}
		}
		else
		{
			directionFacing = 0;
			if (((playersList[spawnerPlayerNumber].mSprite.getPosition().x/32) + (playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged.asSeconds() * 2))*32 > 100)
			{
				sprite.setPosition(((playersList[spawnerPlayerNumber].mSprite.getPosition().x/32) + (playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, ((playersList[spawnerPlayerNumber].mSprite.getPosition().y/32)*32));
			}
			else
			{
				sprite.setPosition(64, ((playersList[spawnerPlayerNumber].mSprite.getPosition().y/32)*32));
			}
		}

		for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
		{
			if (particleEntitiesList[i].particleType == 11 && particleEntitiesList[i].spawnerPlayerNumber == spawnerPlayerNumber)
			{
				destroyParticle = true;
			}
		}
		/*if (playersList[spawnerPlayerNumber].abilitiesVector[8].isEndingAbility == true)
		{
			destroyParticle = true;
		}*/
	}
	if (particleType == 11)
	{
		if (timeSinceAnimChange > sf::seconds((float)0.035))
		{
			currentFrameX++;
		}
		if (currentFrameX > 2)
		{
			currentFrameX = 1;
		}

		if (timeSinceAnimChange > playersList[spawnerPlayerNumber].abilitiesVector[8].durationTotal)
		{
			playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged = sf::Time::Zero;
			destroyParticle = true;
		}

		if ((sprite.getPosition().x / 32) < 1 ||
			(sprite.getPosition().x / 32) > mapSizeX -1 ||
			(sprite.getPosition().y / 32) < 1 ||
			(sprite.getPosition().y / 32) > mapSizeY - 1)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 12)
	{
		for (unsigned int i = 0; i < playersList.size(); i++)
		{
			if (sprite.getGlobalBounds().intersects(playersList[i].mSprite.getGlobalBounds()))
			{
				if (playersList[i].teamAffiliation != teamAffiliation)
				{
					for (unsigned int j = 0; j < playersList[i].statusEffectsVector.size(); j++)
					{
						if (playersList[i].statusEffectsVector[j].effectID == 3 && 
							playersList[i].statusEffectsVector[j].durationLeft <= sf::Time::Zero)
						{
							playersList[i].health -= playersList[spawnerPlayerNumber].abilitiesVector[8].damageDealt * playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged.asSeconds();

							playersList[i].statusEffectsVector[j].durationLeft = playersList[spawnerPlayerNumber].abilitiesVector[8].cooldownTime - sf::seconds(1.f);

							break;
						}
					}
				}
			}
		}

		if (timeSinceAnimChange > sf::seconds((float)0.035))
		{
			currentFrameX++;
		}
		if (currentFrameX > 2)
		{
			currentFrameX = 1;
		}

		if (timeSinceAnimChange > playersList[spawnerPlayerNumber].abilitiesVector[8].durationTotal)
		{
			playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged = sf::Time::Zero;
			if (spawnerPlayerNumber == localPlayerNumber)
			{
				if (isGameLocal == false)
				{
					updateAbilitiesToServer(0, spawnerPlayerNumber, 8, 15, playersList[spawnerPlayerNumber].abilitiesVector[8].cooldownTime, playersList[spawnerPlayerNumber].abilitiesVector[8].damageDealt, playersList[spawnerPlayerNumber].abilitiesVector[8].timeCharged);
				}
			}
			destroyParticle = true;
		}
	}
	if (particleType == 13)
	{
		sprite.setPosition(playersList[spawnerPlayerNumber].mSprite.getPosition());
		if (timeSinceAnimChange > sf::seconds((float)0.075))
		{
			currentFrameX++;
		}
		if (currentFrameX > 3)
		{
			destroyParticle = true;
		}
		for (unsigned int i = 0; i < playersList.size(); i++)
		{
			if (playersList[spawnerPlayerNumber].mSprite.getGlobalBounds().intersects(playersList[i].mSprite.getGlobalBounds()))
			{
				if (playersList[i].teamAffiliation != teamAffiliation)
				{
					for (unsigned int j = 0; j < playersList[i].statusEffectsVector.size(); j++)
					{
						if (playersList[i].statusEffectsVector[j].effectID == 4 && 
							playersList[i].statusEffectsVector[j].durationLeft <= sf::Time::Zero)
						{

							if (playersList[i].directionFacing != directionFacing)
							{
								playersList[i].health -= playersList[spawnerPlayerNumber].abilitiesVector[7].damageDealt;
							}
							else
							{
								playersList[i].health -= (float)(playersList[spawnerPlayerNumber].abilitiesVector[7].damageDealt * 1.5);
							}
							playersList[i].statusEffectsVector[j].durationLeft = playersList[spawnerPlayerNumber].abilitiesVector[7].cooldownTime;
							//Duration used to be cooldownLeft
							break;
						}
					}
				}
			}
		}
	}
	if (particleType == 14)
	{
		sprite.setPosition(playersList[spawnerPlayerNumber].mSprite.getPosition());
		if (playersList[spawnerPlayerNumber].directionFacing == 0)
		{
			directionFacing = 0;
		}
		else
		{
			directionFacing = 1;
		}

		if (timeSinceAnimChange >= playersList[spawnerPlayerNumber].abilitiesVector[8].durationTotal)
		{
			destroyParticle = true;
		}
	}
	if (particleType == 15)
	{
		playersList[spawnerPlayerNumber].mSprite.setPosition(sprite.getPosition().x, sprite.getPosition().y - 10);
		playersList[spawnerPlayerNumber].mSprite.setTexture(deadPlayerTexture);
		playersList[spawnerPlayerNumber].deathTimer += timePerFrame;
		playersList[spawnerPlayerNumber].health = 1.f;

		if (timeSinceAnimChange > playersList[spawnerPlayerNumber].deathDuration)
		{
			playersList[spawnerPlayerNumber].health = playersList[spawnerPlayerNumber].healthMax;
			if (playersList[spawnerPlayerNumber].teamAffiliation == 1)
			{
				playersList[spawnerPlayerNumber].mSprite.setPosition(redTeamSpawnX, redTeamSpawnY);
			}
			else
			{
				playersList[spawnerPlayerNumber].mSprite.setPosition(blueTeamSpawnX, blueTeamSpawnY);
			}
			destroyParticle = true;

			if (spawnerPlayerNumber == localPlayerNumber)
			{
				view1.setCenter(playersList[localPlayerNumber].mSprite.getPosition().x + 30, playersList[localPlayerNumber].mSprite.getPosition().y - 166);
				backgroundSprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 604);
			}

			playersList[spawnerPlayerNumber].deathTimer = sf::Time::Zero;

			if (playersList[spawnerPlayerNumber].characterTexture == 1)
			{
				playersList[spawnerPlayerNumber].mSprite.setTexture(playerTexture);
			}
			else if (playersList[spawnerPlayerNumber].characterTexture == 2)
			{
				playersList[spawnerPlayerNumber].mSprite.setTexture(warriorGuyTexture);
			}
			else if (playersList[spawnerPlayerNumber].characterTexture == 3)
			{
				playersList[spawnerPlayerNumber].mSprite.setTexture(wizardTexture);
			}
			else if (playersList[spawnerPlayerNumber].characterTexture == 4)
			{
				playersList[spawnerPlayerNumber].mSprite.setTexture(rogueTexture);
			}
		}
	}

	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		if (particleEntitiesList[i].destroyParticle == true)
		{
			particleEntitiesList.erase(particleEntitiesList.begin() + i);

			if (isGameLocal == false)
			{
				//Update destroyed particle to server
				//updateToServer(4, bulletsList[i]);
			}

			i--;
		}
	}

	if (directionFacing == 0)
	{
		if (particleType == 1 || particleType == 2 || particleType == 3 || particleType == 4 || 
			particleType == 8 || particleType == 9 || particleType == 10 || particleType == 11 ||
			particleType == 12)
		{
			sprite.setTextureRect(sf::IntRect((currentFrameX * 32), (currentFrameY * 32) - 32, -32, 32));
		}
		else if (particleType == 5 || particleType == 6 || particleType == 7 || particleType == 13 || particleType == 14)
		{
			sprite.setTextureRect(sf::IntRect((currentFrameX * 32), (currentFrameY * 48) - 48, -32, 48));
		}
		else if (particleType == 15)
		{
			sprite.setTextureRect(sf::IntRect((currentFrameX * 48), (currentFrameY * 32) - 32, -48, 32));
		}
	}
	if (directionFacing == 1)
	{
		if (particleType == 1 || particleType == 2 || particleType == 3 || particleType == 4 || 
			particleType == 8 || particleType == 9 || particleType == 10 || particleType == 11 ||
			particleType == 12)
		{
			sprite.setTextureRect(sf::IntRect((currentFrameX * 32) - 32, (currentFrameY * 32) - 32, 32, 32));
		}
		else if (particleType == 5 || particleType == 6 || particleType == 7 || particleType == 13 || particleType == 14)
		{
			sprite.setTextureRect(sf::IntRect((currentFrameX * 32) - 32, (currentFrameY * 48) - 48, 32, 48));
		}
		else if (particleType == 15)
		{
			sprite.setTextureRect(sf::IntRect((currentFrameX * 48) - 48, (currentFrameY * 32) - 32, 48, 32));
		}
	}
}
void spawnParticlesFromServer(unsigned int actor, unsigned int _particleType, int _posY)
{
	if (_particleType == 1)
	{
		spawnJumpDustParticles(actor);
	}
	else if (_particleType == 2)
	{
		spawnWalkDustParticles(actor);
	}
	else if (_particleType == 3)
	{
		spawnHawkTransformParticles(actor);
	}
	else if (_particleType == 4)
	{
		spawnArrowChargeTimeParticles(actor);
	}
	else if (_particleType == 5)
	{
		spawnSwordSlashUpParticles(actor);
	}
	else if (_particleType == 6)
	{
		spawnShieldBlockParticles(actor);
	}
	else if (_particleType == 7)
	{
		spawnSwordSlashDownParticles(actor);
	}
	else if (_particleType == 8)
	{
		spawnFireballParticles(actor);
	}
	else if (_particleType == 9)
	{
		spawnThunderclapChargeParticles(actor);
	}
	else if (_particleType == 10)
	{
		spawnThunderclapMarkerParticles(actor);
	}
	else if (_particleType == 11)
	{
		spawnThunderclapCloudParticles(actor);
	}
	else if (_particleType == 12)
	{
		spawnThunderclapParticles(_posY, actor);
	}
	else if (_particleType == 13)
	{
		spawnRogueSlashParticles(actor);
	}
	else if (_particleType == 14)
	{
		spawnRogueDashParticles(actor);
	}
	else if (_particleType == 15)
	{
		spawnCorpseParticles(actor);
	}
}

//32 by 32
void spawnJumpDustParticles(unsigned int actor)
{
	//Create the dust particle
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(smokeLandingParticlesTexture);
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y + 20);
	newParticleEntity.directionFacing = 0;
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 1;
	newParticleEntity.drawLayer = 1;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnWalkDustParticles(unsigned int actor)
{
	//Create the dust particle
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(smokeWalkingParticlesTexture);
	newParticleEntity.sprite.setColor(sf::Color::Color(204,204,180,200));

	unsigned int randomOffset = rand() % 10 + 5;

	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.sprite.setOrigin(16.f, 16.f);
		newParticleEntity.sprite.setPosition((playersList[actor].mSprite.getPosition().x - 10 + randomOffset), (playersList[actor].mSprite.getPosition().y + 38 + randomOffset));
		newParticleEntity.sprite.setScale((float)(0.1 * randomOffset), (float)(0.1 * randomOffset));
	}
	else if (playersList[actor].directionFacing == 1)
	{
		newParticleEntity.sprite.setOrigin(16.f, 16.f);
		newParticleEntity.sprite.setPosition((playersList[actor].mSprite.getPosition().x + 20 + randomOffset), (playersList[actor].mSprite.getPosition().y + 38 + randomOffset));
		newParticleEntity.sprite.setScale((float)(0.1 * randomOffset), (float)(0.1 * randomOffset));
	}

	newParticleEntity.directionFacing = 0;
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 2;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnHawkTransformParticles(unsigned int actor)
{
	//Create the dust particle
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(smokeTransformParticlesTexture);
	newParticleEntity.sprite.setPosition((float)(playersList[actor].mSprite.getPosition().x - 10), playersList[actor].mSprite.getPosition().y);
	newParticleEntity.sprite.setScale(2.f, 2.f);

	newParticleEntity.directionFacing = 0;
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 3;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnArrowChargeTimeParticles(unsigned int actor)
{
	//Create the dust particle
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(smokeWalkingParticlesTexture);
	newParticleEntity.sprite.setColor(sf::Color::Color((sf::Uint8)(125 * playersList[actor].abilitiesVector[7].timeCharged.asSeconds()),0,0,175));

	unsigned int randomOffset = rand() % 10 + 5;

	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.sprite.setOrigin(16.f, 16.f);
		newParticleEntity.sprite.setPosition((playersList[actor].mSprite.getPosition().x - 15 + randomOffset), (playersList[actor].mSprite.getPosition().y + 10 + randomOffset));
		newParticleEntity.sprite.setScale((float)(0.1 * randomOffset), (float)(0.1 * randomOffset));
		newParticleEntity.directionFacing = 0;
	}
	else if (playersList[actor].directionFacing == 1)
	{
		newParticleEntity.sprite.setOrigin(32.f, 16.f);
		newParticleEntity.sprite.setPosition((playersList[actor].mSprite.getPosition().x + 36 + randomOffset), (playersList[actor].mSprite.getPosition().y + 14 + randomOffset));
		newParticleEntity.sprite.setScale((float)(0.1 * randomOffset), (float)(0.1 * randomOffset));
		newParticleEntity.directionFacing = 1;
	}

	
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 4;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}


//32 by 48
void spawnSwordSlashUpParticles(unsigned int actor)
{
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(swordSlashUpParticlesTexture);
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
	}
	else
	{
		newParticleEntity.directionFacing = 1;
	}
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 5;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnShieldBlockParticles(unsigned int actor)
{
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(shieldBlockParticlesTexture);
	newParticleEntity.sprite.setColor(sf::Color::Color(255, 255, 255, 100));
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y);
	newParticleEntity.spawnerPlayerNumber = actor;
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;

	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
	}
	else
	{
		newParticleEntity.directionFacing = 1;
	}
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 6;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnSwordSlashDownParticles(unsigned int actor)
{
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(swordSlashDownParticlesTexture);
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
	}
	else
	{
		newParticleEntity.directionFacing = 1;
	}
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 7;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;

	particleEntitiesList.push_back(newParticleEntity);
}

//32 by 32
void spawnFireballParticles(unsigned int actor)
{
	//Create the dust particle
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(fireballParticlesTexture);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
		newParticleEntity.sprite.setOrigin(0.f, 16.f);
		newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x - 8, playersList[actor].mSprite.getPosition().y + 26);
	}
	else
	{
		newParticleEntity.sprite.setOrigin(32.f, 16.f);
		newParticleEntity.directionFacing = 1;
		newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x + 40, playersList[actor].mSprite.getPosition().y + 26);
	}
	
	newParticleEntity.currentFrameX = 4;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 8;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;

	newParticleEntity.spawnerPlayerNumber = actor;
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnThunderclapChargeParticles(unsigned int actor)
{
	//Sparks particle
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(thunderclapChargeParticlesTexture);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
		newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x - 5, playersList[actor].mSprite.getPosition().y-8);
	}
	else
	{
		newParticleEntity.directionFacing = 1;
		newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x + 5, playersList[actor].mSprite.getPosition().y-8);
	}

	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 9;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnThunderclapMarkerParticles(unsigned int actor)
{	
	//Marker particles
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(thunderclapMarkerParticlesTexture);
	newParticleEntity.sprite.setColor(sf::Color::Color(255,255,255, 150));
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
		if ((((playersList[actor].mSprite.getPosition().x/32) - (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32) < ((mapSizeX-1) * 32))
		{
			newParticleEntity.sprite.setPosition(((playersList[actor].mSprite.getPosition().x/32) - (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
		else
		{
			newParticleEntity.sprite.setPosition((float)((mapSizeX-1) * 32), ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
	}
	else
	{
		newParticleEntity.directionFacing = 1;
		if (((playersList[actor].mSprite.getPosition().x/32) + (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32 > 32)
		{
			newParticleEntity.sprite.setPosition(((playersList[actor].mSprite.getPosition().x/32) + (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
		else
		{
			newParticleEntity.sprite.setPosition(64, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
	}

	newParticleEntity.currentFrameX = 3;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 10;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnThunderclapCloudParticles(unsigned int actor)
{
	//Thunderclap particles
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(thunderclapMarkerParticlesTexture);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
		if ((((playersList[actor].mSprite.getPosition().x/32) - (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32) < ((mapSizeX-1) * 32))
		{
			newParticleEntity.sprite.setPosition(((playersList[actor].mSprite.getPosition().x/32) - (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
		else
		{
			newParticleEntity.sprite.setPosition((float)((mapSizeX-1) * 32), ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
	}
	else
	{
		newParticleEntity.directionFacing = 1;
		if (((playersList[actor].mSprite.getPosition().x/32) + (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32 > 32)
		{
			newParticleEntity.sprite.setPosition(((playersList[actor].mSprite.getPosition().x/32) + (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
		else
		{
			newParticleEntity.sprite.setPosition(64, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
	}

	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 11;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnThunderclapParticles(unsigned int _posY, unsigned int actor)
{
	//Thunderclap particles
	particleEntity newParticleEntity;
	newParticleEntity.destroyParticle = false;

	newParticleEntity.sprite.setTexture(thunderclapMarkerParticlesTexture);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
		if ((((playersList[actor].mSprite.getPosition().x/32) - (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32) < ((mapSizeX-1) * 32))
		{
			newParticleEntity.sprite.setPosition(((playersList[actor].mSprite.getPosition().x/32) - (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, (((playersList[actor].mSprite.getPosition().y/32)+ _posY)*32));
		}
		else
		{
			newParticleEntity.sprite.setPosition((float)((mapSizeX-1) * 32), ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
	}
	else
	{
		newParticleEntity.directionFacing = 1;
		if (((playersList[actor].mSprite.getPosition().x/32) + (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32 > 32)
		{
			newParticleEntity.sprite.setPosition(((playersList[actor].mSprite.getPosition().x/32) + (playersList[actor].abilitiesVector[8].timeCharged.asSeconds() * 2))*32, (((playersList[actor].mSprite.getPosition().y/32)+ _posY)*32));
		}
		else
		{
			newParticleEntity.sprite.setPosition(64, ((playersList[actor].mSprite.getPosition().y/32)*32));
		}
	}

	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 2;
	newParticleEntity.particleType = 12;
	newParticleEntity.drawLayer = 5;
	
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;
	newParticleEntity.spawnerPlayerNumber = actor;

	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;

	particleEntitiesList.push_back(newParticleEntity);
}

//32 by 48
void spawnRogueSlashParticles(unsigned int actor)
{
	particleEntity newParticleEntity;

	newParticleEntity.spawnerPlayerNumber = actor;
	newParticleEntity.sprite.setTexture(rogueSlashParticlesTexture);
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
	}
	else
	{
		newParticleEntity.directionFacing = 1;
	}
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 13;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;

	particleEntitiesList.push_back(newParticleEntity);
}
void spawnRogueDashParticles(unsigned int actor)
{
	particleEntity newParticleEntity;

	newParticleEntity.spawnerPlayerNumber = actor;
	newParticleEntity.sprite.setTexture(rogueDashParticlesTexture);
	newParticleEntity.sprite.setColor(sf::Color::Color(255,255,255,100));
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y);
	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
	}
	else
	{
		newParticleEntity.directionFacing = 1;
	}
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 14;
	newParticleEntity.drawLayer = 5;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.teamAffiliation = playersList[actor].teamAffiliation;

	particleEntitiesList.push_back(newParticleEntity);
}

void spawnCorpseParticles(unsigned int actor)
{
	particleEntity newParticleEntity;

	newParticleEntity.sprite.setTexture(graveParticlesTexture);
	newParticleEntity.sprite.setPosition(playersList[actor].mSprite.getPosition().x, playersList[actor].mSprite.getPosition().y + 20);
	newParticleEntity.currentFrameX = 1;
	newParticleEntity.currentFrameY = 1;
	newParticleEntity.particleType = 15;
	newParticleEntity.drawLayer = 1;
	newParticleEntity.destroyParticle = false;
	newParticleEntity.timeSinceAnimChange = sf::Time::Zero;
	newParticleEntity.spawnerPlayerNumber = actor;

	/*if (playersList[actor].mSprite.getTexture() == playerTexture)
	{

	}*/
	/*if (isGameLocal == false && actor == localPlayerNumber)
	{
		if (playersList[actor].teamAffiliation == 1)
		{
			updateScoreToServer(0, 1, 2);
		}
		if (playersList[actor].teamAffiliation == 2)
		{
			updateScoreToServer(0, 1, 1);
		}
	}*/
	


	if (playersList[actor].directionFacing == 0)
	{
		newParticleEntity.directionFacing = 0;
	}
	else
	{
		newParticleEntity.directionFacing = 1;
	}

	particleEntitiesList.push_back(newParticleEntity);
}