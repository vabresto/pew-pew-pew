#include "stdafx.h"
#include "receiveUpdateFromServer.h"

#include "globalVariables.h"
#include "HUDManager.h"
#include "animationManager.h"
#include <iostream>

#include "Characters/archer.h"
#include "Characters/rogue.h"
#include "Characters/shieldGuy.h"
#include "Characters/wizard.h"

void receiveUpdateFromServer()
{
	/**********************************/
	//Handling server updates
	/********************************/
	while (UDPSocket.receive(inPacket, senderIPAddress, UDPRecievePort) == sf::Socket::Done)
	{
		if (inPacket >> packetHeader)
		{
			if (packetHeader == 0)
			{
				if (inPacket >> packetSubHeader)
				{
					if (packetSubHeader == 4)
					{
						int _redKills, _blueKills;
						float _serverTime;
						if (inPacket >> _serverTime >> _redKills >> _blueKills)
						{
							timeSpentInCurrentLevel = sf::seconds(_serverTime);
							redTeamKills = _redKills;
							blueTeamKills = _blueKills;
						}
					}
				}
			}
			else if (packetHeader == 1)
			{
				if (inPacket >> packetSubHeader)
				{
					if (packetSubHeader == 1)
					{
						inPacket >> textMessage >> teamWhichSentMessage;
						messagesList.push_back(messagingStruct());
						messagesList[numOfMessages].message = textMessage;
						messagesList[numOfMessages].team = teamWhichSentMessage;
						numOfMessages++;

						std::cout << "Team " << teamWhichSentMessage << "'s " << textMessage << std::endl;

						textMessage.clear();
					}
					inPacket.clear();
				}
			}
			else if (packetHeader == 2)
			{
				if (inPacket >> packetSubHeader)
				{
					if (packetSubHeader == 0)
					{
						int _playerNumber, _team, _characterType;
						std::string _username;
						if (inPacket >> _playerNumber >> _username >> _team >> _characterType)
						{
							playersList[_playerNumber].teamAffiliation = _team;
							playersList[_playerNumber].playerName = _username;

							if (_team == 1)
							{
								playersList[_playerNumber].mSprite.setPosition(redTeamSpawnX, redTeamSpawnY);
								if (_playerNumber == localPlayerNumber)
								{
									view1.setCenter(playersList[_playerNumber].mSprite.getPosition().x + 30, playersList[_playerNumber].mSprite.getPosition().y - 166);
									backgroundSprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 304);
								}
							}
							else if (_team == 2)
							{
								playersList[_playerNumber].mSprite.setPosition(blueTeamSpawnX, blueTeamSpawnY);
								if (_playerNumber == localPlayerNumber)
								{
									view1.setCenter(playersList[_playerNumber].mSprite.getPosition().x + 30, playersList[_playerNumber].mSprite.getPosition().y - 166);
									backgroundSprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 304);
								}
							}

							playersList[_playerNumber].currentFrameX = 1;
							playersList[_playerNumber].currentFrameY = 1;

							if (_characterType == 1)
							{
								playersList[_playerNumber].healthMax = 35.f;
								playersList[_playerNumber].health = playersList[_playerNumber].healthMax;

								playersList[_playerNumber].characterTexture = 1;
								playersList[_playerNumber].mSprite.setTexture(playerTexture);

								givePlayerShootArrowAbility(_playerNumber);
								givePlayerTransformHawkAbility(_playerNumber);
							}
							else if (_characterType == 2)
							{
								playersList[_playerNumber].healthMax = 35.f;
								playersList[_playerNumber].health = playersList[_playerNumber].healthMax;

								playersList[_playerNumber].characterTexture = 4;
								playersList[_playerNumber].mSprite.setTexture(rogueTexture);

								givePlayerRogueSlashAbility(_playerNumber);
								givePlayerRogueDashAbility(_playerNumber);
							}
							else if (_characterType == 3)
							{
								playersList[_playerNumber].healthMax = 60.f;
								playersList[_playerNumber].health = playersList[_playerNumber].healthMax;

								playersList[_playerNumber].characterTexture = 2;
								playersList[_playerNumber].mSprite.setTexture(warriorGuyTexture);

								givePlayerSwordSlashAbility(_playerNumber);
								givePlayerShieldBlockAbility(_playerNumber);
							}
							else if (_characterType == 4)
							{
								playersList[_playerNumber].healthMax = 40.f;
								playersList[_playerNumber].health = playersList[_playerNumber].healthMax;

								playersList[_playerNumber].characterTexture = 3;
								playersList[_playerNumber].mSprite.setTexture(wizardTexture);

								givePlayerShootFireballAbility(_playerNumber);
								givePlayerThunderclapAbility(_playerNumber);
							}
						}
						initializeGameHUD();
					}
					else if (packetSubHeader == 1)
					{
						int _playerNumber, _frameX, _frameY, _directionFacing;
						float _posX, _posY, _health;

						if (inPacket >> _playerNumber >> _posX >> _posY >> _directionFacing >> _frameX >> _frameY >> _health)
						{
							if (_playerNumber == localPlayerNumber)
							{
								//std::cout << "Updated received for local player.\n";
								//view1.setCenter(playersList[localPlayerNumber].mSprite.getPosition().x + 30, playersList[localPlayerNumber].mSprite.getPosition().y - 166);
								//backgroundSprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 604);
							}

							playersList[_playerNumber].mSprite.setPosition(_posX, _posY);
							playersList[_playerNumber].currentFrameX = _frameX;
							playersList[_playerNumber].currentFrameY = _frameY;
							playersList[_playerNumber].directionFacing = _directionFacing;
							playersList[_playerNumber].health = _health;
						}
					}
					else if (packetSubHeader == 2)
					{

					}
					else if (packetSubHeader == 3)
					{
						unsigned int _playerNumber;
						int _characterType;
						unsigned int _textureNumber, _textureSubNumber;

						if (inPacket >> _playerNumber >> _characterType >> _textureNumber >> _textureSubNumber)
						{
							if (_characterType == 1)
							{
								if (_textureNumber == 1)
								{
									playersList[_playerNumber].mSprite.setTexture(playerTexture);
									playersList[_playerNumber].textureSize = _textureNumber;
								}
								else if (_textureNumber == 2)
								{
									playersList[_playerNumber].mSprite.setTexture(hawkTexture);
									playersList[_playerNumber].textureSize = _textureNumber;
								}
							}
							else if (_characterType == 2)
							{
								playersList[_playerNumber].mSprite.setTexture(rogueTexture);
								playersList[_playerNumber].textureSize = _textureNumber;
							}
							else if (_characterType == 3)
							{
								playersList[_playerNumber].mSprite.setTexture(warriorGuyTexture);
								playersList[_playerNumber].textureSize = _textureNumber;
							}
							else if (_characterType == 4)
							{
								playersList[_playerNumber].mSprite.setTexture(wizardTexture);
								playersList[_playerNumber].textureSize = _textureNumber;
							}
						}
					}
					inPacket.clear();
				}
			}
			else if (packetHeader == 3)
			{
				if (inPacket >> packetSubHeader)
				{
					if (packetSubHeader == 0)
					{
						int _changeInScore, _team;

						if (inPacket >> _changeInScore >> _team)
						{
							if (_team == 1)
							{
								redTeamKills += _changeInScore;
							}
							else if (_team == 2)
							{
								blueTeamKills += _changeInScore;
							}
							else
							{
								std::cout << "Received score update for team: " << _team << std::endl;
							}
						}

					}
					inPacket.clear();
				}
			}
			else if (packetHeader == 4)
			{
				if (inPacket >> packetSubHeader)
				{
					if (packetSubHeader == 1)
					{
						sf::Vector2f _bulletStartPos, _bulletVel;
						int _team;
						float _damageDealt;
						inPacket >> _bulletStartPos.x >> _bulletStartPos.y >> _bulletVel.x >> _bulletVel.y >> _team >> _damageDealt;

						bulletsList.push_back(myPlayerNamespace::projectile (bulletTexture, _bulletStartPos, _bulletVel, bulletCount, _team, _damageDealt));
						bulletCount++;
					}
					else if (packetSubHeader == 2)
					{
						unsigned int playerNumber;
						int particleType, posY;

						if (inPacket >> playerNumber >> particleType >> posY)
						{
							spawnParticlesFromServer(playerNumber, particleType, posY);
						}
					}
				}
				inPacket.clear();
			}
			else if (packetHeader == 5)
			{
				if (inPacket >> packetSubHeader)
				{
					if (packetSubHeader == 0)
					{
						unsigned int _playerNumber;
						int _ability, _abilityNumber;
						sf::Int32 _cdTime, _timeCharged;
						float _damageDealt;
						
						if (inPacket >> _playerNumber >> _ability >> _abilityNumber >> _cdTime >> _damageDealt >> _timeCharged)
						{
							playersList[_playerNumber].abilitiesVector[_ability].ability = _abilityNumber;
							playersList[_playerNumber].abilitiesVector[_ability].damageDealt = _damageDealt;
							playersList[_playerNumber].abilitiesVector[_ability].timeCharged = sf::milliseconds(_timeCharged);
							playersList[_playerNumber].abilitiesVector[_ability].cooldownTime = sf::milliseconds(_cdTime);
						}
					}
					if (packetSubHeader == 2)
					{
						unsigned int _playerNumber;
						int _ability, _abilityNumber;
						bool _isStarting, _isDoing, _isEnding, _isActive;
						
						if (inPacket >> _playerNumber >> _ability >> _abilityNumber >> _isStarting >> _isDoing >> _isEnding >> _isActive)
						{
							playersList[_playerNumber].abilitiesVector[_ability].ability = _abilityNumber;
							playersList[_playerNumber].abilitiesVector[_ability].isStartingAbility = _isStarting;
							playersList[_playerNumber].abilitiesVector[_ability].isDoingAbility = _isDoing;
							playersList[_playerNumber].abilitiesVector[_ability].isEndingAbility = _isEnding;
							playersList[_playerNumber].abilitiesVector[_ability].isActive = _isActive;
						}
					}
				}
			}
			else if (packetHeader == 6)
			{

			}
		}
	}
}