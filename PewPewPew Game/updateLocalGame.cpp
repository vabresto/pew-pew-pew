#include "stdafx.h"
#include "updateLocalGame.h"

#include "player.h"
#include "globalVariables.h"
#include "enemies.h"
#include "animationManager.h"
#include "abilitiesManager.h"
#include "updateToServer.h"


void updateLocalGame()
{
	//Ability cooldowns
	for (unsigned int j = 0; j < playersList.size(); j++)
	{
		for (unsigned int i = 0; i < 11; i++)
		{
			if (playersList[localPlayerNumber].abilitiesVector[i].cooldownLeft > sf::Time::Zero)
			{
				playersList[localPlayerNumber].abilitiesVector[i].cooldownLeft -= timePerFrame;
			}
		}
	}

	//Update status effects
	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		for (unsigned int j = 0; j < playersList[i].statusEffectsVector.size(); j++)
		{
			if (playersList[i].statusEffectsVector[j].effectID == 1)
			{

			}
			else if (playersList[i].statusEffectsVector[j].effectID != 1 &&
				playersList[i].statusEffectsVector[j].durationLeft > sf::Time::Zero)
			{
				playersList[i].statusEffectsVector[j].durationLeft -= timePerFrame;
			}
			/*else
			{
				playersList[i].statusEffectsVector.erase(playersList[i].statusEffectsVector.begin() + j);
			}*/
		}
	}

	
	
	//Slow debuff
	/*if (playersList[localPlayerNumber].slowDebuffTimeLeft > 0)
	{
		playersList[localPlayerNumber].playerMaxVelocity.x = playersList[localPlayerNumber].slowDebuffPlayerSpeed;
		playersList[localPlayerNumber].playerMaxVelocity.y = playersList[localPlayerNumber].slowDebuffPlayerJumpSpeed;
	}
	else
	{
		playersList[localPlayerNumber].playerMaxVelocity.x = playersList[localPlayerNumber].playerMaxSpeed;
		playersList[localPlayerNumber].playerMaxVelocity.y = playersList[localPlayerNumber].playerMaxFallSpeed;
	}*/

	//Kinda choppy movement results from this ... but meh
	if (((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))) && (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)))
	{
		playersList[localPlayerNumber].playerVelocity.x = 0.f;
	}

	//Update player
	//Only update local players - all other updates are received from server
	//Try activating all abilities (update them)
	doAllAbilities(localPlayerNumber);
	playersList[localPlayerNumber].updatePlayer();
	animatePlayer(localPlayerNumber);

	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		if (playersList[i].isNPC == true)
		{
			updatedNPC = i;
			playersList[i].updatePlayer();
			animatePlayer(updatedNPC);
			doAllAbilities(updatedNPC);
		}
		if (isGameLocal == false)
		{
			if (i != localPlayerNumber)
			{
				animateOnlinePlayer(i);
				doAllAbilities(i);
			}
		}
	}

	if (isGameLocal == false && playersList[localPlayerNumber].health > 0)
	{
		updateCharacterToServer(1, localPlayerNumber, playersList[localPlayerNumber].mSprite.getPosition().x, playersList[localPlayerNumber].mSprite.getPosition().y, playersList[localPlayerNumber].directionFacing, playersList[localPlayerNumber].currentFrameX, playersList[localPlayerNumber].currentFrameY, playersList[localPlayerNumber].health);
	}
	updatedNPC = 0;

	//Update slow time left for all players
	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		if (playersList[i].slowDebuffTimeLeft <= 0)
		{
			playersList[i].slowDebuffTimeLeft = 0;
		}
		else
		{
			playersList[i].slowDebuffTimeLeft -= timePerFrame.asSeconds();
		}
	}

	//Update Mountain Gem
	if (loadGem == true)
	{
		gemList[0].update();
	}

	//Update bullets (Arrows)
	for (unsigned int i = 0; i < bulletsList.size(); i++)
	{
		bulletsList[i].update();
	}

	//Update particles
	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		particleEntitiesList[i].update();
	}
}