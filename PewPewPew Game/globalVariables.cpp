#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <cctype>
#include <string>
#include <vector>
#include <sstream>
#include "globalVariables.h"
#include "player.h"
#include "serverCode.h"
#include "tallMountainGem.h"
#include "enemies.h"

sf::Time elapsedTime;
//sf::Time timeSinceAnimChange;

sf::IpAddress localLANIPAddress = sf::IpAddress::getLocalAddress();
sf::IpAddress localIPAddress = sf::IpAddress::getPublicAddress();
sf::IpAddress recipientIPAddress;
sf::IpAddress senderIPAddress;

sf::SocketSelector socketSelector;
sf::UdpSocket UDPSocket;
sf::TcpSocket TCPSocket;
sf::TcpListener TCPListener;

sf::Packet outPacket, inPacket;
int packetHeader;
int packetSubHeader;

std::string testData;

unsigned short UDPSendPort, TCPSendPort, UDPRecievePort, TCPRecievePort;

bool gameHasFocus = true;
bool isNewPlayerNPC = true;

sf::RectangleShape outline(sf::Vector2f(20.f, 42.f));

sf::Time timeSinceLastWalkParticle = sf::Time::Zero;

/****************************************************/
//Timekeeping Stuff
/***************************************************/
const sf::Time timePerFrame = sf::seconds(1.f/60.f);
sf::Clock mainGameClock;
sf::Time timeSinceLastUpdate;
sf::Clock slowDebuffClock;

int main();
void openMap(std::string fileLocation);
void playLevelOne();
void scroll(int x, int y);


sf::RenderWindow gameWindow;
int currentFrameX, currentFrameY;

bool isGameOngoing = false;

bool displayFPS = false;
bool closeWindow = false;

bool scrollRight, scrollLeft, scrollUp, scrollDown;
bool bulletsfired;
bool leaveDestroyedArrows = true;
std::string line;

int updatedNPC = 1;

std::vector < std::vector < sf::Vector2i > > map;
//Variable tempMap is used to store the 'line' of the map (x,y) (0,0),(0,1),(0,2) etc
//Used to make life simpler
std::vector < sf::Vector2i > tempMap;

std::string newFileLocation, newTilesetLocation;
int mapSizeX, mapSizeY, mapTileSize, tempX, tempY;
//Generate variables for holding the contents of the map
sf::Texture tileTexture;
sf::Sprite tiles;

char newFileOrExit;

//Flag variable to see how I want to input data
/********************************************/
//True will refer to the format of 10x?
//This means that input will be in the form of
//texture co-ords, like 0,4
//with a maximum of 10 tiles per row
/*******************************************/
//False will refer to the format of x*i + y 
//Where all texture tiles will be numerated from 0 to z (z being the last texture tile)
//This simplifies texture tile definitions to only one value, as oppose to two (a coord)
/*******************************************/
bool textureInputMode = true;
char openOrNewFile;

std::ofstream file;

std::string fileLocation;

std::fstream openfile;
std::fstream connectionInfoFile;

//Enable scrolling
bool enableScroll = true;

std::string tileLocation;

int screenSizeX = 1072;
int screenSizeY = 604;

float redTeamSpawnX, redTeamSpawnY, blueTeamSpawnX, blueTeamSpawnY;

float playerPosX, playerPosY;
int bulletPosX, bulletPosY;
//sf::RenderWindow gameWindow;
sf::View view1(sf::Vector2f((float)screenSizeX/2, (float)screenSizeY/2), sf::Vector2f((float)screenSizeX, (float)screenSizeY));

sf::Texture guiElements;
sf::Font arial;

sf::Sprite backgroundSprite;

sf::Vector2f viewCenter;
sf::Vector2f viewSize;

float scrollSpeed = 10;

sf::RectangleShape toolbarBox(sf::Vector2f((float)(screenSizeX*0.17), (float)(screenSizeY*0.6)));
sf::Sprite activeTileSprite;
sf::RectangleShape Selected;
//Toolbox elements
int xTileValue = 0;
int yTileValue = 0;
int layerValue = 0;
std::string tempXTileValue,tempYTileValue,tempLayerValue;

sf::Sprite upArrowX, downArrowX, saveButton, upArrowY, downArrowY, upArrowLayer, downArrowLayer;

sf::Text editBoxX, editBoxY, layerText;

bool showToolBox;

sf::Vector2f mousePositionGlobal;
sf::Vector2i mousePositionLocal;

const float maxY = 50.f;
const sf::Vector2f gravity(0.f, 0.5);
sf::Vector2f velocity(2.f, 5.f);

int bulletCount = 0;
int targetsCount = 0;
int targetsKilledCount = 0;

int redTeamKills = 0;
int blueTeamKills = 0;

sf::Text playerHealthText;
sf::Text deathScreenTimerText;

sf::Texture playerTexture;
sf::Texture bulletTexture;
sf::Texture warriorGuyTexture;
sf::Texture wizardTexture;
sf::Texture rogueTexture;
sf::Texture deadPlayerTexture;

sf::Texture hawkTexture;



sf::Texture deathOverlayGreyTexture;
sf::Sprite deathOverlayGreySprite;
sf::Texture healthFloatyBoxTexture;
sf::Sprite healthFloatyBoxSprite;

sf::Texture enemyMonsterBaseTexture;
sf::Texture mountainGemTexture;

std::string username;

sf::Vector2f oldPlayerVelocity;
sf::Vector2f oldPlayerPosition;

bool isTyping = false;
bool canType = true;
std::string textMessage;
bool toAll;
int teamAffiliation = 0;

std::vector < messagingStruct > messagesList;

sf::Text myText;
sf::Text typingText;

sf::Text redTeamScore, blueTeamScore;

int numOfMessages = 0;
unsigned int displayMessages = 0;

int enableWinCondition = 1;
bool startGame = false;
int teamWhichSentMessage;

sf::Vector2f bulletPos;
sf::Vector2f bulletVel;

int numOfRedPlayers = 0;
int numOfBluePlayers = 0;
int whichTeam;

std::vector < playerStatsServer > redTeamPlayerStatsServer;
std::vector < playerStatsServer > blueTeamPlayerStatsServer;

std::vector < playerStatsClient > redTeamPlayerStatsClient;
std::vector < playerStatsClient > blueTeamPlayerStatsClient;

std::vector < myPlayerNamespace::player > redTeamPlayers;
std::vector < myPlayerNamespace::player > blueTeamPlayers;

std::vector < myPlayerNamespace::player > playersList;
std::vector < playerStatsServer > playerStatsServerList;

bool redTeamPlayerUpdate = false, blueTeamPlayerUpdate = false;
int updatedPlayer = 0;

int iAmPlayerNumber = 0;
int localPlayerNumber = 0;

sf::Texture characterTexture;

sf::Vector2f updatePlayerPositionVar;
sf::Vector2f updatePlayerVelocityVar;

std::string updatePlayerStringVar;
int updatePlayerTeamVar;

sf::Vector2f bulletStartPos;

int bulletTypeReceived, teamReceived;
float damageDealtReceieved;
bool isDestroyedReceived;
sf::Vector2f bulletPositionReceived, bulletVelocityReceived;

bool wasConnectedTo = false;
bool isStillConnectedTo = false;

std::string messageToBeSent;

sf::Vector2f enemyStartPosition;

std::vector < computerID > connections;

bool addPlayerToList = true;

std::string serverStartGameCode;

int numberOfGameLoopsRan = 0;

bool isLocalPlayer = false;

unsigned int drawMapX1, drawMapX2, drawMapY1, drawMapY2, tilesDrawn;

sf::Text playerNameDrawerText;

bool serverResendUpdateToSender;

sf::Text victoryText;

bool allPlayersReady = false;
int numOfPlayersReady = 0;
unsigned int mapGeneratorSeed;

bool loadGem = true;
bool isGameLocal = true;
bool randomMapBeingPlayed = false;

sf::Time timeSinceJumpParticles;
sf::Time timeSpentInCurrentLevel;

sf::Time hawkFlapSpeed = sf::seconds((float)0.5);
sf::Time timeSinceWingsFlap = sf::Time::Zero;

float soundEffectsVolume, musicVolume;

//float gemPosX, gemPosY;

//enemyMonster::tallMountainGem tallMountainGemObject(mountainGemTexture, gemPosX, gemPosY);