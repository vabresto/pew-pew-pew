#include "stdafx.h"
#include "drawGameToScreen.h"
#include "globalVariables.h"
#include "updateToServer.h"
#include "HUDManager.h"
#include "animationManager.h"
#include "enemies.h"
#include "player.h"
#include "mainMenuElements.h"

#include <iostream>
#include <SFML/Graphics.hpp>

void drawGameToScreen()
{
	//View Managing
	viewCenter = view1.getCenter();
	viewSize = view1.getSize();

	//Calculating what part of the map must be rendered
	if ((viewCenter.x / mapTileSize) - 17 >= 0)
	{
		drawMapX1 = (int)((viewCenter.x / mapTileSize) - 17);
	}
	else
	{
		drawMapX1 = 0;
	}
	if ((viewCenter.x / mapTileSize) + 18 <= mapSizeX)
	{
		drawMapX2 = (int)(viewCenter.x / mapTileSize) + 18;
	}
	else
	{
		drawMapX2 = mapSizeX;
	}
	if ((viewCenter.y / mapTileSize) - 10 >= 1)
	{
		drawMapY1 = (int)(viewCenter.y / mapTileSize) - 10;
	}
	else
	{
		drawMapY1 = 1;
	}
	if (((viewCenter.y / mapTileSize) + 11 <= mapSizeY) && (viewCenter.y / mapTileSize) > 0)
	{
		drawMapY2 = (int)(viewCenter.y / mapTileSize) + 11;
	}
	else
	{
		drawMapY2 = mapSizeY + 1;
	}

	//Update game HUD
	updateGameHUD();

	/***************************/
	//Drawing stuff to the window
	/***************************/
	//Clear buffer
	gameWindow.clear();

	//Draw background
	gameWindow.draw(backgroundSprite);

	//The view has to be set in the draw function
	//for a reason that is not yet clear to me.
	gameWindow.setView(view1);

	//Draw Bullets (Arrows)
	for (unsigned int i = 0; i < bulletsList.size(); i++)
	{
		if (bulletsList[i].destroyBullet == true)
		{
			gameWindow.draw(bulletsList[i]);
		}
	}

	//Render map
	//Add stuff to new buffer
	//Y loop
	for (unsigned int i = drawMapY1; i < drawMapY2; i++)
	{
		//X loop
		for (unsigned int j = drawMapX1; j < drawMapX2; j++)
		{
			if ((map[i][j].x != -1) && (map[i][j].y != -1))
			{
				tiles.setPosition((float)(j * mapTileSize), (float)(i * mapTileSize));
				tiles.setTextureRect(sf::IntRect(map[i][j].x * mapTileSize, map[i][j].y * mapTileSize, mapTileSize, mapTileSize));
				gameWindow.draw(tiles);
				//tilesDrawn++;
			}
		}
	}

	//DEBUG: output tiles drawn - DISABLED
	//std::cout << tilesDrawn << std::endl;
	//tilesDrawn = 0;

	//Draw Gem
	if (loadGem == true)
	{
		gameWindow.draw(gemList[0]);
	}

	//Draw particles (layer 1)
	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		if (particleEntitiesList[i].drawLayer == 1)
		{
			gameWindow.draw(particleEntitiesList[i].sprite);
		}
	}

	//Draw target dummies
	for (unsigned int i = 0; i < targetsList.size(); i++)
	{
		gameWindow.draw(targetsList[i]);
	}

	//Draw Bullets (Arrows)
	for (unsigned int i = 0; i < bulletsList.size(); i++)
	{
		if (bulletsList[i].destroyBullet == false)
		{
			gameWindow.draw(bulletsList[i]);
		}
	}

	//Draw players
	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		gameWindow.draw(playersList[i]);
	}

	//Draw particles (layer 5)
	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		if (particleEntitiesList[i].drawLayer == 5)
		{
			gameWindow.draw(particleEntitiesList[i].sprite);
		}
	}

	//Draw the outline if the player is typing
	if (isTyping == true && isGameLocal == false)
	{
		gameWindow.draw(outline);
	}

	//Display Positions of arrows and guy
	/*for (unsigned int i = 0; i < playersList.size(); i++)
	{
		if (playersList[i].isNPC == true)
		{
			myText.setColor(sf::Color::White);
			myText.setPosition(view1.getCenter().x + 400.f, view1.getCenter().y - 200.f);
			myText.setString(std::to_string((_ULonglong)playersList[i].mSprite.getPosition().x));

			gameWindow.draw(myText);

			myText.setPosition(view1.getCenter().x + 450.f, view1.getCenter().y - 200.f);
			myText.setString(std::to_string((_ULonglong)playersList[i].mSprite.getPosition().y));

			gameWindow.draw(myText);
		}
	}

	int validBulletNum = 0;
	for (unsigned int i = 0; i < bulletsList.size(); i++)
	{
		
		if (bulletsList[i].destroyBullet == false)
		{
			myText.setColor(sf::Color::White);
			myText.setPosition(view1.getCenter().x + 400.f, view1.getCenter().y - 100.f + validBulletNum * 50.f);
			myText.setString(std::to_string((_ULonglong)bulletsList[i].mSprite.getPosition().x));

			gameWindow.draw(myText);

			myText.setPosition(view1.getCenter().x + 450.f, view1.getCenter().y - 100.f + validBulletNum * 50.f);
			myText.setString(std::to_string((_ULonglong)bulletsList[i].mSprite.getPosition().y));

			gameWindow.draw(myText);

			validBulletNum++;
		}
	}*/

	//Darken screen when player is dead
	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		if (particleEntitiesList[i].particleType == 15 && particleEntitiesList[i].spawnerPlayerNumber == localPlayerNumber)
		{
			deathOverlayGreySprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 304);
			gameWindow.draw(deathOverlayGreySprite, sf::BlendMultiply);

			deathScreenTimerText.setPosition(view1.getCenter().x - 25, view1.getCenter().y - 225);
			deathScreenTimerText.setString(std::to_string((_ULonglong)(playersList[particleEntitiesList[i].spawnerPlayerNumber].deathDuration.asSeconds() - particleEntitiesList[i].timeSinceAnimChange.asSeconds())));
			gameWindow.draw(deathScreenTimerText);
		}
	}

	//Draw players' names
	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		healthFloatyBoxSprite.setPosition(playersList[i].mSprite.getPosition().x + 3.f, playersList[i].mSprite.getPosition().y - 17.f);
		gameWindow.draw(healthFloatyBoxSprite);

		playerNameDrawerText.setPosition(playersList[i].mSprite.getPosition().x, playersList[i].mSprite.getPosition().y - 42);
		playerNameDrawerText.setString(playersList[i].playerName);

		if (playersList[i].teamAffiliation == 1)
		{
			playerNameDrawerText.setColor(sf::Color::Red);
		}
		else if (playersList[i].teamAffiliation == 2)
		{
			playerNameDrawerText.setColor(sf::Color::Blue);
		}
		gameWindow.draw(playerNameDrawerText);
	}

	/*********************/
	//Chat 'box'
	/*********************/
	if (isGameLocal == false)
	{
		//Check size of chat list
		if (messagesList.size() > 5)
		{
			displayMessages = 5;
		}
		else
		{
			displayMessages = messagesList.size();
		}

		//Display chat
		for (unsigned int i = 0; i < displayMessages; i++)
		{
			if (displayMessages == 5)
			{
				myText.setString(messagesList[i + messagesList.size() - 5].message);
				if (messagesList[i + messagesList.size() - 5].team == 0)
				{
					myText.setColor(sf::Color::White);
				}
				else if (messagesList[i + messagesList.size() - 5].team == 1)
				{
					myText.setColor(sf::Color::Red);
				}
				else if (messagesList[i + messagesList.size() - 5].team == 2)
				{
					myText.setColor(sf::Color::Blue);
				}
				else
				{
					myText.setColor(sf::Color::Magenta);
				}
			}
			else
			{
				myText.setString(messagesList[i].message);

				if (messagesList[i].team == 0)
				{
					myText.setColor(sf::Color::White);
				}
				else if (messagesList[i].team == 1)
				{
					myText.setColor(sf::Color::Red);
				}
				else if (messagesList[i].team == 2)
				{
					myText.setColor(sf::Color::Blue);
				}
				else
				{
					myText.setColor(sf::Color::Magenta);
				}
			}

			myText.setPosition(view1.getCenter().x - 500, view1.getCenter().y + 20*i + 160);
			gameWindow.draw(myText);
		}
	}

	//Display time left on "slowDebuffTimeLeft"
	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		if (playersList[i].slowDebuffTimeLeft > 0.f)
		{
			myText.setColor(sf::Color::Magenta);
			myText.setPosition(playersList[i].mSprite.getPosition().x + 3.f, playersList[i].mSprite.getPosition().y + 52.f);
			myText.setString(std::to_string((_ULonglong)playersList[i].slowDebuffTimeLeft));
			gameWindow.draw(myText);
		}
	}

	//Display cooldown on secondary ability
	if (playersList[localPlayerNumber].abilitiesVector[8].isActive == true &&
		playersList[localPlayerNumber].abilitiesVector[8].ability != 15)
	{
		myText.setColor(sf::Color::Magenta);
		myText.setPosition(playersList[localPlayerNumber].mSprite.getPosition().x + 3.f, playersList[localPlayerNumber].mSprite.getPosition().y + 52.f);
		myText.setString(std::to_string((_ULonglong)playersList[localPlayerNumber].abilitiesVector[8].durationLeft.asSeconds()));
		gameWindow.draw(myText);
	}
	else if (playersList[localPlayerNumber].abilitiesVector[8].isActive == false  && playersList[localPlayerNumber].abilitiesVector[8].ability == 1)
	{
		if (playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft > sf::Time::Zero)
		{
			myText.setColor(sf::Color::Magenta);
			myText.setPosition(playersList[localPlayerNumber].mSprite.getPosition().x + 3.f, playersList[localPlayerNumber].mSprite.getPosition().y + 52.f);
			myText.setString(std::to_string((_ULonglong)playersList[localPlayerNumber].abilitiesVector[8].cooldownLeft.asSeconds()));
			gameWindow.draw(myText);
		}
	}

	//Display death timer over grave
	for (unsigned int i = 0; i < particleEntitiesList.size(); i++)
	{
		if (particleEntitiesList[i].particleType == 15)
		{
			myText.setColor(sf::Color::Red);
			myText.setPosition(particleEntitiesList[i].sprite.getPosition().x + 10.f, particleEntitiesList[i].sprite.getPosition().y - 10.f);
			myText.setString(std::to_string((_ULonglong)(playersList[particleEntitiesList[i].spawnerPlayerNumber].deathDuration.asSeconds() - particleEntitiesList[i].timeSinceAnimChange.asSeconds())));
			gameWindow.draw(myText);
		}
	}

	//Display Unit Health
	for (unsigned int i = 0; i < playersList.size(); i++)
	{
		//healthFloatyBoxSprite.setPosition(playersList[i].mSprite.getPosition().x + 3.f, playersList[i].mSprite.getPosition().y - 17.f);
		//gameWindow.draw(healthFloatyBoxSprite);
		if (playersList[i].health >= 0)
		{
			playerHealthText.setColor(sf::Color::Green);
			playerHealthText.setPosition(playersList[i].mSprite.getPosition().x + 10.f, playersList[i].mSprite.getPosition().y - 17.f);
			playerHealthText.setString(std::to_string((_ULonglong)playersList[i].health));
			gameWindow.draw(playerHealthText);
		}
		else
		{
			playerHealthText.setColor(sf::Color::Green);
			playerHealthText.setPosition(playersList[i].mSprite.getPosition().x + 10.f, playersList[i].mSprite.getPosition().y - 17.f);
			playerHealthText.setString("0");
			gameWindow.draw(playerHealthText);
		}
	}

	//Give the player live feedback of what they're typing
	if (isTyping == true && isGameLocal == false)
	{
		typingText.setString(messageToBeSent);
		typingText.setPosition(view1.getCenter().x - 500, view1.getCenter().y + 275);
		gameWindow.draw(typingText);
	}

	/*//Display score
	redTeamScore.setPosition(view1.getCenter().x - 480, view1.getCenter().y - 275);
	redTeamScore.setString("Red Team: " + std::to_string((_ULonglong)redTeamKills) + "/" + std::to_string((_ULonglong)enableWinCondition));

	blueTeamScore.setPosition(view1.getCenter().x + 210, view1.getCenter().y - 275);
	blueTeamScore.setString("Blue Team: " + std::to_string((_ULonglong)blueTeamKills));*/

	gameWindow.draw(redTeamScore);
	gameWindow.draw(blueTeamScore);

	myText.setPosition(view1.getCenter().x, view1.getCenter().y - 275);
	myText.setString("Time: " + std::to_string((_ULonglong)timeSpentInCurrentLevel.asSeconds()));
	myText.setColor(sf::Color::White);
	gameWindow.draw(myText);

	//Check if win condition has been met, if it is true
	if (enableWinCondition != 0)
	{
		//Display score
		redTeamScore.setPosition(view1.getCenter().x - 480, view1.getCenter().y - 275);
		redTeamScore.setString("Red Team: " + std::to_string((_ULonglong)redTeamKills) + "/" + std::to_string((_ULonglong)enableWinCondition));

		blueTeamScore.setPosition(view1.getCenter().x + 210, view1.getCenter().y - 275);
		blueTeamScore.setString("Blue Team: " + std::to_string((_ULonglong)blueTeamKills) + "/" + std::to_string((_ULonglong)enableWinCondition));

		if (redTeamKills >= enableWinCondition)
		{
			if (isGameLocal == false)
			{
				//Send Disconnect code
				//updateToServer(0);
				updateBackendToServer(0, "_codeSuppliedByFunction");
			}

			std::cout << "Red Team Wins!\n";
			redTeamKills = 0;
			blueTeamKills = 0;

			map.clear();
			closeWindow = true;
			gameEndScreen(1);
		}
		else if (blueTeamKills >= enableWinCondition)
		{
			if (isGameLocal == false)
			{
				//Send Disconnect code
				//updateToServer(0);
				updateBackendToServer(0, "_codeSuppliedByFunction");
			}

			std::cout << "Blue Team Wins!\n";
			redTeamKills = 0;
			blueTeamKills = 0;

			map.clear();
			closeWindow = true;
			gameEndScreen(2);
		}
	}
	else
	{
		//Display score
		redTeamScore.setPosition(view1.getCenter().x - 480, view1.getCenter().y - 275);
		redTeamScore.setString("Red Team: " + std::to_string((_ULonglong)redTeamKills));

		blueTeamScore.setPosition(view1.getCenter().x + 210, view1.getCenter().y - 275);
		blueTeamScore.setString("Blue Team: " + std::to_string((_ULonglong)blueTeamKills));
	}

	//Hide HUD if player is in its area (TODO: Improve HUD)
	if ((view1.getCenter().y - playersList[localPlayerNumber].mSprite.getPosition().y) > -160)
	{
		drawGameHUD();
	}

	//Render buffer
	if (closeWindow == false)
	{
		gameWindow.display();
	}
}