#include "stdafx.h"
#include "scrollView.h"

#include "globalVariables.h"

void scrollView()
{
	/************************/
	//View Scrolling
	/************************/
	enableScroll = false;
	//Mouse Controlled Scrolling
	/*if ((mousePositionLocal.y < screenSizeY * 0.1) && (enableScroll == true))
	{
	//If the view would leave the map by scrolling; don't scroll
	if (viewCenter.y < mapTileSize * 10.7)
	{

	}
	else
	{
	scroll(0,-scrollSpeed);
	}
	}
	if ((mousePositionLocal.y > screenSizeY * 0.9) && (enableScroll == true))
	{
	if (viewCenter.y > ((mapSizeY * mapTileSize)) - (mapTileSize * 8.7))
	{

	}
	else
	{
	scroll(0,scrollSpeed);
	}
	}
	if (mousePositionLocal.x < screenSizeX * 0.1 && enableScroll == true)
	{
	if (viewCenter.x < (mapTileSize * 17))
	{

	}
	else
	{
	scroll(-scrollSpeed,0);
	}
	}
	if ((mousePositionLocal.x > screenSizeX * 0.9) && enableScroll == true)
	{
	if (viewCenter.x > (mapSizeX * mapTileSize) - (mapTileSize * 17.1))
	{

	}
	else
	{
	scroll(scrollSpeed,0);
	}
	}*/

	//Player sprite controlled camera scrolling
	if (playersList[localPlayerNumber].mSprite.getPosition().x < view1.getCenter().x - (screenSizeX * 0.1))
	{
		if (viewCenter.x < (mapTileSize * 17))
		{

		}
		else
		{
			scroll(-scrollSpeed,0);
		}
	}
	if (playersList[localPlayerNumber].mSprite.getPosition().x > view1.getCenter().x + (screenSizeX * 0.1))
	{
		if (viewCenter.x > (mapSizeX * mapTileSize) - (mapTileSize * 17.1))
		{

		}
		else
		{
			scroll(scrollSpeed,0);
		}
	}
	if ((playersList[localPlayerNumber].mSprite.getPosition().y < view1.getCenter().y))
	{
		if (viewCenter.y < mapTileSize * 10.7)
		{

		}
		else
		{
			scroll(0,-scrollSpeed);
		}
	}
	if ((playersList[localPlayerNumber].mSprite.getPosition().y > view1.getCenter().y + (screenSizeY * 0.2)))
	{
		if (viewCenter.y > ((mapSizeY * mapTileSize)) - (mapTileSize * 8.7))
		{

		}
		else
		{
			scroll(0,scrollSpeed);
		}
	}
}