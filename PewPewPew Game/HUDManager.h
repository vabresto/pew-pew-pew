#ifndef HUDMANAGER_CPP_
#define HUDMANAGER_CPP_

extern void initializeGameHUD();
extern void drawGameHUD();
extern void updateGameHUD();

extern sf::Texture HUDAbilitiesGraphicTexture;
extern sf::Sprite HUDAbilitiesGraphicSprite;

extern sf::Texture HUDAbilitiesAbilityTwoTexture;
extern sf::Sprite HUDAbilitiesAbilityTwoSprite;

extern sf::Texture HUDAbilitiesAbilityThreeTexture;
extern sf::Sprite HUDAbilitiesAbilityThreeSprite;

extern sf::Text abilityTwoCooldownTimerText, abilityOneCooldownTimerText;

#endif