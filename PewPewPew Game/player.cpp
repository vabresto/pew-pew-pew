#include "stdafx.h"
#include "player.h"
#include "globalVariables.h"
#include "updateToServer.h"
#include "animationManager.h"
#include "audioManager.h"
#include "NPC.h"

#include <vector>
#include <iostream>
//#include <windows.h>
#include <math.h>

// you need this because of forward declaration
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics.hpp>


namespace myPlayerNamespace {
	/****************/
	//Player Class
	/****************/

	/********************************************/
	//Player Hitbox (Tiles)
	//(x,y) -------------------- (x + 32, y)
	//      (x+5, y+6) ------ (x + 28, y + 6)
	//
	//
	//
	//      (x+5, y+48) ----- (x+28, y + 48)
	/////////////////////////////////////////////
	//Player Hitbox (Bullets/Other)
	//(x,y) -------------------- (x+32, y+32)
	//         (x+9, y+5) ------- (x+26, y + 5)
	//
	//
	//
	//
	//         (x+9, y+48) ------ (x+26, y+5)
	//
	player::player(const sf::Texture& imagePath, const float _startPosX, const float _startPosY, const int _team, const bool _isNPC) : mSprite(imagePath), mSource(1, player::Right) 
	{
		//mSprite.setPosition(300.f, 400.f);
		//outline.setPosition(308.f, 406.f);
		if (_team == 1)
		{
			mSprite.setPosition(redTeamSpawnX, redTeamSpawnY);
		}
		else
		{
			mSprite.setPosition(blueTeamSpawnX, blueTeamSpawnY);
		}
		mSprite.setPosition(_startPosX, _startPosY);
		//outline.setPosition(_startPosX + 8, _startPosY + 6);
		mSprite.setTextureRect(sf::IntRect(0,0,32,48));

		hitGround = false;
		isShooting = false;
		canWalk = true;
		isUsingAbilityAnimation = false;
		isDead = false;

		sf::Time timeSinceAnimChange;

		deathTimer = sf::Time::Zero;
		deathDuration = sf::seconds(5.f);

		textureSize = 1;

		isNPC = _isNPC;

		if (isNPC == true)
		{
			NPCState = 1;
		}

		isJumping = true;
		doubleJump = false;
		directionFacing = 1;
		//mSprite.setOrigin(0.f, -48.f);
		xSpeed = 0.f;
		ySpeed = 0.f;

		slowDebuffTimeLeft = 0;
		slowDebuffPlayerSpeed = 0.f;
		slowDebuffPlayerJumpSpeed = 5.f;

		doubleJumpAllowed = false;

		playerSpeed = 1;
		playerJumpSpeed = 10;
		playerFallSpeed = 2;

		maxNormalJumpSpeed = 10;

		playerMaxSpeed = 3;
		playerMaxJumpSpeed = 10;
		playerMaxFallSpeed = 7;

		playerVelocity.x = 0.f;
		playerVelocity.y = 0.f;

		playerMaxVelocity.x = 3;
		playerMaxVelocity.y = 7;

		if (_team == 1)
		{
			mSprite.setColor(sf::Color::White);
			teamAffiliation = 1;
		}
		else
		{
			mSprite.setColor(sf::Color::White);
			teamAffiliation = 2;
		}

		for (unsigned int i = 0; i < 11; i++)
		{
			playerAbilitiesStruct newAbilities;
			newAbilities.ability = 0;

			newAbilities.cooldownLeft = sf::Time::Zero;
			newAbilities.cooldownTime = sf::Time::Zero;
			newAbilities.isActive = false;

			abilitiesVector.push_back(newAbilities);
		}

		for (unsigned int i = 0; i < 6; i++)
		{
			statusEffectStruct newEffect;
			newEffect.effectID = i;
			newEffect.value = 0;

			if (i == 1)
			{
				newEffect.value = 1;
			}
			if (i == 3)
			{
				newEffect.durationLeft = sf::Time::Zero;
			}
			if (i == 4)
			{
				newEffect.durationLeft = sf::Time::Zero;
			}
			statusEffectsVector.push_back(newEffect);
		}

		

	}
	player::~player() {
		// TODO Auto-generated destructor stub
	}
	void player::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		target.draw(mSprite, states);
	}
	void player::updatePlayer()
	{
		if (abilitiesVector[8].ability == 1 && abilitiesVector[8].isActive == true)
		{

		}
		else if (abilitiesVector[8].ability == 1 && abilitiesVector[8].isActive == false)
		{
			if (playerVelocity.y > 0)
			{
				scrollSpeed = 14.f;
			}
			else if (playerVelocity.y < 0)
			{
				scrollSpeed = 10.f;
			}
		}

		//Variables to store old + new position
		playerPosX = mSprite.getPosition().x;
		playerPosY = mSprite.getPosition().y;

		oldPlayerPosition.x = playerPosX;
		oldPlayerPosition.y = playerPosY;

		oldPlayerVelocity.x = playerVelocity.x;
		oldPlayerVelocity.y = playerVelocity.y;

		if (isNPC == true)
		{
			controlNPC(updatedNPC);
		}

		//Collision top
		if (((map[(int)(playerPosY+6)/32][(int)(playerPosX+5)/32].x) != 0 || (map[(int)(playerPosY+6)/32][(int)(playerPosX+5)/32].y) != 0 ) || 
			((map[(int)(playerPosY+6)/32][(int)(playerPosX+28)/32].x) != 0 || (map[(int)(playerPosY+6)/32][(int)(playerPosX+28)/32].y) != 0 ))
		{
			if (playerVelocity.y < 0)
			{
				playerVelocity.y = 0;
			}
		}

		//Collision right
		if (((map[(int)(playerPosY+45)/32][(int)(playerPosX+28)/32].x) != 0 || (map[(int)(playerPosY+45)/32][(int)(playerPosX+28)/32].y) != 0 ) || 
			((map[(int)(playerPosY+6)/32][(int)(playerPosX+28)/32].x) != 0 || (map[(int)(playerPosY+6)/32][(int)(playerPosX+28)/32].y) != 0 ))
		{
			if (((map[(int)(playerPosY+45)/32][(int)(playerPosX+28)/32].x) == 4 && (map[(int)(playerPosY+45)/32][(int)(playerPosX+28)/32].y) == 5 ) && 
				((map[(int)(playerPosY+6)/32][(int)(playerPosX+28)/32].x) == 4 && (map[(int)(playerPosY+6)/32][(int)(playerPosX+28)/32].y) == 5 ))
			{
				//std::cout << "Ladder right!";
			}
			else
			{
				if (playerVelocity.x > 0)
				{
					playerVelocity.x = 0;
				}
			}
		}

		//Collision left
		if (((map[(int)(playerPosY+6)/32][(int)(playerPosX+5)/32].x) != 0 || (map[(int)(playerPosY+6)/32][(int)(playerPosX+5)/32].y) != 0 ) || 
			((map[(int)(playerPosY+45)/32][(int)(playerPosX+5)/32].x) != 0 || (map[(int)(playerPosY+45)/32][(int)(playerPosX+5)/32].y) != 0 ))
		{
			if (((map[(int)(playerPosY+6)/32][(int)(playerPosX+5)/32].x) == 4 && (map[(int)(playerPosY+6)/32][(int)(playerPosX+5)/32].y) == 5 ) && 
				((map[(int)(playerPosY+45)/32][(int)(playerPosX+5)/32].x) == 4 && (map[(int)(playerPosY+46)/32][(int)(playerPosX+5)/32].y) == 5 ))
			{
				//std::cout << "Ladder left!";
			}
			else
			{
				if (playerVelocity.x < 0)
				{
					playerVelocity.x = 0;
				}
			}
		}

		//Update player texture - might be unnecessary due to doing it in the draw loop
		/*if (playerVelocity.x < 0)
		{
			mSprite.setTextureRect(sf::IntRect(32,0,-32,48));
		}
		if (playerVelocity.x > 0)
		{
			mSprite.setTextureRect(sf::IntRect(0,0,32,48));
		}*/
		//animatePlayer();

		//Check for collision with bullets
		for (unsigned int i = 0; i < bulletsList.size(); i++)
		{
			for (unsigned int j = 0; j < playersList.size(); j++)
			{
				if (playersList[j].mSprite.getGlobalBounds().intersects(bulletsList[i].mSprite.getGlobalBounds()))
				{
					if (bulletsList[i].teamAffiliationBullet == playersList[j].teamAffiliation)
					{
						//Do nothing - same team
					}
					else
					{
						if (bulletsList[i].destroyBullet == false)
						{
							bulletsList[i].destroyBullet = true;
							bulletsList.erase(bulletsList.begin() + i);

							if (playersList[j].abilitiesVector[8].ability == 8 && 
								playersList[j].abilitiesVector[8].isActive == true &&
								playersList[j].directionFacing != bulletsList[i].direction)
							{

							}
							else
							{
								playersList[j].health -= bulletsList[i].damageDealt;
							}

							/*if (isGameLocal == false)
							{
								updateToServer(4, bulletsList[i]);
							}*/

							i--;
						}
					}
				}
			}
		}

		//Actually *move* the player (and the outline)
		if (canWalk == true)
		{
			mSprite.move(playerVelocity.x, playerVelocity.y);
			outline.move(playerVelocity.x, playerVelocity.y);
		}
		else
		{
			mSprite.move(0, playerVelocity.y);
			outline.move(0, playerVelocity.y);
		}
		
		//Score for random map
		if (randomMapBeingPlayed == true)
		{
			if (teamAffiliation == 1)
			{
				redTeamKills = (int)(400 - (mSprite.getPosition().y /32));
			}
			else if (teamAffiliation == 2)
			{
				blueTeamKills = (int)(400 - (mSprite.getPosition().y /32));
			}
		}

		//Check if game is played locally only
		if (isGameLocal == false)
		{
			//Update position + velocity to server
			if (mSprite.getPosition().x != oldPlayerPosition.x || mSprite.getPosition().y != oldPlayerPosition.y || oldPlayerVelocity != playerVelocity)
			{
				//updateToServer(2, mSprite.getPosition(), playerVelocity);

				if (isNPC == true)
				{
					updateCharacterToServer(1, updatedNPC, mSprite.getPosition().x, mSprite.getPosition().y, directionFacing, currentFrameX, currentFrameY, health);
				}
				else
				{
					updateCharacterToServer(1, localPlayerNumber, mSprite.getPosition().x, mSprite.getPosition().y, directionFacing, currentFrameX, currentFrameY, health);
				}
			}
		}

		//Gravity + Collision bottom
		if ((((map[(int)(playerPosY+48)/32][(int)(playerPosX+5)/32].x) != 0 || (map[(int)(playerPosY+48)/32][(int)(playerPosX+5)/32].y) != 0 ) || 
			((map[(int)(playerPosY+48)/32][(int)(playerPosX+28)/32].x) != 0 || (map[((int)playerPosY+48)/32][(int)(playerPosX+28)/32].y) != 0 )))
		{
			{
				//Anti-Daniel walls
				if ((((map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].x) == 2 && (map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].y) == 3 ) || 
					((map[(int)(playerPosY+48)/32][(int)(playerPosX + 32)/32].x) == 2 && (map[((int)playerPosY+48)/32][(int)(playerPosX + 32)/32].y) == 3 )) ||
					(((map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].x) == 3 && (map[(int)(playerPosY+48)/32][(int)(playerPosX)/32].y) == 3 ) || 
					((map[(int)(playerPosY+48)/32][(int)(playerPosX + 32)/32].x) == 3 && (map[((int)playerPosY+48)/32][(int)(playerPosX + 32)/32].y) == 3 )))
				{
					doubleJump = true;

					if (playerVelocity.y <= (playerMaxVelocity.y/2))
					{
						playerVelocity.y += gravity.y;
					}
					else
					{
						playerVelocity.y = (playerMaxVelocity.y/2);
					}


					if ((((map[(int)(playerPosY+50)/32][(int)(playerPosX)/32].x) != 0 && (map[(int)(playerPosY+50)/32][(int)(playerPosX)/32].y) != 0 ) && 
					((map[(int)(playerPosY+50)/32][(int)(playerPosX + 35)/32].x) != 0 && (map[((int)playerPosY+50)/32][(int)(playerPosX + 32)/32].y) != 0 )))
					{
						playerVelocity.y = 0.f;

						if (playerHitGroundSound.getStatus() != playerHitGroundSound.Playing)
						{
							playerHitGroundSound.play();
						}

					}
				}
				else
				{
					if (abilitiesVector[8].isActive == false)
					{
						if (playerHitGroundSound.getStatus() != playerHitGroundSound.Playing && isJumping == true && playerVelocity.y > 0)
						{
							playerHitGroundSound.play();
						}
						playerVelocity.y = 0.f;
					}
					if (abilitiesVector[8].isActive == false &&
						abilitiesVector[8].ability == 22)
					{
						if (playerHitGroundSound.getStatus() != playerHitGroundSound.Playing && isJumping == true && playerVelocity.y > 0)
						{
							playerHitGroundSound.play();
						}
						playerVelocity.y = 0.f;
					}
					isJumping = false;
					doubleJump = false;
					doubleJumpAllowed = false;
				}

				if ((((map[(int)(playerPosY+46)/32][(int)(playerPosX+10)/32].x) != 0 || (map[(int)(playerPosY+46)/32][(int)(playerPosX+10)/32].y) != 0 ) || 
					((map[(int)(playerPosY+46)/32][(int)(playerPosX+20)/32].x) != 0 || (map[(int)(playerPosY+46)/32][(int)(playerPosX+20)/32].y) != 0 )))
				{
					playerVelocity.y = 0.f;
					playerPosY -= 2;
					mSprite.setPosition(playerPosX, playerPosY);
					outline.setPosition(playerPosX+8, playerPosY+6);

					if (mSprite.getPosition().x == playersList[localPlayerNumber].mSprite.getPosition().x &&
						mSprite.getPosition().y == playersList[localPlayerNumber].mSprite.getPosition().y &&
						deathTimer <= sf::Time::Zero)
					{
						view1.setCenter(view1.getCenter().x, view1.getCenter().y -2);
						backgroundSprite.setPosition(backgroundSprite.getPosition().x, backgroundSprite.getPosition().y - 2);
					}

					if (abilitiesVector[8].isActive == true && abilitiesVector[8].ability == 1)
					{

					}
					else
					{
						if (playerHitGroundSound.getStatus() != playerHitGroundSound.Playing)
						{
							playerHitGroundSound.play();
						}
					}

					/*currentFrameX = 3;
					currentFrameY = 3;
					timeSinceAnimChange = sf::Time::Zero;*/
				}				
			}
		}
		else
		{
			if (abilitiesVector[8].isActive == false && abilitiesVector[8].ability == 1)
			{
				//scrollSpeed = 10.f;
				if (playerVelocity.y <= playerMaxVelocity.y && isJumping == true)
				{
					playerVelocity.y += gravity.y;
				}
				isJumping = true;
			}
			else if (abilitiesVector[8].isActive == true  && abilitiesVector[8].ability == 1)
			{
				//scrollSpeed = 14.f;
				if (playerVelocity.y <= (float)((playerMaxVelocity.y/2)-0.5))
				{
					playerVelocity.y += gravity.y;
				}
				else
				{
					playerVelocity.y = 2.f;
				}
			}
			else if (abilitiesVector[8].ability != 1)
			{
				if (playerVelocity.y <= playerMaxVelocity.y && isJumping == true)
				{
					playerVelocity.y += gravity.y;
				}
				isJumping = true;
			}
		}

		for (unsigned int j = 0; j < playersList.size(); j++)
		{
			if (playersList[j].health <= 0 && playersList[j].deathTimer <= sf::Time::Zero)
			{
				//spawnCorpseParticles(j);

				if (isGameLocal == false && playersList[j].playerID == localPlayerNumber)
				{
					updateParticlesToServer(2, j, 15);

					if (playersList[j].playerID == localPlayerNumber)
					{
						if (playersList[j].teamAffiliation == 1)
						{
							updateScoreToServer(0, 1, 2);
						}
						if (playersList[j].teamAffiliation == 2)
						{
							updateScoreToServer(0, 1, 1);
						}
					}
				}
				playersList[j].mSprite.setTexture(deadPlayerTexture);
				playersList[j].deathTimer = playersList[j].deathDuration;
				playersList[j].isDead = true;
			}
		}

		/*if (health <= 0 && deathTimer <= sf::Time::Zero)
		{
			if (isGameLocal == false && playerID == localPlayerNumber)
			{
				updateParticlesToServer(2, playerID, 15);

				if (teamAffiliation == 1)
				{
					updateScoreToServer(0, 1, 2);
				}
				else if (teamAffiliation == 2)
				{
					updateScoreToServer(0, 1, 1);
				}
			}
			mSprite.setTexture(deadPlayerTexture);
			deathTimer = deathDuration;
			isDead = true;
		}*/

		//if (deathTimer <= sf::Time::Zero && health <= 0)
		//{
		//if (deathTimer > deathDuration)
		if (deathTimer <= sf::Time::Zero && isDead == true)
		{
			health = healthMax;
			isDead = false;
			if (teamAffiliation == 1)
			{
				mSprite.setPosition(redTeamSpawnX, redTeamSpawnY);
			}
			else
			{
				mSprite.setPosition(blueTeamSpawnX, blueTeamSpawnY);
			}

			if (playerID == localPlayerNumber)
			{
				view1.setCenter(playersList[localPlayerNumber].mSprite.getPosition().x + 30, playersList[localPlayerNumber].mSprite.getPosition().y - 166);
				backgroundSprite.setPosition(view1.getCenter().x - 540, view1.getCenter().y - 604);
			}

			deathTimer = sf::Time::Zero;

			if (characterTexture == 1)
			{
				mSprite.setTexture(playerTexture);
			}
			else if (characterTexture == 2)
			{
				mSprite.setTexture(warriorGuyTexture);
			}
			else if (characterTexture == 3)
			{
				mSprite.setTexture(wizardTexture);
			}
			else if (characterTexture == 4)
			{
				mSprite.setTexture(rogueTexture);
			}
		}
		//}
	}

	/******************/
	//Projectile Class
	/******************/
	projectile::projectile(const sf::Texture& imagePath, const sf::Vector2f _currentPosition, const sf::Vector2f _velocity, const int _bulletNumber, const int _team, float _damageDealt) : mSprite(imagePath), mSource(1, projectile::Left) 
	{
		//mSprite.setPosition(300.f, 400.f);
		projectile::mSprite.setPosition(_currentPosition);
		bulletType = 1;
		destroyBullet = false;
		myBulletNumber = _bulletNumber;
		damageDealt = _damageDealt;
		projectileSpeed = 7.f;
		if (_velocity.x < 0)
		{
			mSprite.setTextureRect(sf::IntRect(16,0,16,16));
			velocity.y = (float)(-2);
			velocity.x = _velocity.x;
			direction = 0;
		}
		else if (_velocity.x > 0)
		{
			mSprite.setTextureRect(sf::IntRect(0,0,16,16));
			velocity.y = (float)(-2);
			velocity.x = _velocity.x;
			direction = 1;
		}
		/*if (direction == 0)
		{
			mSprite.setTextureRect(sf::IntRect(16,0,16,16));
			startPos.x = _startPosX;
			startPos.y = _startPosY;
			goingLeft = true;
			goingRight = false;
		}
		else
		{
			mSprite.setTextureRect(sf::IntRect(0,0,16,16));
			startPos.x = _startPosX;
			startPos.y = _startPosY;
			goingLeft = false;
			goingRight = true;
		}*/

		if (_team == 1)
		{
			mSprite.setColor(sf::Color::Red);
			teamAffiliationBullet = 1;
		}
		else
		{
			mSprite.setColor(sf::Color::Blue);
			teamAffiliationBullet = 2;
		}
	}

	projectile::~projectile() {

		// TODO Auto-generated destructor stub
	}

	void projectile::draw(sf::RenderTarget& target, sf::RenderStates states) const {

		target.draw(mSprite, states);
	}
	void projectile::moveLeft(float speed) 
	{
		//mSource.y = Left;
		mSprite.setTextureRect(sf::IntRect(16,0,16,16));
		mSprite.move(-speed, 0);

		//animation
		/*mSource.x++;
		if (mSource.x * 32 >= (int) mSprite.getTexture()->getSize().x) {
		mSource.x = 0;
		}*/
	}
	void projectile::moveRight(float speed) 
	{
		//mSource.y = Right;
		mSprite.move(speed, 0);
		mSprite.setTextureRect(sf::IntRect(0,0,16,16));

		//animation
		/*mSource.x++;
		if (mSource.x * 32 >= (int) mSprite.getTexture()->getSize().x) {
		mSource.x = 0;
		}*/
	}
	void projectile::update()
	{
		if (velocity.y < 3.f && projectile::destroyBullet == false)
		{
			velocity.y += (float)(gravity.y/9);
		}

		projectile::mSprite.setRotation(tan((float)velocity.x/(float)7));

		projectile::mSprite.move(velocity);

		//Collision detection
		if (velocity.x < 0)
		{
			if ((map[(int)(mSprite.getPosition().y+7)/32][(int)(mSprite.getPosition().x+7)/32].x) != 0 || 
				(map[(int)(mSprite.getPosition().y+7)/32][(int)(mSprite.getPosition().x+7)/32].y) != 0)
			{
				if (projectile::destroyBullet == false)
				{
					if (direction == 0)
					{
						mSprite.setPosition(mSprite.getPosition().x+4, mSprite.getPosition().y);
					}
					else if (direction == 1)
					{
						mSprite.setPosition(mSprite.getPosition().x-4, mSprite.getPosition().y);
					}
				}
				projectile::destroyBullet = true;
				arrowImpactSound.play();
			}
		}

		if (velocity.x > 0)
		{
			if ((map[(int)(mSprite.getPosition().y+7)/32][(int)(mSprite.getPosition().x+7)/32].x) != 0 || 
				(map[(int)(mSprite.getPosition().y+7)/32][(int)(mSprite.getPosition().x+7)/32].y) != 0)
			{
				if (projectile::destroyBullet == false)
				{
					if (direction == 0)
					{
						mSprite.setPosition(mSprite.getPosition().x+4, mSprite.getPosition().y);
					}
					else if (direction == 1)
					{
						mSprite.setPosition(mSprite.getPosition().x-4, mSprite.getPosition().y);
					}
				}
				projectile::destroyBullet = true;
				arrowImpactSound.play();
			}			
		}

		//Deleting unneeded bullets
		for (unsigned int i = 0; i < bulletsList.size(); i++)
		{
			if (bulletsList[i].destroyBullet == true)
			{
				if (leaveDestroyedArrows == false)
				{
					bulletsList.erase(bulletsList.begin() + i);

					/*if (isGameLocal == false)
					{
						updateToServer(4, bulletsList[i]);
					}*/

					i--;
				}
				else
				{
					bulletsList[i].velocity.x = 0.f;
					bulletsList[i].velocity.y = 0.f;

					/*if (isGameLocal == false)
					{
						updateToServer(4, bulletsList[i]);
					}*/
				}	
			}
		}
	}

	/****************************************/
	//Testing stuff with packets & bullets
	/****************************************/
	/*sf::Packet& operator <<(sf::Packet& packet, const std::vector < myPlayerNamespace::projectile > _projectilesList)
	{
		int sizeOfVector = _projectilesList.size();
		for (int i = 0; i < _projectilesList.size(); i++)
		{
			packet << _projectilesList[i].bulletType << _projectilesList[i].currentPosition.x << _projectilesList[i].currentPosition.y << _projectilesList[i].velocity.x << _projectilesList[i].velocity.y << _projectilesList[i].teamAffiliationBullet;
		}
		return packet;
	}

	sf::Packet& operator >>(sf::Packet& packet, const std::vector < myPlayerNamespace::projectile > _projectilesList)
	{
		int sizeOfVector;
		packet >> sizeOfVector;
		for (int i = 0; i < sizeOfVector; i++)
		{
			sf::Vector2f bulletPos, bulletVel;
			int team, bulletType;
			packet >> bulletType >> bulletPos.x >> bulletPos.y >> bulletVel.x >> bulletVel.y >> team;
			
			bulletsList.push_back(myPlayerNamespace::projectile (bulletTexture, bulletPos, bulletVel, bulletCount, team));
		}
		return packet;
	}*/

} /* end namespace*/;

std::vector < myPlayerNamespace::projectile > bulletsList;